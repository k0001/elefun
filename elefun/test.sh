#! /usr/bin/env nix-shell
#! nix-shell ./shell.nix -i bash
set -e

# Manual cleanup necessary for systems where no Nix build sandbox is used.
_cleanup_cmd="echo 'Cleaning up...'"
add_cleanup() { _cleanup_cmd="$_cleanup_cmd; ($1 || true)"; }
cleanup() { code=$?; eval "$_cleanup_cmd"; exit $code; }
trap cleanup EXIT INT TERM

runghc ./Setup.hs haddock
runghc ./Setup.hs build -j test:test

echo "Starting PostgreSQL server"
PGDATA=$(mktemp -d)
PGHOST=$(mktemp -d)
initdb -D $PGDATA
echo "listen_addresses = ''" >> $PGDATA/postgresql.conf
echo "unix_socket_directories = '$PGHOST'" >> $PGDATA/postgresql.conf
pg_ctl -D $PGDATA start -l $PGDATA/logfile
add_cleanup "pg_ctl -D $PGDATA stop"
while ! (pg_ctl -D $PGDATA status >> /dev/null); do sleep 0.1; done


echo "Creating elefun-test databasE"
psql -h $PGHOST -d template1 -c \
   "CREATE ROLE \"elefun-test\" WITH \
    LOGIN NOCREATEDB NOCREATEROLE \
    ENCRYPTED PASSWORD 'elefun-test'"
createdb -h $PGHOST -T template1 -E utf-8 -O elefun-test elefun-test

echo "Running tests"
ELEFUN_TEST_DB_URL=postgresql:///elefun-test?host=$PGHOST \
  runghc ./Setup.hs test
