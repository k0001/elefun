{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses #-}
{-# OPTIONS_HADDOCK hide #-}

module Elefun.Expr
 ( PgArray
 , pgArray

 , PgArrayN
 , pgArrayN

 , PgBool
 , pgBool
 , true
 , false

 , PgBytea
 , pgBytea

 , PgCitext

 , PgDate

 , PgFloat4
 , pgFloat4

 , PgFloat8
 , pgFloat8

 , PgInt2
 , pgInt2

 , PgInt4
 , pgInt4

 , PgInt8
 , pgInt8

 , PgJsonb
 , unsafePgJsonb

 , PgJson

 , PgText
 , pgText

 , PgTimestamptz

 , PgTimestamp

 , PgTime

 , PgUuid
 , pgUuid

 , PgNumeric
 , pgRational
 , pgScientific
 , pgFixed

 , PgPrimType(pgPrimTypeName)
 , PgTyped(PgType)

 , E(UnsafeE, unE)
 , rE
 , rE'
 , CastE
 , castE
 , upcastE
 , unsafeDowncastE
 , unsafeCoerceE
 , unsaferCoerceE
 , unsaferCastE

 , N(UnsafeN, unN)
 , null
 , n
 , rN
 , rN'
 , en
 , liftN2
 , matchN
 , mapN
 , altN
 , bindN
 , forN
 , fromN

 , ToE(e)

 , reSub
 , reSplit
 , reReplace
 , reReplaceg
 , reMatch

 , toTimestamptz
 , toTimestamp
 , tstzEpoch
 , tsCentury
 , tsDay
 , tsDayOfTheWeek
 , tsDayOfTheWeekISO8601
 , tsDayOfTheYear
 , tsDecade
 , tsHour
 , tsMicroseconds
 , tsMillenium
 , tsMilliseconds
 , tsMinute
 , tsMonth
 , tsQuarter
 , tsSecond
 , tsWeekISO8601
 , tsYear
 , tsYearISO8601

 , nowClock
 , nowStatement
 , nowTransaction

 , bwand
 , bwor
 , bwxor
 , bwnot
 , bwsl
 , bwsr

 , eq
 , eqN
 , elem

 , lt
 , lte
 , gt
 , gte

 , not
 , and
 , ands
 , or
 , ors

 , bool

 , round
 , truncate
 , ceiling
 , floor

 , mod

 ) where

import BasePrelude
  hiding ((<>), null, and, or, not, elem, bool, round, truncate, ceiling, floor, mod)
import Control.Lens
import Control.Exception as Ex
import Data.Fixed (Fixed(..))
import qualified Data.Fixed as Fixed
import Data.Kind
import qualified Data.Aeson as Aeson
import qualified Data.ByteString as B
import qualified Data.ByteString.Builder as BB
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Lazy.Char8 as BL8
import qualified Data.List.NonEmpty as NEL
import Data.Proxy (Proxy(..))
import Data.Semigroup (Semigroup(..))
import Data.Scientific (Scientific)
import Data.Tagged
import qualified Data.Text
import qualified Data.Text.Lazy
import qualified Data.Text.Lazy.Encoding
import qualified Data.UUID.Types
import GHC.Exts (Constraint)
import GHC.Float (float2Double)
import GHC.Real (notANumber, infinity)
import GHC.TypeLits (Nat, KnownNat, type (+))
import qualified GHC.TypeLits as GHC

import qualified Elefun.Sql as Sql
import Elefun.Util (NoInstance)

-------------------------------------------------------------------------------

data PgBool

pgBool :: Bool -> E PgBool
pgBool = \x -> UnsafeE (Sql.ExprLiteral (Sql.LiteralBool (Sql.BoolLit x)))
{-# INLINE pgBool #-}

true :: E PgBool
true = pgBool True
{-# INLINE true #-}

false :: E PgBool
false = pgBool False
{-# INLINE false #-}

-------------------------------------------------------------------------------

data PgBytea

pgBytea :: BL.ByteString -> E PgBytea
pgBytea = \x -> UnsafeE (Sql.ExprLiteral (Sql.LiteralBytes (Sql.BytesLit x)))

-------------------------------------------------------------------------------

data PgCitext

-------------------------------------------------------------------------------

data PgDate

-------------------------------------------------------------------------------

data PgFloat4

pgFloat4 :: Float -> E PgFloat4
pgFloat4 = \x -> UnsafeE (Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitFloat x)))
{-# INLINE pgFloat4 #-}

-------------------------------------------------------------------------------

data PgFloat8

pgFloat8 :: Double -> E PgFloat8
pgFloat8 = \x -> UnsafeE (Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitDouble x)))
{-# INLINE pgFloat8 #-}

-------------------------------------------------------------------------------

data PgInt2

pgInt2 :: Int16 -> E PgInt2
pgInt2 = \x -> UnsafeE (Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 x)))
{-# INLINE pgInt2 #-}

-------------------------------------------------------------------------------

data PgInt4

pgInt4 :: Int32 -> E PgInt4
pgInt4 = \x -> UnsafeE (Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt32 x)))
{-# INLINE pgInt4 #-}

-------------------------------------------------------------------------------

data PgInt8

pgInt8 :: Int64 -> E PgInt8
pgInt8 = \x -> UnsafeE (Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt64 x)))
{-# INLINE pgInt8 #-}

-------------------------------------------------------------------------------

data PgJsonb

-- | Takes a raw @json@ value
unsafePgJsonb :: BL.ByteString -> E PgJsonb
unsafePgJsonb = \bl -> UnsafeE (Sql.ExprRaw (Sql.Raw bl))

-------------------------------------------------------------------------------

data PgJson

-------------------------------------------------------------------------------

data PgText

pgText :: Data.Text.Lazy.Text -> E PgText
pgText = \x -> UnsafeE (Sql.ExprLiteral (Sql.LiteralText (Sql.TextLit x)))
{-# INLINE pgText #-}

-------------------------------------------------------------------------------

data PgUuid

pgUuid :: Data.UUID.Types.UUID -> E PgUuid
pgUuid = \x ->
  let bl = "'" <> Data.UUID.Types.toLazyASCIIBytes x <> "'"
  in UnsafeE (Sql.ExprRaw (Sql.Raw bl))
{-# INLINE pgUuid #-}

-------------------------------------------------------------------------------

data PgTimestamptz

-------------------------------------------------------------------------------
data PgTimestamp

-------------------------------------------------------------------------------
data PgTime

-------------------------------------------------------------------------------

-- | An array where no element is NULL.
data PgArray (a :: Type)

-- | An array where the elements can be NULL.
data PgArrayN (a :: Type)

-- | PostgreSQL @numeric@ type, with @scale@ indicating how many decimal digits
-- does this @numeric@ value support.
--
-- Note that @scale@ is a phantom types are only ever used in the Haskell side,
-- and never on the PostgreSQL side. That is, a @'PgNumeric' s@ type in
-- Haskell maps to a @numeric@ type without a scale specified.
--
-- 'PgNumeric' doesn't support specifying the “precission” of the PostgreSQL
-- @numeric@ type, as there's no use for that precision on the Haskell side and
-- we always support the full precision.
data PgNumeric (scale :: Nat)

-- | Maximum numeric scale for a type.
type family PgNumericScale (t :: k) :: Nat
type instance PgNumericScale Fixed.E0  = 0
type instance PgNumericScale Fixed.E1  = 1
type instance PgNumericScale Fixed.E2  = 2
type instance PgNumericScale Fixed.E3  = 3
type instance PgNumericScale Fixed.E6  = 6
type instance PgNumericScale Fixed.E9  = 9
type instance PgNumericScale Fixed.E12 = 12

-- | Convert a 'Rational' to a @numeric@ column in PostgreSQL. 'notANumber' is
-- supported.
--
-- Returns 'Nothing' in case of positive or negative 'infinity'.
pgRational :: KnownNat s => Rational -> Maybe (E (PgNumeric s))
pgRational x
  | x == infinity    = Nothing
  | x == (-infinity) = Nothing
  | x == notANumber  = Just (UnsafeE (Sql.ExprLiteral (Sql.LiteralNum Sql.NumLitNaN)))
  | otherwise        = Just (pgScientific (fromRational x))

pgScientific :: forall s. KnownNat s => Scientific -> E (PgNumeric s)
pgScientific = \x ->
  UnsafeE (Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitScientific x)))
{-# INLINABLE pgScientific #-}

pgFixed
  :: forall a s
  .  ( KnownNat s
     , Fixed.HasResolution a, GHC.CmpNat s (PgNumericScale a + 1) ~ 'LT)
  => Fixed a -> E (PgNumeric s)
pgFixed = \(MkFixed x) -> case GHC.natVal (Proxy :: Proxy s) of
  0 -> unsaferCoerceE (pgInt2 (fromInteger x))
  _ -> pgScientific (error "TODO")
{-# INLINABLE pgFixed #-}

-------------------------------------------------------------------------------

-- | Only 'PgPrimType' instances are allowed as indexes to @opaleye@'s
-- 'O.Column'.
--
-- You probably won't be adding new 'PgPrimType' instances yourself,
-- unless you are trying to represent a concrete PostgreSQL data type, but even
-- then you might get away with creating 'PgTyped' instances instead.
--
-- Notice that while it's not technically necessary for @a@ to be of kind
-- 'Type', we embrace this kind so as to improve type-inference downstream.
class PgPrimType (a :: Type) where
  pgPrimTypeName :: Proxy a -> Sql.TypeName

instance forall a. PgPrimType a => PgPrimType (PgArray a) where
  pgPrimTypeName _ =
    let Sql.TypeName x = pgPrimTypeName (Proxy @a)
    in Sql.TypeName (x <> "[]")

instance forall a. PgPrimType a => PgPrimType (PgArrayN a) where
  pgPrimTypeName _ = pgPrimTypeName (Proxy @(PgArray a))

instance PgPrimType PgBool where pgPrimTypeName _ = "boolean"
instance PgPrimType PgBytea where pgPrimTypeName _ = "bytea"
instance PgPrimType PgCitext where pgPrimTypeName _ = "citext"
instance PgPrimType PgDate where pgPrimTypeName _ = "date"
instance PgPrimType PgFloat4 where pgPrimTypeName _ = "float4"
instance PgPrimType PgFloat8 where pgPrimTypeName _ = "float8"
instance PgPrimType PgInt2 where pgPrimTypeName _ = "int2"
instance PgPrimType PgInt4 where pgPrimTypeName _ = "int4"
instance PgPrimType PgInt8 where pgPrimTypeName _ = "int8"
instance PgPrimType PgJsonb where pgPrimTypeName _ = "jsonb"
instance PgPrimType PgJson where pgPrimTypeName _ = "json"
instance PgPrimType PgText where pgPrimTypeName _ = "text"
instance PgPrimType PgTimestamptz where pgPrimTypeName _ = "timestamptz"
instance PgPrimType PgTimestamp where pgPrimTypeName _ = "timestamp"
instance PgPrimType PgTime where pgPrimTypeName _ = "time"
instance PgPrimType PgUuid where pgPrimTypeName _ = "uuid"

-- | Notice: 'pgPrimTypeName' is always @"numeric"@ and the @scale@ argument is
-- not rendered in SQL. This is good, don't worry too much. See 'PgNumeric'
instance PgPrimType (PgNumeric scale) where pgPrimTypeName _ = "numeric"

-- | Only 'PgTyped' instances are allowed as indexes to 'E' and 'N'.
--
-- The @'PgType' a ~ 'PgType' ('PgType' a)@ guarantees that @'PgType' a@ is a
-- fixpoint.
--
-- Law 1: The @a@ in @'PgType' a@ is only nominal (i.e., it is possible to
-- safely downcast @'PgType' a@ to @a@ and upcast it back to @'PgType' a@
-- provided the column's value didn't change):
--
-- @
-- 'upcastE' . 'unsafeDowncastE' = 'id'
-- 'unsafeDowncastE' . 'upcastE' = 'id'
-- @
--
-- Notice that while it's not technically necessary for @a@ to be of kind
-- 'Type', we embrace this kind so as to improve type-inference downstream.
class (PgPrimType (PgType a), PgTyped (PgType a), PgType a ~ PgType (PgType a))
  => PgTyped (a :: Type) where
  -- | @'PgType' a@ indicates the primitive PostgreSQL column type that will
  -- ultimately be used as the index to @opaleye@'s 'O.Column'. This could be
  -- @a@ itself, in the case of primitive types such as 'PgInt4', or it could
  -- be something else.
  --
  -- The motivation for this is the same as for “newtype wrappers” in Haskell.
  -- Say in you have this newtype in Haskell:
  --
  -- @
  -- newtype UserId = UserId 'Int32'
  -- @
  --
  -- Now 'UserId' and 'Int32' are different types, even though they share the
  -- same underlying representation 'Int32'. With 'PgTyped' you can achieve
  -- something similar:
  --
  -- @
  -- instance 'PgTyped' UserId where
  --   type 'PgType' UserId = 'PgInt4'
  -- @
  --
  -- With that in place, you won't be able to accidentally mistake @'E'
  -- 'PgInt4'@ for @'E' UserId@ values, yet you will be able to easily
  -- reuse most of the machinery available to @'E' 'PgInt4'@, except:
  --
  -- * You will need a 'O.QueryRunnerColumnDefault' instance to fetch @'E'
  --   UserId@ values from the database as @UserId@:
  --
  --   @
  --   instance 'O.QueryRunnerColumnDefault' 'PgInt4' UserId
  --   @
  --
  --   You might find 'Tisch.Run.qrcWrapped' useful for simple cases like
  --   @UserId@.
  --
  --   Notice that the instance mentions 'PgInt4' directly, not our 'PgTyped'
  --   @UserId@. This is fine, not much would be gained by making the difference
  --   between them at this point, as you still need to do the 'Pg.FromRow'
  --   parsing and any inconsistencies will be uncovered there.
  --
  -- * If you want to reuse the exising 'Num', 'Fractional' or similar instances
  --   for @'E' 'PgInt4', you will need to explicitely ask for it by
  --   instantiating @'PgNum' UserId@, @'PgFractional' UserId@, etc.
  type PgType a :: Type

instance PgTyped PgBool where type PgType PgBool = PgBool
instance PgTyped PgBytea where type PgType PgBytea = PgBytea
instance PgTyped PgCitext where type PgType PgCitext = PgCitext
instance PgTyped PgDate where type PgType PgDate = PgDate
instance PgTyped PgFloat4 where type PgType PgFloat4 = PgFloat4
instance PgTyped PgFloat8 where type PgType PgFloat8 = PgFloat8
instance PgTyped PgInt2 where type PgType PgInt2 = PgInt2
instance PgTyped PgInt4 where type PgType PgInt4 = PgInt4
instance PgTyped PgInt8 where type PgType PgInt8 = PgInt8
instance PgTyped PgJsonb where type PgType PgJsonb = PgJsonb
instance PgTyped PgJson where type PgType PgJson = PgJson
instance PgTyped PgText where type PgType PgText = PgText
instance PgTyped PgTimestamptz where type PgType PgTimestamptz = PgTimestamptz
instance PgTyped PgTimestamp where type PgType PgTimestamp = PgTimestamp
instance PgTyped PgTime where type PgType PgTime = PgTime
instance PgTyped PgUuid where type PgType PgUuid = PgUuid
instance PgTyped (PgNumeric s) where type PgType (PgNumeric s) = PgNumeric s

instance PgTyped a => PgTyped (PgArray a) where type PgType (PgArray a) = PgArray (PgType a)
instance PgTyped a => PgTyped (PgArrayN a) where type PgType (PgArrayN a) = PgArrayN (PgType a)

-------------------------------------------------------------------------------

-- | A nullable expression.
--
-- Notice that while it's not technically necessary for @a@ to be of kind
-- 'Type', we embrace this kind so as to improve type-inference downstream.
newtype N (a :: Type) = UnsafeN { unN :: Sql.Expr }
  deriving (Eq, Show)

-- | Render an 'N' to raw SQL.
rN :: N a -> BB.Builder
rN = Sql.rExpr . unN

-- | Render an 'N' to raw SQL.
rN' :: N a -> String
rN' = BL8.unpack . BB.toLazyByteString . rN

-------------------------------------------------------------------------------

-- | A not-nullable expression.
--
-- Notice that while it's not technically necessary for @a@ to be of kind
-- 'Type', we embrace this kind so as to improve type-inference downstream.
newtype E (a :: Type) = UnsafeE { unE :: Sql.Expr }
  deriving (Eq, Show)

-- | Render an 'E' to raw SQL.
rE :: E a -> BB.Builder
rE = Sql.rExpr . unE

-- | Render an 'E' to raw SQL.
rE' :: E a -> String
rE' = BL8.unpack . BB.toLazyByteString . rE

-- | Like 'unsaferCoerceE', but with a guarantee that the underlying 'PgType's
-- are the same.
unsafeCoerceE :: (PgTyped a, PgTyped b, PgType a ~ PgType b) => E a -> E b
unsafeCoerceE = unsaferCoerceE

-- | Like 'unsaferCastE', but without any explicit mention of the
-- target type.
unsaferCoerceE :: (PgTyped a, PgTyped b) => E a -> E b
unsaferCoerceE = UnsafeE . unE

-- | Unsafely but explicitely coerce one column type to another one by
-- appending a target type name like @::int4@ to the PostgreSQL value, even if
-- it is not guaranteed that the @int4@ type can properly hold the value. Use
-- 'unsafeCoerceE' if you don't want the explicit casting behavior.
unsaferCastE :: forall a b. (PgTyped a, PgTyped b) => E a -> E b
unsaferCastE = \x ->
  UnsafeE (Sql.ExprCast (Sql.Cast (unE x) (pgPrimTypeName (Proxy @(PgType b)))))

-- | Convert a Haskell value to its 'E' representation.
--
-- A a default implementation of 'e' is given for 'Wrapped' instances (see
-- 'eWrapped').
--
-- Notice that as described in the documentation for 'PgTyped', @b@ here could
-- be any 'PgTyped'. For example, following the @UserId@ example mentioned in
-- the documentation for 'PgTyped', you can have:
--
-- @
-- instance 'ToE' @UserId@ @UserId@
-- @
--
-- And that instance would give you @'e' :: UserId -> 'E' UserId@. If
-- @UserId@ is an instance of 'Wrapped', then you get a default implementation
-- for 'e', otherwise you must implement it yourself.
class PgTyped b => ToE (a :: Type) (b :: Type) where
  -- | Convert a constant Haskell value (say, a 'Bool') to its equivalent
  -- PostgreSQL representation (for example, to a @'E' 'PgBool'@, or any
  -- other compatible 'PgTyped').
  --
  -- Some example simplified types:
  --
  -- @
  -- 'e' :: 'Bool' -> 'E' 'PgBool'
  -- 'e' :: 'Int32' -> 'E' 'PgInt4'
  -- @
  e :: a -> E b
  default e :: (Wrapped a, PgTyped b, ToE (Unwrapped a) (PgType b)) => a -> E b
  e = -- Downcasting here is safe due to the Law 1 of 'PgTyped'.
        unsafeDowncastE . e . view _Wrapped'

instance {-# OVERLAPPABLE #-}
  (Wrapped a, PgTyped b, ToE (Unwrapped a) (PgType b)) => ToE a b

instance ToE a b => ToE (Tagged t a) b where e = e . view _Wrapped'
instance ToE String PgText where e = pgText . Data.Text.Lazy.pack
instance ToE Data.Text.Text PgText where e = pgText . Data.Text.Lazy.fromStrict
instance ToE Data.Text.Lazy.Text PgText where e = pgText
instance ToE Char PgText where e = pgText . Data.Text.Lazy.singleton
instance ToE Bool PgBool where e = pgBool
instance ToE Word8 PgInt2 where e = fromIntegral
instance ToE Word8 PgInt4 where e = fromIntegral
instance ToE Word8 PgInt8 where e = fromIntegral
instance ToE Word16 PgInt4 where e = fromIntegral
instance ToE Word16 PgInt8 where e = fromIntegral
instance ToE Word32 PgInt8 where e = fromIntegral
instance ToE Int8 PgInt2 where e = fromIntegral
instance ToE Int8 PgInt4 where e = fromIntegral
instance ToE Int8 PgInt8 where e = fromIntegral
instance ToE Int16 PgInt2 where e = fromIntegral
instance ToE Int16 PgInt4 where e = fromIntegral
instance ToE Int16 PgInt8 where e = fromIntegral
instance ToE Int32 PgInt4 where e = fromIntegral
instance ToE Int32 PgInt8 where e = fromIntegral
instance ToE Int64 PgInt8 where e = fromIntegral
instance ToE Float PgFloat4 where e = pgFloat4
instance ToE Float PgFloat8 where e = pgFloat8 . float2Double
instance ToE Double PgFloat8 where e = pgFloat8
instance ToE B.ByteString PgBytea where e = pgBytea . BL.fromStrict
instance ToE BL.ByteString PgBytea where e = pgBytea
-- instance ToE Data.Time.UTCTime PgTimestamptz where e = E . O.pgUTCTime
-- instance ToE Data.Time.LocalTime PgTimestamp where e = E . O.pgLocalTime
-- instance ToE Data.Time.TimeOfDay PgTime where e = E . O.pgTimeOfDay
-- instance ToE Data.Time.Day PgDate where e = E . O.pgDay
instance ToE Data.UUID.Types.UUID PgUuid where e = pgUuid
-- instance ToE (Data.CaseInsensitive.CI String) PgCitext where e = E . O.unsafeCoerceColumn . O.pgString . Data.CaseInsensitive.original
-- instance ToE (Data.CaseInsensitive.CI Data.Text.Text) PgCitext where e = E . O.pgCiStrictText
-- instance ToE (Data.CaseInsensitive.CI Data.Text.Lazy.Text) PgCitext where e = E . O.pgCiLazyText
instance ToE Aeson.Value PgJson where e = unsaferCoerceE . pgText . Data.Text.Lazy.Encoding.decodeUtf8 . Aeson.encode
instance ToE Aeson.Value PgJsonb where e = unsaferCoerceE . pgText . Data.Text.Lazy.Encoding.decodeUtf8 . Aeson.encode

instance GHC.KnownNat s => ToE Integer (PgNumeric s) where e = fromInteger
instance GHC.KnownNat s => ToE Scientific (PgNumeric s) where e = pgScientific
instance GHC.KnownNat s => ToE Rational (PgNumeric s) where e = pgScientific . fromRational
instance
  ( GHC.KnownNat s
  , Fixed.HasResolution a, GHC.CmpNat s (PgNumericScale a GHC.+ 1) ~ 'LT
  ) => ToE (Fixed a) (PgNumeric s) where e = pgFixed

instance {-# OVERLAPPABLE #-} forall a b. ToE a b => ToE [a] (PgArray b) where
  e = pgArray . map (e :: a -> E b)

-- | Build a @'E' ('PgArray' x)@ from any 'Foldable'.
--
-- The return type is not fixed to @'E' ('PgArray' x)@ so that you can
-- easily use 'pgArray' as part of the implementation for 'e' (see instance
-- @'ToE' [a] ('PgArray' p)@ as an example of this).
pgArray
 :: forall f a as
 .  (Foldable f, PgTyped a, PgTyped as, PgType as ~ PgArray (PgType a))
 => f (E a) -> E as
pgArray xs = UnsafeE $ Sql.ExprArray $ case NEL.nonEmpty (toList xs) of
   Nothing -> Sql.ArrayEmpty (pgPrimTypeName (Proxy @(PgType a)))
   Just ns -> Sql.Array (fmap unE ns)

-- | Conversions from 'Int' are explicitely disabled.
instance {-# OVERLAPPING #-}
  ( PgTyped a
  , GHC.TypeError
      ('GHC.Text "ToE conversions from Int to E are disabled because the size"
       'GHC.:$$: 'GHC.Text "of Int is machine-dependent, which is likely to cause you maintenance"
       'GHC.:$$: 'GHC.Text "problems in the future. Be explicit about the size of your integer,"
       'GHC.:$$: 'GHC.Text "use one Int8, Int16, Int32, Int64 from Data.Int.")
  ) => ToE Int a where e = undefined

-- | Conversions from 'Word' are explicitely disabled.
instance {-# OVERLAPPING #-}
  ( PgTyped a
  , GHC.TypeError
      ('GHC.Text "ToE conversions from Word to E are disabled because the size"
       'GHC.:$$: 'GHC.Text "of Word is machine-dependent, which is likely to cause you maintenance"
       'GHC.:$$: 'GHC.Text "problems in the future. Be explicit about the size of your integer,"
       'GHC.:$$: 'GHC.Text "use one of Word8, Word16, Word32 from Data.Word.")
  ) => ToE Word a where e = undefined

-------------------------------------------------------------------------------

-- | @'CastE' a b@ says that @'E' a@ can be safely cast to @'E' b@
-- using 'castE'.
--
-- Notice that an explicit cast will be performed on the PostgreSQL side. For
-- example, using @'castE' :: 'E' 'PgUuid' -> 'E' 'PgText'@ will
-- explicitely add @::text@ to the value in an @uuid@ column. This allows for
-- much more interesting and predictable conversions between different types
-- compared to 'unsafeCoerceE'.
--
-- Notice that while it's not technically necessary for @a@ and @b@ to be of
-- kind 'Type', we embrace this kind so as to improve type-inference downstream.
class (PgTyped a, PgTyped b) => CastE (a :: Type) (b :: Type) where

instance CastE PgCitext PgText
instance CastE PgText PgCitext
instance CastE PgUuid PgText
instance CastE PgUuid PgCitext
instance CastE PgInt2 PgText
instance CastE PgInt2 PgCitext
instance CastE PgInt2 PgInt4
instance CastE PgInt2 PgInt8
instance CastE PgInt2 (PgNumeric s)
instance CastE PgInt4 PgText
instance CastE PgInt4 PgCitext
instance CastE PgInt4 PgInt8
instance CastE PgInt4 (PgNumeric s)
instance CastE PgInt8 (PgNumeric s)

instance (GHC.CmpNat s (s' GHC.+ 1) ~ 'LT) => CastE (PgNumeric s) (PgNumeric s')

-- Shooting yourself in the foot? I will help you.

type family TypeErrorRange a b :: Constraint where
  TypeErrorRange a b = GHC.TypeError
    ('GHC.Text "If really want to explicitly cast " 'GHC.:<>: 'GHC.ShowType a 'GHC.:<>:
     'GHC.Text " to " 'GHC.:<>: 'GHC.ShowType b 'GHC.:<>:
     'GHC.Text " then use 'unsaferCastE'." 'GHC.:$$:
     'GHC.Text "The value will silently overflow if it is outside the range of "
     'GHC.:<>: 'GHC.ShowType b)
instance TypeErrorRange PgInt4 PgInt2 => CastE PgInt4 PgInt2
instance TypeErrorRange PgInt8 PgInt2 => CastE PgInt8 PgInt2
instance TypeErrorRange PgInt8 PgInt4 => CastE PgInt8 PgInt4

type family TypeErrorTimeCasting a b c :: Constraint where
  TypeErrorTimeCasting a b c = GHC.TypeError
    ('GHC.Text "Do not cast " 'GHC.:<>: 'GHC.ShowType a 'GHC.:<>:
     'GHC.Text " to " 'GHC.:<>: 'GHC.ShowType b 'GHC.:$$: 'GHC.Text c)
instance TypeErrorTimeCasting PgDate PgTimestamp "Read Section 8.5 of the PostgreSQL documentation." => CastE PgDate PgTimestamp
instance TypeErrorTimeCasting PgDate PgTimestamptz "Read Section 8.5 of the PostgreSQL documentation." => CastE PgDate PgTimestamptz
instance TypeErrorTimeCasting PgTimestamp PgDate "Use timestampDate." => CastE PgTimestamp PgDate
instance TypeErrorTimeCasting PgTimestamp PgTime "Use timestampTime." => CastE PgTimestamp PgTime
instance TypeErrorTimeCasting PgTimestamp PgTimestamptz "Use toTimestamptz instead." => CastE PgTimestamp PgTimestamptz
instance TypeErrorTimeCasting PgTimestamptz PgDate "Use (timestampDate . toTimestamp zone)" => CastE PgTimestamptz PgDate
instance TypeErrorTimeCasting PgTimestamptz PgTime "Use (timestampTime . toTimestamp zone)" => CastE PgTimestamptz PgTime
instance TypeErrorTimeCasting PgTimestamptz PgTimestamp "Use toTimestamp instead." => CastE PgTimestamptz PgTimestamp

-- | Safely and explicitely cast one column type to another one. See 'CastE'.
castE :: CastE a b => E a -> E b
castE = unsaferCastE

-- | Safe upcasting.
upcastE :: PgTyped a => E a -> E (PgType a)
upcastE = unsafeCoerceE

-- | Unsafe downcasting.
unsafeDowncastE :: PgTyped a => E (PgType a) -> E a
unsafeDowncastE = unsafeCoerceE

--------------------------------------------------------------------------------

-- | This is used to convey the idea that if one of the operators is NULL, then
-- the result will be NULL as well.
data LiftNull = LiftNull deriving (Eq, Show)

opInfix :: Sql.Op 'Sql.Infix -> E a -> E b -> E c
{-# INLINE opInfix #-}
opInfix o = \a b -> UnsafeE (Sql.ExprOpApp (Sql.OpAppInfix (unE a) o (unE b)))

opPrefix :: Sql.Op 'Sql.Prefix -> E a -> E b
{-# INLINE opPrefix #-}
opPrefix o = \a -> UnsafeE (Sql.ExprOpApp (Sql.OpAppPrefix o (unE a)))

opPostfix :: Sql.Op 'Sql.Postfix -> E a -> E b
{-# INLINE opPostfix #-}
opPostfix o = \a -> UnsafeE (Sql.ExprOpApp (Sql.OpAppPostfix (unE a) o))

fun :: Sql.Name -> [Sql.Expr] -> E a
{-# INLINE fun #-}
fun f xs = UnsafeE (Sql.ExprFunApp (Sql.FunApp f xs []))

funN :: Sql.Name -> [Sql.Expr] -> N a
funN f xs = UnsafeN (unE (fun f xs))

cond :: E a -> [(E PgBool, E a)] -> E a
{-# INLINE cond #-}
cond = \els xs -> case NEL.nonEmpty xs of
  Nothing -> els
  Just ns -> UnsafeE (Sql.ExprCase (Sql.Case (fmap (unE *** unE) ns) (unE els)))

--------------------------------------------------------------------------------

class (PgTyped a, PgNum (PgType a)) => PgNum (a :: Type) where
  pgFromInteger :: Integer -> E a

instance PgNum PgInt2 where
  pgFromInteger = pgInt2 . fromInteger
instance PgNum PgInt4 where
  pgFromInteger = pgInt4 . fromInteger
instance PgNum PgInt8 where
  pgFromInteger = pgInt8 . fromInteger
instance PgNum PgFloat4 where
  pgFromInteger = pgFloat4 . fromInteger
instance PgNum PgFloat8 where
  pgFromInteger = pgFloat8 . fromInteger
instance KnownNat s => PgNum (PgNumeric s) where
  pgFromInteger = pgScientific . fromInteger


instance forall a. PgNum a => Num (E a) where
  fromInteger = pgFromInteger
  (*) = opInfix "*"
  (+) = opInfix "+"
  (-) = opInfix "-"
  abs = \x -> fun "abs" [unE x]
  negate = opPrefix "-"
  signum c =
    let gt' = opInfix "<" :: E a -> E a -> E PgBool
    in cond 0 [(gt' 0 c, 1), (gt' c 0, -1)]

--------------------------------------------------------------------------------

class (PgTyped a, PgFractional (PgType a)) => PgFractional (a :: Type) where
  pgFromRational :: Rational -> E a

instance PgFractional PgFloat4 where
  pgFromRational = error "TODO"

instance PgFractional PgFloat8 where
  pgFromRational = error "TODO"

-- | WARNING: 'pgFromRational' throws 'Ex.RatioZeroDenominator' if given a
-- positive or negative 'infinity'.
instance KnownNat s => PgFractional (PgNumeric s) where
  pgFromRational = maybe (Ex.throw Ex.RatioZeroDenominator) id . pgRational

--------------------------------------------------------------------------------

instance Semigroup (E PgText)
instance Monoid (E PgText) where
  mempty = pgText mempty
  mappend = opInfix "||"

---

-- instance Semigroup (E PgCitext)
-- instance Monoid (E PgCitext) where
--   mempty = e (Data.CaseInsensitive.mk "")
--   mappend ea eb =
--     (mappend (unsaferCoerceE ea :: E PgText)
--              (unsaferCoerceE eb :: E PgText))

---
instance Semigroup (E PgBytea) where
instance Monoid (E PgBytea) where
  mempty = pgBytea mempty
  mappend = opInfix "||"

---
instance PgTyped a => Semigroup (E (PgArray a))

instance forall a. PgTyped a => Monoid (E (PgArray a)) where
  mempty = pgArray ([] :: [E a])
  mappend ea eb = fun "array_cat" [unE ea, unE eb]

--------------------------------------------------------------------------------

-- | Sql operator @%@
mod :: PgNum a => E a -> E a -> E a
mod = opInfix "%"

-------------------------------------------------------------------------------

-- | A 'PgIntegral' is guaranteed to be an integral type.
class PgTyped a => PgIntegral a

instance PgIntegral PgInt2
instance PgIntegral PgInt4
instance PgIntegral PgInt8
instance PgIntegral (PgNumeric 0)

truncate :: (PgFloating a, PgIntegral b) => E a -> E b
truncate = \x -> fun "truncate" [unE x]

round :: (PgFloating a, PgIntegral b) => E a -> E b
round = \x -> fun "round" [unE x]

ceiling :: (PgFloating a, PgIntegral b) => E a -> E b
ceiling = \x -> fun "ceil" [unE x]

floor :: (PgFloating a, PgIntegral b) => E a -> E b
floor = \x -> fun "floor" [unE x]

-------------------------------------------------------------------------------

-- | A @'PgFractional' a@ instance gives you a @'Fractional' ('E' a)@ instance
-- for free.
instance (PgTyped a, PgFractional a, Num (E a)) => Fractional (E a) where
  fromRational = pgFromRational
  (/) = opInfix "/"

-------------------------------------------------------------------------------
-- | A 'PgFloating' instance gives you 'Floating'-support.
class (PgTyped a, PgFractional a, PgFloating (PgType a)) => PgFloating a

instance PgFloating PgFloat4
instance PgFloating PgFloat8

-- | @2.718281828459045@
euler's :: PgFloating a => E a
euler's = unsaferCastE (e (2.718281828459045 :: Double) :: E PgFloat8)
{-# INLINE euler's #-}

instance
  ( PgTyped a, PgFloating a, PgFloating (E a), Fractional (E a)
  ) => Floating (E a) where
  pi = fun "pi" []
  exp = \x -> fun "exp" [unE x]
  log = \x -> fun "log" [unE x]
  sqrt = \x -> fun "sqrt" [unE x]
  (**) = \base x -> fun "power" [unE base, unE x]
  logBase = \base x -> fun "log" [unE base, unE x]
  sin = \x -> fun "sin" [unE x]
  cos = \x -> fun "cos" [unE x]
  tan = \x -> fun "tan" [unE x]
  asin = \x -> fun "asin" [unE x]
  acos = \x -> fun "acos" [unE x]
  atan = \x -> fun "atan" [unE x]
  -- Not the most efficient implementations follow, but PostgreSQL doesn't
  -- provide builtin support for hyperbolic functions. We add these for
  -- completeness, so that we can implement the 'Floating' typeclass in full.
  sinh x = ((euler's ** x) - (euler's ** (negate x))) / fromInteger 2
  cosh x = ((euler's ** x) + (euler's ** (negate x))) / fromInteger 2
  tanh x = ((euler's ** x) - (euler's ** (negate x)))
         / ((euler's ** x) + (euler's ** (negate x)))
  asinh x = log (x + sqrt ((x ** fromInteger 2) + fromInteger 1))
  acosh x = log (x + sqrt ((x ** fromInteger 2) - fromInteger 1))
  atanh x = log ((fromInteger 1 + x) / (fromInteger 1 - x)) / fromInteger 2

-------------------------------------------------------------------------------

-- Booleans.

-- | Like 'Prelude.bool'.
--
-- @
-- 'bool' f t b  ~~~  if b then t else f
-- @
bool :: E a -> E a -> E PgBool -> E a
bool f t = \b -> cond f [(b, t)]

-- | Logical NOT.
not :: E PgBool -> E PgBool
not = opPrefix "NOT"

-- | Logical OR. See 'eq' for possible argument types.
or :: E PgBool -> E PgBool -> E PgBool
or = opInfix "OR"

-- | Whether any of the given 'PgBool's is true.
--
-- Notice that 'lor' is more general that 'lors', as it doesn't restrict @e@.
--
-- Mnemonic reminder: Logical ORs.
ors :: Foldable f => f (E PgBool) -> E PgBool
ors = foldl' or false

-- Logical AND. See 'eq' for possible argument types.
and :: E PgBool -> E PgBool -> E PgBool
and = opInfix "AND"

-- | Whether all of the given 'PgBool's are true.
--
-- Notice that 'land' is more general that 'lands', as it doesn't restrict
-- @e@.
--
-- Mnemonic reminder: Logical ANDs.
ands :: Foldable f => f (E PgBool) -> E PgBool
ands = foldl' and true

--------------------------------------------------------------------------------
-- Equality

-- | A @'PgEq' a@ instance states that @a@ can be compared for equality.
class PgTyped a => PgEq a

instance PgEq PgBool
instance PgEq PgBytea
instance PgEq PgCitext
instance PgEq PgDate
instance PgEq PgFloat4
instance PgEq PgFloat8
instance PgEq PgInt2
instance PgEq PgInt4
instance PgEq PgInt8
instance PgEq PgJsonb
instance PgEq PgJson
instance PgEq PgText
instance PgEq PgTimestamptz
instance PgEq PgTimestamp
instance PgEq PgTime
instance PgEq PgUuid
instance PgEq (PgNumeric s)

-- | Whether two values are equal.
--
-- Mnemonic reminder: EQual.
eq :: PgEq a => E a -> E a -> E PgBool
eq = opInfix "="

-- | Whether two nullable values are equal, but returning 'isNull' in case any of
-- the given arguments is 'isNull'. This is more performant than using @'liftN2'
-- 'eq'@ or similar, and it is particularly important when used as the condition
-- for an 'Elefun.innerJoin', 'Elefun.crossJoin', 'Elefun.leftJoin', or 'Elefun.fullJoin'.
--
-- Mnemonic reminder: EQual, Nullable.

-- TODO: Remove this function in favor of an optimized @'liftN2' 'eq'@.
eqN :: PgEq a => N a -> N a -> N PgBool
eqN = \a b -> UnsafeN (Sql.ExprOpApp (Sql.OpAppInfix (unN a) "=" (unN b)))

-- | Like 'Prelude.elem'. Whether the given value is a member of the given
-- collection.
elem :: (PgEq a, Foldable f) => E a -> f (E a) -> E PgBool
elem x xs = case NEL.nonEmpty (toList xs) of
  Nothing -> false
  Just ns -> UnsafeE (Sql.ExprIn (Sql.In (unE x) (unE <$> ns)))

--------------------------------------------------------------------------------
-- Ordering

-- | A 'PgOrd' instance says that @a@ has an ordering. See 'orderBy'.
class (PgTyped a, PgOrd (PgType a)) => PgOrd a

instance PgOrd PgInt2
instance PgOrd PgInt4
instance PgOrd PgInt8
instance PgOrd PgFloat4
instance PgOrd PgFloat8

-- | Whether the first argument is less than the second.
--
-- Mnemonic reminder: Less Than.
lt :: PgOrd a => E a -> E a -> E PgBool
lt = opInfix "<"

-- | Whether the first argument is less than or equal to the second.
--
-- Mnemonic reminder: Less Than or Equal.
lte :: PgOrd a => E a -> E a -> E PgBool
lte = opInfix "<="

-- | Whether the first argument is greater than the second.
--
-- Mnemonic reminder: Greater Than.
gt :: PgOrd a => E a -> E a -> E PgBool
gt = opInfix ">"

-- | Whether the first argument is greater than or equal to the second.
--
-- Mnemonic reminder: Greater Than or Equal.
gte :: PgOrd a => E a -> E a -> E PgBool
gte = opInfix ">="

--------------------------------------------------------------------------------
-- Bitwise

-- | Only 'PgBitwise' instance can be used with bitwise operators 'btwand',
-- 'bwor', 'bwxor', 'bwnot', 'bwsl' and 'bwsr'.
class PgTyped a => PgBitwise a

instance PgBitwise PgInt2
instance PgBitwise PgInt4
instance PgBitwise PgInt8
-- instance PgBitwise PgBitstring ?

-- | Bitwise AND. Sql operator: @&@
bwand :: PgBitwise a => E a -> E a -> E a
bwand = opInfix "&"

-- | Bitwise OR. Sql operator: @|@
bwor :: PgBitwise a => E a -> E a -> E a
bwor = opInfix "|"

-- | Bitwise XOR. Sql operator: @#@
bwxor :: PgBitwise a => E a -> E a -> E a
bwxor = opInfix "#"

-- | Bitwise NOT. Sql operator: @~@
bwnot :: PgBitwise a => E a -> E a
bwnot = opPrefix "~"

-- | Bitwise shift left. Sql operator: @<<@
--
-- @'bwsl' a n@ shifts @a@ to the right @n@ positions. Translates to @a << n@ in
-- the generated SQL.
bwsl :: (PgBitwise a, PgIntegral b) => E a -> E b -> E a
bwsl = opInfix "<<"

-- | Bitwise shift right. Sql operator: @>>@
--
-- @'bwsr' a n@ shifts @a@ to the right @n@ positions. Translates to @a >> n@ in
-- the generated SQL.
bwsr :: (PgBitwise a, PgIntegral b) => E a -> E b -> E a
bwsr = opInfix ">>"

--------------------------------------------------------------------------------
-- Time

-- Convert a PostgreSQL @timestamptz@ to a @timestamp@ at a given timezone.
--
-- Notice that a @timestamp@ value is usually meaningless unless you also know
-- the timezone where that @timestamp@ happens. In other words, you should
-- store the passed in @'E' zone@ somewhere.
--
-- Warning: Dealing with @timestamp@ values in PostgreSQL is very error prone
-- unless you really know what you are doing.  Quite likely you shouldn't be
-- using @timestamp@ values in PostgreSQL unless you are storing distant dates
-- in the future for which the precise UTC time can't be known (e.g., can you
-- tell the UTC time for January 1 4045, 00:00:00 in Peru? Me neither, as I
-- have no idea in what timezone Peru will be in year 4045, so I can't convert
-- that to UTC).
--
-- Law 1: Provided the timezone database information stays the same, the
-- following equality holds:
--
-- @
-- 'toTimestamptz' zone . 'toTimestamp' zone === 'id'
-- 'toTimestamp' zone . 'toTimestamptz' zone === 'id'
-- @
toTimestamptz
  :: ( PgTyped zone, PgType zone ~ PgText
     , PgTyped a, PgType a ~ PgTimestamptz
     , PgTyped b, PgType b ~ PgTimestamp )
  => E zone -> E a -> E b
toTimestamptz = \zone a -> fun "timezone" [unE zone, unE a]

-- Convert a PostgreSQL @timestamp@ to a @timestamptz@, making the assumption
-- that the given @timestamp@ happens at the given timezone.
--
-- Law 1: Provided the timezone database information stays the same, the
-- following equality holds:
--
-- @
-- 'toTimestamptz' zone . 'toTimestamp' zone === 'id'
-- 'toTimestamp' zone . 'toTimestamptz' zone === 'id'
-- @
toTimestamp
  :: ( PgTyped zone, PgType zone ~ PgText
     , PgTyped a, PgType a ~ PgTimestamp
     , PgTyped b, PgType b ~ PgTimestamptz )
  => E zone -> E a -> E b
toTimestamp = \zone a -> fun "timezone" [unE zone, unE a]

fun__date_part :: (PgTyped tsy, PgTyped b) => String -> E tsy -> E b
fun__date_part p = \x -> fun "date_part"
   [unE (pgText (Data.Text.Lazy.pack p)), unE x]

tstzEpoch :: E PgTimestamptz -> E PgFloat8
tstzEpoch = fun__date_part "epoch"

tsCentury :: (PgIntegral b) => E PgTimestamp -> E b
tsCentury = fun__date_part "century"

tsDay :: (PgIntegral b) => E PgTimestamp -> E b
tsDay = fun__date_part "day"

tsDayOfTheWeek :: (PgIntegral b) => E PgTimestamp -> E b
tsDayOfTheWeek = fun__date_part "dow"

tsDayOfTheWeekISO8601 :: (PgIntegral b) => E PgTimestamp -> E b
tsDayOfTheWeekISO8601 = fun__date_part "isodow"

tsDayOfTheYear :: (PgIntegral b) => E PgTimestamp -> E b
tsDayOfTheYear = fun__date_part "doy"

tsDecade :: (PgIntegral b) => E PgTimestamp -> E b
tsDecade = fun__date_part "decade"

tsHour :: (PgIntegral b) => E PgTimestamp -> E b
tsHour = fun__date_part "hour"

tsMicroseconds :: (PgIntegral b, CastE PgInt4 b) => E PgTimestamp -> E b
tsMicroseconds = fun__date_part "microseconds"

tsMillenium :: (PgIntegral b) => E PgTimestamp -> E b
tsMillenium = fun__date_part "millenium"

tsMilliseconds :: (PgIntegral b, CastE PgInt4 b) => E PgTimestamp -> E b
tsMilliseconds = fun__date_part "milliseconds"

tsMinute :: (PgIntegral b) => E PgTimestamp -> E b
tsMinute = fun__date_part "minute"

tsMonth :: (PgIntegral b) => E PgTimestamp -> E b
tsMonth = fun__date_part "month"

tsQuarter :: (PgIntegral b) => E PgTimestamp -> E b
tsQuarter = fun__date_part "quarter"

tsSecond :: (PgIntegral b) => E PgTimestamp -> E b
tsSecond = fun__date_part "second"

tsWeekISO8601 :: (PgIntegral b) => E PgTimestamp -> E b
tsWeekISO8601 = fun__date_part "week"

tsYear :: (PgIntegral b) => E PgTimestamp -> E b
tsYear = fun__date_part "year"

tsYearISO8601 :: (PgIntegral b) => E PgTimestamp -> E b
tsYearISO8601 = fun__date_part "isoyear"

-- | Time when the current transaction started.
--
-- Sql function: @transaction_timestamp()@, @now()@.
nowTransaction :: E PgTimestamptz
nowTransaction = fun "transaction_timestamp" []

-- | Time when the current statement started.
--
-- SqlFunction: @statement_timestamp()@.
nowStatement :: E PgTimestamptz
nowStatement = fun "statement_timestamp" []

-- | Current clock time.
--
-- SqlFunction: @clock_timestamp()@.
nowClock :: E PgTimestamptz
nowClock = fun "clock_timestamp" []

--------------------------------------------------------------------------------
-- Regular expressions

-- | Whether the given regular expression matches the given text.
--
-- Sql operator: @~@
reMatch
  :: (PgText ~ regex, PgText ~ source)
  => E regex
  -> E source
  -> E PgBool  -- ^ Is there a match?
reMatch = flip (opInfix "~")

-- | Extract a substring matching the given regular expression. If there is no
-- match, then @NULL@ is returned.
--
-- Sql function: @substring()@.
reSub
  :: (PgText ~ regex, PgText ~ source)
  => E regex
  -- ^ Regular expression. If the pattern contains any parentheses, the portion
  -- of the text that matched the first parenthesized subexpression (the one
  -- whose left parenthesis comes first) is returned. See Section 9.1 of the
  -- PostgreSQL manual to understand the syntax.
  -> E source
  -> N PgText -- ^ Possibly matched substring.
reSub re' a = funN "substring" [unE a, unE re']

-- | Replaces with @replacement@ in the given @source@ string the /first/
-- substring matching the regular expression @regex@.
--
-- Sql function: @regexp_replace(_, _, _)@.
reReplace
  :: (PgText ~ regex, PgText ~ source, PgText ~ replacement)
  => E regex
  -- ^ Regular expression. See Section 9.1 of the PostgreSQL manual to
  -- understand the syntax.
  -> E replacement
  -- ^ Replacement expression. See Section 9.1 of the PostgreSQL manual to
  -- understand the syntax.
  -> E source
  -> E PgText
reReplace re' rep s = fun "regexp_replace" [unE s, unE re', unE rep]

-- | Like 'reReplace', but replaces /all/ of the substrings matching the
-- pattern, not just the first one.
--
-- Sql function: @regexp_replace(_, _, _, 'g')@.
reReplaceg
  :: (PgText ~ regex, PgText ~ source, PgText ~ replacement)
  => E regex
  -- ^ Regular expression. See Section 9.1 of the PostgreSQL manual to
  -- understand the syntax.
  -> E replacement
  -- ^ Replacement expression. See Section 9.1 of the PostgreSQL manual to
  -- understand the syntax.
  -> E source
  -> E PgText
reReplaceg re' rep s = fun "regexp_replace"
   [unE s, unE re', unE rep, unE (pgText "g")]

-- | Split the @source@ string using the given Regular Expression as a
-- delimiter.
--
-- If there is no match to the pattern, the function an array with just one
-- element: the original string. If there is at least one match,
-- for each match it returns the text from the end of the last match (or the
-- beginning of the string) to the beginning of the match. When there are no
-- more matches, it returns the text from the end of the last match to the end
-- of the string.
--
-- Sql function: @regexp_split_to_array(source, regexp)@.
reSplit
  :: (PgText ~ regex, PgText ~ source)
  => E regex
  -- ^ Regular expression. See Section 9.1 of the PostgreSQL manual to
  -- understand the syntax.
  -> E source
  -> E (PgArray PgText)
reSplit re' s = fun "regexp_split_to_array" [unE s, unE re']


--------------------------------------------------------------------------------

-- | Like @opaleye@'s @'O.Column' ('O.Nullable' x)@, but with @x@ guaranteed
-- to be not-'O.Nullable'.
--
-- Think of @'N' a@ as @'Maybe' ('E' a)@, with @NULL@ being analogous to
-- the 'Nothing' constructor and 'en' being analogous to the 'Just'
-- constructor.
--
-- Build safely using 'n', 'en' or 'N'.
--
-- /Notice that 'N' is very different from 'Col': 'Col' is used to describe/
-- /the properties of a column at compile time. 'N' is used at runtime/
-- /for manipulating with values stored in columns./
--
-- We do not use @opaleye@'s @'O.Column' ('O.Nullable' x)@, instead we use
-- @'N' y@ where @x ~ 'PgType' y@. This is where we drift a bit appart from
-- Opaleye. See https://github.com/tomjaguarpaw/haskell-opaleye/issues/97

-- | Build a 'N' from a Haskell term, where 'Nothing' means @NULL@.
n :: ToE a b => Maybe a -> N b
n = \case Just x -> en (e x)
          Nothing -> null

-- | Convert a 'E' to a 'N'.
en :: E a -> N a
en = \x -> UnsafeN (unE x)

-- | Convert a 'N' to a 'E'.
--
-- This function behaves as 'Data.Maybe.fromMaybe'.
--
-- @
-- 'fromN' ka na == 'matchN' ka id na
-- @
fromN :: PgTyped a => E a -> N a -> E a
fromN x = matchN x id

-- | Case analysis for 'N'.
--
-- This function behaves as 'Prelude.maybe' for 'Maybe': If @'N' a@ is
-- @NULL@, then evaluate to the first argument, otherwise it applies the given
-- function to the @'E' a@ underlying the given @'N' a@.
matchN
  :: (PgTyped a, PgTyped b) => E b -> (E a -> E b) -> N a -> E b
matchN eb0 f na = UnsafeE (Sql.ExprCase (
  Sql.Case (pure (unE (isNull na), unE eb0)) (unE (f (UnsafeE (unN na))))))

-- | Like 'fmap' for 'Maybe'.
--
-- Apply the given function to the underlying @('E' a)@ only as long as the
-- given @('N' a)@ is not @NULL@, otherwise, evaluates to @NULL@.
mapN :: (PgTyped a, PgTyped b) => (E a -> E b) -> N a -> N b
mapN f = \na -> bindN na (\ea -> en (f ea))

-- | 'mapN' with the arguments flipped.
forN :: (PgTyped a, PgTyped b) => N a -> (E a -> E b) -> N b
forN kna f = mapN f kna

-- | Like 'Control.Applicative.liftA2' for 'Maybe'.
--
-- Lift a function working on 'E's to a function working on 'N's.
liftN2
  :: (PgTyped a, PgTyped b, PgTyped c)
  => (E a -> E b -> E c)
  -> (N a -> N b -> N c)
liftN2 f = \na nb -> bindN na (\ea -> bindN nb (\eb -> en (f ea eb)))

-- | Monadic bind like the one for 'Maybe'.
--
-- Apply the given function to the underlying @('E' a)@ only as long as the
-- given @('N' a)@ is not @NULL@, otherwise, evaluates to @NULL@.
bindN :: (PgTyped a, PgTyped b) => N a -> (E a -> N b) -> N b
bindN na f = UnsafeN (Sql.ExprCase (
  Sql.Case (pure (unE (isNull na), Sql.ExprNull)) (unN (f (UnsafeE (unN na))))))
{-# INLINE bindN #-}

-- | Like @('<|>') :: 'Maybe' a -> 'Maybe' a -> 'Maybe' a@.
--
-- Evaluates to the first argument if it is not @NULL@, otherwise
-- evaluates to the second argument.
altN :: PgTyped a => N a -> N a -> N a
altN l r = funN "COALESCE" [unN l, unN r]

-- | Whether a 'N' is @NULL@.
isNull :: N a -> E PgBool
isNull = \x -> opPostfix "IS NULL" (UnsafeE (unN x))

-- | The @NULL@ SQL expression.
null :: PgTyped a => N a
null = UnsafeN Sql.ExprNull

--------------------------------------------------------------------------------

-- | Behaves as the 'Monoid' instance for 'Maybe'.
instance (PgTyped a, Monoid (E a)) => Monoid (N a) where
  mempty = en mempty
  mappend = liftN2 mappend

---

instance {-# OVERLAPPING #-} (NoInstance Num (N a)) => Num (N a) where
  fromInteger = undefined
  abs = undefined
  signum = undefined
  (*) = undefined
  (+) = undefined
  (-) = undefined

instance {-# OVERLAPPING #-}
  ( NoInstance Fractional (N a)
  , Num (N a)
  ) => Fractional (N a) where
    fromRational = undefined
    recip = undefined

instance {-# OVERLAPPING #-}
  ( NoInstance Floating (N a)
  , Num (N a)
  , Fractional (N a)
  ) => Floating (N a) where
    pi = undefined
    exp = undefined
    log = undefined
    sin = undefined
    cos = undefined
    asin = undefined
    acos = undefined
    atan = undefined
    sinh = undefined
    cosh = undefined
    asinh = undefined
    acosh = undefined
    atanh = undefined

---

instance forall a b. ToE a b => ToE [Maybe a] (PgArrayN b) where
  e = \yas -> pgArrayN (map (n :: Maybe a -> N b) yas)

-- | Build a @'E' ('PgArrayN' x)@ from any 'Foldable'.
--
-- The return type is not fixed to @'E' ('PgArrayN' x)@ so that you can
-- easily use 'pgArrayN' as part of the implementation for 'e' (see instance
-- @'ToE' ['Maybe' a] ('PgArrayN' p)@ as an example of this).
pgArrayN
 :: forall f a as
 .  (Foldable f, PgTyped a, PgTyped as, PgType as ~ PgArrayN (PgType a))
 => f (N a) -> E as
pgArrayN xs = UnsafeE $ Sql.ExprArray $ case NEL.nonEmpty (toList xs) of
   Nothing -> Sql.ArrayEmpty (pgPrimTypeName (Proxy @(PgType a)))
   Just ns -> Sql.Array (fmap unN ns)


