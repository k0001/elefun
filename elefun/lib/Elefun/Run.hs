{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_HADDOCK hide #-}

module Elefun.Run where

import BasePrelude hiding (insert, delete)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except (runExceptT, throwE)
import qualified Control.Monad.Trans.State as S
import qualified Data.ByteString.Builder as BB
import Data.Constraint (Dict(Dict))
import qualified Data.List.NonEmpty as NEL
import qualified Data.Vector as V
import qualified Database.PostgreSQL.LibPQ as Pq
import Flay (Flayable1, Trivial, flay1, collect1)
import qualified Flay

import qualified Elefun.Col as C
import qualified Elefun.Expr as E
import qualified Elefun.Conn as Conn
import qualified Elefun.Parse as P
import qualified Elefun.Query as Q
import qualified Elefun.Sql as Sql
import Elefun.Util (I(I,unI), K, ffor)

--------------------------------------------------------------------------------

select
  :: ( MonadIO m
     , Flay.Record (a C.CParser)
     , Flay.Record (a C.CRaw)
     , Flay.Terminal (a (K ()))
     , Flayable1 Typeable a
     , Flayable1 Trivial a
     , Flayable1 C.MkCRaw a
     , Flayable1 C.CParserFromE a )
  => Conn.Conn d
  -> Q.Q d () (a C.CExpr)
  -> m (Either String (V.Vector (a C.COut)))  -- ^
select con q = do
  let (s,_) = Q.runCExprQ q (Q.emptyS, ())
      sql = BB.toLazyByteString (Sql.rSel (Q.sSel s))
  Conn.exec con sql >>= getRowsFromResult >>= \case
     Right va -> pure (traverse (parseRow defaultRowParser) va)
     Left e -> pure (Left ("Error running SELECT: " ++ e))

--------------------------------------------------------------------------------

insert
  :: ( MonadIO m
     , Flay.Record (b C.CParser)
     , Flay.Record (b C.CRaw)
     , Flayable1 C.RunCInExprToE a
     , Flayable1 Trivial a
     , Flayable1 C.MkColName a
     , Flayable1 C.MkCRaw b
     , Flayable1 Trivial b
     , Flayable1 Typeable b
     , Flayable1 C.CParserFromE b
     , Flay.Terminal (a (K ()))
     , Flay.Terminal (b (K ())) )
  => Conn.Conn d
  -> Q.SchemaName
  -> Q.TableName
  -> Either [a C.CIn] (Q.Q d () (a C.CExpr))
     -- ^ Either values or query results to insert.
  -> (a C.CExpr -> b C.CExpr)
     -- ^ @RETURNING@ expression. You can use @'const' 'Elefun.R0'@ if you are
     -- not interested in returning anything.
  -> m (Either String (V.Vector (b C.COut)))   -- ^
insert _ _ _ (Left []) _ = pure (Right V.empty)
insert con sn tn e0 fb = do
  let e1 = first NEL.fromList e0
  let sql = BB.toLazyByteString (Sql.rInsert (insertSql sn tn e1 fb))
  Conn.exec con sql >>= getRowsFromResult >>= \case
     Right vb -> pure (traverse (parseRow defaultRowParser) vb)
     Left e -> pure (Left ("Error running INSERT: " ++ e))

insertSql
  :: forall d a b
  .  ( Flayable1 C.RunCInExprToE a
     , Flayable1 C.MkColName a
     , Flayable1 Trivial a
     , Flayable1 Trivial b
     , Flay.Terminal (a (K ())) )
  => Q.SchemaName
  -> Q.TableName
  -> Either (NEL.NonEmpty (a C.CIn))
            (Q.Q d () (a C.CExpr))
  -> (a C.CExpr -> b C.CExpr)
  -> Sql.Insert   -- ^
insertSql sn tn e0 fb =
   Sql.Insert (Q.tableSql sn tn)
              (either insertInputValuesSql insertInputSelectSql e0)
              (returningSql fb (Q.terminalCName :: a C.CName))

returningSql
  :: forall a b
  .  ( Flayable1 Trivial a
     , Flayable1 Trivial b )
  => (a C.CExpr -> b C.CExpr)
  -> a C.CName
  -> Maybe Sql.Returning -- ^
returningSql f aCName = do
  let exprs = collect1 hExprs (f (Q.fromCNameToCExpr aCName))
  Sql.Returning <$> NEL.nonEmpty exprs
 where
  hExprs :: forall c. Dict (Trivial c) -> C.CExpr c -> [Sql.Expr]
  hExprs = \Dict (C.UnsafeCExpr x) -> [x]

insertInputValuesSql
  :: ( Flayable1 C.RunCInExprToE a
     , Flayable1 C.MkColName a)
  => NEL.NonEmpty (a C.CIn)
  -> Sql.InsertInput   -- ^
insertInputValuesSql aCIns =
  Sql.InsertInputValues
    (NEL.fromList (cSqlNames (NEL.head aCIns)))
    (fmap (NEL.fromList . collect1 hCInExpr) aCIns)
 where
  hCInExpr :: forall c. Dict (C.RunCInExprToE c) -> C.CIn c -> [Sql.Expr]
  hCInExpr = \Dict cin -> [C.runCInExprToE cin]

insertInputSelectSql
  :: ( Flayable1 C.MkColName a
     , Flayable1 Trivial a)
  => Q.Q d () (a C.CExpr)
  -> Sql.InsertInput   -- ^
insertInputSelectSql q =
  let (s, aCName) = Q.runCExprQ q (Q.emptyS, ())
  in Sql.InsertInputSelect
       (NEL.fromList (cSqlNames aCName))
       (Sql.runSel (Q.sSel s))

--------------------------------------------------------------------------------

delete
  :: ( MonadIO m
     , Flay.Record (b C.CParser)
     , Flay.Record (b C.CRaw)
     , Flayable1 Trivial a
     , Flayable1 C.MkColName a
     , Flayable1 C.MkCRaw b
     , Flayable1 Trivial b
     , Flayable1 Typeable b
     , Flayable1 C.CParserFromE b
     , Flay.Terminal (a (K ()))
     , Flay.Terminal (b (K ())) )
  => Conn.Conn d
  -> Q.SchemaName
  -> Q.TableName
  -> (a C.CExpr -> E.E E.PgBool)
     -- ^ @WHERE@ clause. Only rows returning 'E.true' here will be deleted.
  -> (a C.CExpr -> b C.CExpr)
     -- ^ @RETURNING@ expression. You can use @'const' 'Elefun.R0'@ if you are
     -- not interested in returning anything.
  -> m (Either String (V.Vector (b C.COut)))   -- ^
delete con sn tn fw fb = do
  let sql = BB.toLazyByteString (Sql.rDelete (deleteSql sn tn fw fb))
  Conn.exec con sql >>= getRowsFromResult >>= \case
     Right vb -> pure (traverse (parseRow defaultRowParser) vb)
     Left e -> pure (Left ("Error running DELETE: " ++ e))

deleteSql
  :: forall a b
  .  ( Flayable1 C.MkColName a
     , Flayable1 Trivial a
     , Flayable1 Trivial b
     , Flay.Terminal (a (K ())) )
  => Q.SchemaName
  -> Q.TableName
  -> (a C.CExpr -> E.E E.PgBool)
     -- ^ @WHERE@ clause. Only rows returning 'E.true' here will be deleted.
  -> (a C.CExpr -> b C.CExpr)
     -- ^ @RETURNING@ expression. You can use @'const' 'Elefun.R0'@ if you are
     -- not interested in returning anything.
  -> Sql.Delete  -- ^
deleteSql sn tn fw fb =
    let aCName = Q.terminalCName :: a C.CName
    in Sql.Delete (Q.tableSql sn tn)
                  (Just (E.unE (fw (Q.fromCNameToCExpr aCName))))
                  (returningSql fb aCName)

--------------------------------------------------------------------------------

update
  :: ( MonadIO m
     , Flayable1 C.MkCRaw b
     , Flayable1 C.MkColName a
     , Flayable1 Trivial a
     , Flayable1 Trivial b
     , Flayable1 Typeable b
     , Flayable1 C.CParserFromE b
     , Flay.Terminal (a (K ()))
     , Flay.Terminal (b (K ()))
     , Flay.Record (b C.CParser)
     , Flay.Record (b C.CRaw) )
  => Conn.Conn d
  -> Q.SchemaName
  -> Q.TableName
  -> (a C.CExpr -> Either (a C.CUp) (Q.Q d () (a C.CUp)))
  -> (a C.CExpr -> E.E E.PgBool)
     -- ^ @WHERE@ clause. Only rows returning 'E.true' here will be updated.
  -> (a C.CExpr -> b C.CExpr)
     -- ^ @RETURNING@ expression. You can use @'const' 'Elefun.R0'@ if you are
     -- not interested in returning anything.
  -> m (Either String (V.Vector (b C.COut)))   -- ^
update con sn tn feu fw fb = do
  let ysql = fmap (BB.toLazyByteString . Sql.rUpdate)
                  (updateSql sn tn feu fw fb)
  case ysql of
     Nothing -> pure (Right V.empty)
     Just sql -> Conn.exec con sql >>= getRowsFromResult >>= \case
        Right vb -> pure (traverse (parseRow defaultRowParser) vb)
        Left e -> pure (Left ("Error running UPDATE: " ++ e))

updateSql
  :: forall a b d
  .  ( Flayable1 C.MkColName a
     , Flayable1 Trivial a
     , Flayable1 Trivial b
     , Flay.Terminal (a (K ())) )
  => Q.SchemaName
  -> Q.TableName
  -> (a C.CExpr -> Either (a C.CUp) (Q.Q d () (a C.CUp)))
  -> (a C.CExpr -> E.E E.PgBool)
     -- ^ @WHERE@ clause. Only rows returning 'E.true' here will be updated.
  -> (a C.CExpr -> b C.CExpr)
     -- ^ @RETURNING@ expression. You can use @'const' 'Elefun.R0'@ if you are
     -- not interested in returning anything.
  -> Maybe Sql.Update  -- ^
updateSql sn tn feu fw fb = ffor yuiSql $ \uiSql ->
   Sql.Update
     (Q.tableSql sn tn) uiSql
     (Just (E.unE (fw (Q.fromCNameToCExpr aCName))))
     (returningSql fb aCName)
  where
    aCName :: a C.CName = Q.terminalCName
    aCExpr :: a C.CExpr = Q.fromCNameToCExpr aCName
    yuiSql :: Maybe Sql.UpdateInput = case feu aCExpr of
      Left cUp -> Sql.UpdateInputValues <$> NEL.nonEmpty (Q.runCUp cUp)
      Right q -> case Q.runCUpQ q (Q.emptyS, ()) of
        (s, ns) -> Sql.UpdateInputSelect <$> NEL.nonEmpty ns
                                         <*> pure (Sql.runSel (Q.sSel s))

--------------------------------------------------------------------------------

getRowsFromResult
  :: forall m a
  .  ( MonadIO m
     , Flayable1 C.MkCRaw a
     , Flayable1 Trivial a
     , Flay.Terminal (a (K ())) )
  => Pq.Result
  -> m (Either String (V.Vector (a C.CRaw))) -- ^
getRowsFromResult res = runExceptT $ do
  let aK = Flay.terminal :: a (K ())
  -- TODO: call Pq.resultStatus?
  let ncols = getSum (collect1 (\(Dict :: Dict (Trivial c)) _ -> Sum 1) aK)
  ncolsActual <- liftIO (Pq.nfields res)
  when (ncols /= ncolsActual) $ do
     throwE "Got a number of columns different from the one we were expecting"
  meta <- liftIO $ fmap V.fromList $ for [Pq.Col 0 .. pred ncols] $ \col -> do
     (,) <$> Pq.ftype res col <*> Pq.fformat res col
  nrows <- liftIO (Pq.ntuples res)
  fmap V.fromList $ for [Pq.Row 0 .. pred nrows] $ \row -> do
     flip S.evalStateT (Pq.Col 0) $ do
        (`flay1` aK) $ \(Dict :: Dict (C.MkCRaw x)) _ -> do
            col@(Pq.Col i) <- S.get
            S.put $! succ col
            ycr <- liftIO (Pq.getvalue' res row col) >>= \case
               Nothing -> pure (C.mkCRaw Nothing)
               Just bs -> do
                  (oid, fmt) <- V.unsafeIndexM meta (fromIntegral i)
                  pure (C.mkCRaw (Just (P.RawField oid fmt bs)))
            case ycr of
               Just cr -> pure cr
               Nothing -> lift $ throwE "Got NULL in a not-nullable column"

--------------------------------------------------------------------------------

defaultRowParser
  :: (Flayable1 C.CParserFromE a, Flay.Terminal (a (K ())))
  => a C.CParser
{-# INLINABLE defaultRowParser #-}
defaultRowParser = unI (flay1 h Flay.terminal)
 where
   h :: forall x. Dict (C.CParserFromE x) -> K () x -> I (C.CParser x)
   h Dict _ = I C.cParserFromE

parseRow
  :: ( Flay.Record (a C.CRaw)
     , Flay.Record (a C.CParser)
     , Flayable1 Typeable a )
  => a C.CParser
  -> a C.CRaw
  -> Either String (a C.COut)   -- ^
{-# INLINABLE parseRow #-}
parseRow aP aR = fromJust <$> Flay.zip1 h aP aR
   where
     h :: Dict (Typeable x) -> C.CParser x -> C.CRaw x
       -> Either String (C.COut x)
     h = \Dict cp cr -> C.runCParser cp cr

--------------------------------------------------------------------------------

cSqlNames :: Flayable1 C.MkColName a => a f -> [Sql.Name]
cSqlNames = collect1 $ \(Dict :: Dict (C.MkColName x)) (_ :: f c) ->
      [C.mkColSqlName (Proxy :: Proxy c)]

