{-# LANGUAGE Arrows #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses #-}
{-# OPTIONS_HADDOCK hide #-}

module Elefun.Query where

import BasePrelude hiding (union, (<>))
import Control.Arrow (Arrow(..))
import qualified Control.Monad.Trans.State as S
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy.Char8 as BL8
import qualified Data.ByteString.Builder as BB
import Data.Constraint (Dict(Dict))
import Data.Profunctor (Profunctor(..), Strong(..), Choice(..), Cochoice(..))
import Data.Semigroup (Semigroup((<>)))
import Data.Void (Void, absurd)
import Data.Word (Word32)
import Flay (Flayable1, Trivial, flay1, trivial1)
import qualified Flay
import Numeric.Natural (Natural)

import Elefun.Expr (E(UnsafeE), N)
import qualified Elefun.Expr as E
import qualified Elefun.Sql as Sql
import Elefun.Col
  (CUp(CUp), CExpr(UnsafeCExpr), CName(CName), MkColName(mkColName), mkColSqlName)
import Elefun.Util (I(I,unI), K(K), P2(P2), R0(R0))

--------------------------------------------------------------------------------

-- | The state carried by 'Q'
data S = S
  { sSel :: !Sql.Sel  -- ^ SQL query being built.
  , sNxt :: !Word32   -- ^ Used to generate unique names.
  } deriving (Eq, Show)

emptyS :: S
emptyS = S Sql.SelEmpty 0
{-# INLINE emptyS #-}

newAliasS :: S -> (S, Alias)
newAliasS = \s ->
  let bs = BL8.toStrict (BB.toLazyByteString ("z" <> BB.word32Dec (sNxt s)))
  in (s {sNxt = succ (sNxt s)}, UnsafeAlias bs)
{-# INLINE newAliasS #-}

mapSelS :: (Sql.Sel -> Sql.Sel) -> S -> S
mapSelS f = \s -> s{sSel = f (sSel s)}
{-# INLINE mapSelS #-}

--------------------------------------------------------------------------------

-- | An alias for a column, table, etc.
newtype Alias = UnsafeAlias { unAlias :: B.ByteString }
  deriving (Eq, Show)

aliasName :: Alias -> Sql.Name
aliasName = Sql.NameNoQuote . unAlias

--------------------------------------------------------------------------------

-- | Represents a @SELECT@ query.
--
-- The @d@ phantom type is used to constrain this 'Q' to a particular database.
newtype Q d a b = Q { unQ :: (S, a) -> (S, b) }

instance Category (Q d) where
  {-# INLINABLE id #-}
  id = Q id
  {-# INLINABLE (.) #-}
  qf . qg = Q (\x -> unQ qf (unQ qg x))

instance Functor (Q d a) where
  {-# INLINABLE fmap #-}
  fmap g = \qf -> Q (\(s, b) ->
    let (s', c) = unQ qf (s, b)
    in (s', g c))

instance Profunctor (Q d) where
  {-# INLINABLE dimap #-}
  dimap f g = \qh -> Q (\(s, a) ->
    let (s', c) = unQ qh (s, f a)
    in (s', g c))

instance Choice (Q d) where
  {-# INLINABLE left' #-}
  left' = \qb -> Q (\(s, eac) ->
    case eac of
       Left a -> case unQ qb (s, a) of (s', b) -> (s', Left b)
       Right c -> (s, Right c))

instance Cochoice (Q d) where
  {-# INLINABLE unleft #-}
  unleft = \qebd -> Q (\(s, a) ->
    let (s', ebd) = unQ qebd (s, Left a)
    in case ebd of
         Left b -> (s', b)
         Right _ -> error "Cochoice: Q d") -- some law preventing this?

instance Strong (Q d) where
  {-# INLINABLE first' #-}
  first' = \qf -> Q (\(s, (b, d)) ->
    let (s', c) = unQ qf (s, b)
    in (s', (c, d)))

instance Arrow (Q d) where
  {-# INLINABLE arr #-}
  arr = \f -> Q (\(s, b) -> (s, f b))
  {-# INLINE first #-}
  first = first'

instance ArrowChoice (Q d) where
  {-# INLINE left #-}
  left = left'
  {-# INLINABLE (+++) #-}
  qb +++ qb' = Q (\(s, eaa') ->
    case eaa' of
       Left a -> case unQ qb (s, a) of (s', b) -> (s', Left b)
       Right a' -> case unQ qb' (s, a') of (s', b') -> (s', Right b'))

instance Applicative (Q d a) where
  {-# INLINABLE pure #-}
  pure = \a -> Q (\(s, _) -> (s, a))
  {-# INLINABLE (<*>) #-}
  qff <*> qfx = Q (\(s, a) ->
    let (s',  f) = unQ qff (s,  a)
        (s'', x) = unQ qfx (s', a)
    in (s'', f x))

instance Monad (Q d a) where
  {-# INLINABLE (>>=) #-}
  qfb >>= k = Q (\(s, a) ->
    let (s', b) = unQ qfb (s, a)
    in unQ (k b) (s', a))

instance Semigroup b => Semigroup (Q d a b) where
  {-# INLINABLE (<>) #-}
  (<>) = liftA2 (<>)

instance Monoid b => Monoid (Q d a b) where
  {-# INLINABLE mempty #-}
  mempty = mempty <$ id
  {-# INLINABLE mappend #-}
  mappend = liftA2 mappend

instance Foldable (Q d ()) where
  {-# INLINABLE foldr #-}
  foldr = \f c q -> case unQ q (emptyS, ()) of (_, b) -> f b c

instance Traversable (Q d ()) where
  {-# INLINABLE traverse #-}
  traverse f = \q -> case unQ q (emptyS, ()) of (_, b) -> fmap pure (f b)

instance Flayable1 Trivial b => Show (Q d () (b CExpr)) where
  {-# INLINABLE show #-}
  show = \q -> case runCExprQ q (emptyS, ()) of
     (s,_) -> BL8.unpack (BB.toLazyByteString (Sql.rSel (sSel s)))

-- | Convert a 'Q' to its Kleisli arrow form, but without wrapping it up in
-- 'Kleisli'. In other words, allow the  provision of the @a@ input for 'Q'
-- using normal function application.
--
-- @'unkleisli' . 'kleisli'  == 'id'@
--
-- @'kleisli' . 'unkleisli'  == 'id'@
kleisli :: Q d a b -> (a -> Q d () b)
kleisli q a = lmap (\() -> a) q
{-# INLINABLE kleisli #-}

-- | Convert a 'Q' in its Kleisli arrow form to a 'Q' that takes its input in
-- the 'Q' category.
--
-- @'unkleisli' . 'kleisli'  == 'id'@
--
-- @'kleisli' . 'unkleisli'  == 'id'@
unkleisli :: (a -> Q d () b) -> Q d a b
unkleisli faq = Q (\(s,a) -> unQ (faq a) (s,()))
{-# INLINABLE unkleisli #-}

-- | Initial object in the 'Q' category.
--
-- This 'Q' can't ever be executed because it's not possible to provide an input
-- for it, since a 'Void' can't ever be provided.
initial :: Q d Void a
initial = Q (\(s, v) -> (s, absurd v))

-- | Terminal object in the 'Q' category.
--
-- You can use 'terminal' to generalize 'Q's that take @()@ as input to take
-- and ignore any input instead. For example:
--
-- @
-- __> :t q1__
-- q1 :: 'Q' d () 'Int'
-- __> :t q1 . 'terminal'__
-- q1 . 'terminal' :: 'Q' d a 'Int'
-- @
terminal :: Q d a ()
terminal = Q (\(s, _) -> (s, ()))
{-# INLINE terminal #-}

--------------------------------------------------------------------------------

withS :: (S -> (S, b)) -> Q d a b
withS f = Q (\(s, _) -> f s)
{-# INLINE withS #-}

newAlias :: Q d a Alias
newAlias = withS newAliasS
{-# INLINE newAlias #-}

mapS :: (S -> S) -> Q d a a
mapS f = Q (\(s, a) -> (f s, a))
{-# INLINE mapS #-}

mapSel :: (Sql.Sel -> Sql.Sel) -> Q d a a
mapSel f = mapS (mapSelS f)
{-# INLINE mapSel #-}

--------------------------------------------------------------------------------

-- | This is just @'Control.Category.id' :: 'Q' d a a@, but with a different
-- name that is perhaps more intuitive when using monadic notation.
--
-- @
-- -- | Whether the input is even.
-- isEven :: 'Q' d 'Int' 'Bool'
-- isEven = do
--     num :: 'Int' <- 'input'
--     'pure' ('even' num :: 'Bool')
-- @
input :: Q d a a
input = id
{-# INLINE input #-}

-- | @LIMIT@ the number of resulting rows.
--
-- Notice that the last 'limit' performed on a 'Q' is the one that will be
-- enforced. That is:
--
-- @'limit' x . 'limit' y  ==  'limit' x@
limit :: Natural -> Q d a a
limit n = mapSel $ Sql.SelLimit
  (Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInteger (fromIntegral n))))

-- | @OFFSET@ the number of resulting rows.
--
-- Notice that the last 'offset' performed on a 'Q' is the one that will be
-- enforced. That is:
--
-- @'offset' x . 'offset' y  ==  'offset' x@
offset :: Natural -> Q d a a
offset n = mapSel $ Sql.SelOffset
  (Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInteger (fromIntegral n))))

--------------------------------------------------------------------------------

terminalCName
  :: forall a. (Flay.Terminal (a (K ())), Flayable1 MkColName a) => a CName
terminalCName = unI (Flay.flay1 h (Flay.terminal :: a (K ())))
  where
    h :: forall c. Dict (MkColName c) -> K () c -> I (CName c)
    h = \Dict _ -> I mkColName

-- | Runs a query (i.e., @b 'CExpr'@) returning the names of the aliases that
-- point to the resulting expressions.
--
-- TODO Maybe return @('S', ['Alias'])@ instead? It's easier to use downstream?
runCExprQ :: Flayable1 Trivial b => Q d a (b CExpr) -> (S, a) -> (S, b CName)
runCExprQ q (s0, a) = case unQ q (s0, a) of
  (s, bF) -> swap (S.runState (trivial1 h bF) s)
 where
  h :: forall c. CExpr c -> S.State S (CName c)
  h = \(UnsafeCExpr x) -> do
    (s', al) <- S.gets newAliasS
    let fsl = Sql.SelList (Sql.SelectList (pure (x, Just (aliasName al))))
    S.put $! mapSelS fsl s'
    pure (CName (aliasName al))

fromCNameToCExpr :: Flayable1 Trivial a => a CName -> a CExpr
fromCNameToCExpr = unI . trivial1 (\(CName n) ->
  I (UnsafeCExpr (Sql.ExprColumn (Sql.Column Nothing n))))

--------------------------------------------------------------------------------

-- | Identity 'CUp'. That is, no update is performed on any column.
identityCUp :: (Flay.Terminal (a (K ())), Flayable1 Trivial a) => a CUp
{-# INLINABLE identityCUp #-}
identityCUp = unI (Flay.trivial1 (\(K ()) -> I (CUp Nothing)) Flay.terminal)

-- | Returns a list of the column names that shall be updated, together with
-- their new values.
--
-- Use 'runCUp' if the 'CUp' is inside a 'Q'.
runCUp :: Flayable1 MkColName a => a CUp -> [(Sql.Name, Sql.Expr)]
{-# INLINABLE runCUp #-}
runCUp = Flay.collect1 $ \(Dict :: Dict (MkColName c)) (CUp y :: CUp c) ->
  case y of Nothing -> []
            Just (UnsafeCExpr x) -> [(mkColSqlName (Proxy :: Proxy c), x)]

-- | This is like 'runCExprQ', except it doesn't return a @b 'CName'@ since
-- not all of the columns are meant to be updated by a 'CUp'. Instead, it
-- returns a list of 'Sql.Name's identifying the columns that shall be updated,
-- in the same order in which they will appear in the query select list.
runCUpQ :: Flayable1 MkColName b => Q d a (b CUp) -> (S, a) -> (S, [Sql.Name])
{-# INLINABLE runCUpQ #-}
runCUpQ q (s0, a) = case unQ q (s0, a) of
  (s1, bF) -> case S.execState (flay1 h bF) (s1, []) of
     (s2, ns) -> (s2, reverse ns)
 where
  h :: forall c. Dict (MkColName c) -> CUp c -> S.State (S, [Sql.Name]) (K () c)
  h = \Dict (CUp y) -> case y of
    Nothing -> pure (K ())
    Just (UnsafeCExpr x) -> do
       (s, ns) <- S.get
       let (s', al) = newAliasS s
           fsl = Sql.SelList (Sql.SelectList (pure (x, Just (aliasName al))))
           !s'' = mapSelS fsl s'
           !ns' = mkColSqlName (Proxy :: Proxy c) : ns
       S.put $! (s'', ns')
       pure (K ())

--------------------------------------------------------------------------------

-- | Case-sensitive schema name, without surrounding quotes.
newtype SchemaName = SchemaName B.ByteString
  deriving (Eq, Show)
instance IsString SchemaName where
  fromString = SchemaName . B8.pack

-- | Case-sensitive table name, without surrounding quotes.
newtype TableName = TableName B.ByteString
  deriving (Eq, Show)
instance IsString TableName where
  fromString = TableName . B8.pack

tableSql :: SchemaName -> TableName -> Sql.Table
tableSql (SchemaName sn) (TableName tn) =
  Sql.Table (Sql.Identifier (Just (Sql.Schema (Sql.NameQuote sn)))
                            (Sql.NameQuote tn))

-- | Select from a table.
--
-- For example, provided the following datatype @Foo@ and related 'Flayable1'
-- instance:
--
-- @
-- data Foo f = Foo
--   { _fooX :: f (''Elefun.Cn' 'Elefun.E' 'Elefun.PgInt2' 'Int16' "x")
--   , _fooY :: f (''Elefun.Cn' 'Elefun.N' 'Elefun.PgInt4' 'Int32' "y")
--   } deriving ('Generic')
--
-- instance
--   ( c (''Elefun.Cn' 'Elefun.E' 'Elefun.PgInt2' 'Int16' "x")
--   , c (''Elefun.Cn' 'Elefun.N' 'Elefun.PgInt4' 'Int32' "y")
--   ) => 'Flayable1' c Foo
-- @
--
-- … then the following Haskell expression:
--
-- @'fromTable' \"public\" \"foo\" :: 'Q' d a (Foo 'CExpr')@
--
-- … would generate the following SQL:
--
-- @SELECT "x" z0, "y" z1 FROM "public"."foo"@
--
-- Keep in mind that the aliases @z0@ and @z1@ that show up in the SQL query are
-- generated internally and might change when the resulting 'Q' is further
-- composed. So, don't rely on these aliases being predictable.
--
-- Notice that 'fromTable' will require that the column medatada be of types
-- either ''Elefun.Cn' or ''Elefun.Ci', since only those mention the column name.
fromTable
  :: forall d a b
  .  (Flayable1 MkColName b, Flay.Terminal (b (K ())))
  => SchemaName -> TableName -> Q d a (b CExpr) -- ^
{-# INLINABLE fromTable #-}
fromTable sn tn = do
    _ <- mapSel (Sql.SelFrom (Sql.FromTable (tableSql sn tn) Nothing Nothing))
    Flay.flay1 h (Flay.terminal :: b (K ())) :: Q d a (b CExpr)
  where
    h :: forall c. Dict (MkColName c) -> K () c -> Q d a (CExpr c)
    h = \Dict _ -> do
       let x = mkColSqlName (Proxy :: Proxy c)
       pure (UnsafeCExpr (Sql.ExprColumn (Sql.Column Nothing x)))

--------------------------------------------------------------------------------

boolF
  :: (Flay.Record (a CExpr), Flayable1 Typeable a)
  => a CExpr       -- ^ False
  -> a CExpr       -- ^ True
  -> E.E E.PgBool  -- ^ Predicate
  -> a CExpr
{-# INLINE boolF #-}
boolF faF taF (UnsafeE b) = fromJust (unI (Flay.zip1 h faF taF))
  where
    h :: Dict (Typeable x) -> CExpr x -> CExpr x -> I (CExpr x)
    {-# INLINE h #-}
    h = \Dict (UnsafeCExpr f) (UnsafeCExpr t) ->
            -- Like 'E.bool', but without type information
            I (UnsafeCExpr (Sql.ExprCase (Sql.Case (pure (b, t)) f)))

--------------------------------------------------------------------------------

-- | Perform an SQL @LEFT JOIN@.
--
-- Hint: If your join condition is an equality between two columns, and at least
-- one of those two columns is 'Nullable', then prefer to use 'E.eqN' rather
-- than 'E.eq'.
leftJoin
  :: ( Flayable1 Trivial l
     , Flayable1 Trivial r
     , Flayable1 Typeable z
     , Flay.Record (z CExpr) )
  => Q d () (l CExpr)                    -- ^ Left
  -> Q d () (r CExpr)                    -- ^ Right
  -> (l CExpr -> r CExpr -> N E.PgBool)  -- ^ Condition
  -> (l CExpr -> z CExpr)                -- ^ Map left
  -> (l CExpr -> r CExpr -> z CExpr)     -- ^ Map left and right
  -> Q d () (z CExpr)
leftJoin lq rq wh gl glr = do
   almarkr <- newAlias
   P2 lF (P2 R0 rF) <- unsafeJoin Sql.LeftOuter lq
     (rq >>> mark almarkr &&& id >>^ uncurry P2)
     (\lF (P2 R0 rF) -> wh lF rF)
   let eb = Sql.ExprRaw (Sql.Raw (BL8.fromStrict (unAlias almarkr)))
   pure (boolF (gl lF) (glr lF rF) (E.UnsafeE eb))
 where
   mark :: Alias -> Q d a (R0 CExpr)
   mark al = withS $ \s ->
      let lit = Sql.ExprLiteral (Sql.LiteralBool (Sql.BoolLit True))
          sl = Sql.SelectList (pure (lit, Just (aliasName al)))
      in (mapSelS (Sql.SelList sl) s, R0)

-- | Perform an SQL @FULL JOIN@.
--
-- Hint: If your join condition is an equality between two columns, and at least
-- one of those two columns is 'Nullable', then use 'E.eqN' rather than
-- @'E.liftN2' 'E.eq'@, because 'fullJoin' supports only merge-joinable or
-- hash-joinable join conditions.
fullJoin
  :: ( Flayable1 Trivial l
     , Flayable1 Trivial r
     , Flayable1 Typeable z
     , Flay.Record (z CExpr) )
  => Q d () (l CExpr)                        -- ^ Left
  -> Q d () (r CExpr)                        -- ^ Right
  -> (l CExpr -> r CExpr -> N E.PgBool)    -- ^ Condition
  -> (l CExpr -> z CExpr)                  -- ^ Map left
  -> (r CExpr -> z CExpr)                  -- ^ Map right
  -> (l CExpr -> r CExpr -> z CExpr)     -- ^ Map left and right
  -> Q d () (z CExpr)
fullJoin lq rq wh gl gr glr = do
   almarkl <- newAlias
   almarkr <- newAlias
   P2 (P2 R0 lF) (P2 R0 rF) <- unsafeJoin Sql.FullOuter
     (lq >>> mark almarkl &&& id >>^ uncurry P2)
     (rq >>> mark almarkr &&& id >>^ uncurry P2)
     (\(P2 R0 lF) (P2 R0 rF) -> wh lF rF)
   let ebl = Sql.ExprRaw (Sql.Raw (BL8.fromStrict (unAlias almarkl)))
       ebr = Sql.ExprRaw (Sql.Raw (BL8.fromStrict (unAlias almarkr)))
   pure (boolF (gl lF)
               (boolF (gr rF) (glr lF rF) (E.UnsafeE ebl))
               (E.UnsafeE ebr))
 where
   mark :: Alias -> Q d a (R0 CExpr)
   mark al = withS $ \s ->
      let lit = Sql.ExprLiteral (Sql.LiteralBool (Sql.BoolLit True))
          sl = Sql.SelectList (pure (lit, Just (aliasName al)))
      in (mapSelS (Sql.SelList sl) s, R0)

--------------------------------------------------------------------------------

-- | Perform an SQL @CROSS JOIN@.
--
-- Hint: If your join condition is an equality between two columns, and at least
-- one of those two columns is 'Nullable', then prefer to use 'E.eqN' rather
-- than 'E.eq'.

-- Note: Due to some clever optimization, this will render as @CROSS JOIN@, not
-- as @INNER JOIN@.
crossJoin
  :: ( Flayable1 Trivial l, Flayable1 Trivial r)
  => Q d () (l CExpr)                 -- ^ Left
  -> Q d () (r CExpr)                 -- ^ Right
  -> Q d () (P2 l r CExpr)
crossJoin lq rq = innerJoin lq rq (\_ _ -> E.en E.true)

-- | Perform an SQL @INNER JOIN@.
--
-- Hint: If your join condition is an equality between two columns, and at least
-- one of those two columns is 'Nullable', then prefer to use 'E.eqN' rather
-- than 'E.eq'.
innerJoin
  :: (Flayable1 Trivial l, Flayable1 Trivial r)
  => Q d () (l CExpr)                 -- ^ Left
  -> Q d () (r CExpr)                 -- ^ Right
  -> (l CExpr -> r CExpr -> N E.PgBool) -- ^ Condition
  -> Q d () (P2 l r CExpr)
innerJoin lq rq wh = unsafeJoin Sql.Inner lq rq wh

unsafeJoin
  :: forall d l r
  .  (Flayable1 Trivial l, Flayable1 Trivial r)
  => Sql.JoinRel                  -- ^ Join type
  -> Q d () (l CExpr)             -- ^ Left)
  -> Q d () (r CExpr)             -- ^ Right
  -> (l CExpr -> r CExpr -> N E.PgBool) -- ^ Condition
  -> Q d () (P2 l r CExpr)
  -- ^ Any of @l@ or @r@, as returned by this query, could be @NULL@ columns.
unsafeJoin rel lq rq wh = Q $ \(lrsi, _) ->
  let -- left side stuff
      lsi :: S = emptyS { sNxt = sNxt lrsi }
      (lso :: S, lcn :: l CName) = runCExprQ lq (lsi, ())
      lselect :: Sql.Select = Sql.runSel (sSel lso)
      (lso' :: S, lal :: Alias) = newAliasS lso
      lfrom = Sql.FromSelect lselect Nothing (Just (aliasName lal, []))
      lF :: l CExpr = unI $ trivial1 (\(CName n) ->
         let i = Sql.Identifier Nothing (aliasName lal)
             c = Sql.Column (Just (Sql.Table i)) n
         in I (UnsafeCExpr (Sql.ExprColumn c))) lcn
      -- same as above, but for the right side
      rsi :: S = emptyS { sNxt = sNxt lso' }
      (rso :: S, rcn :: r CName) = runCExprQ rq (rsi, ())
      rselect :: Sql.Select = Sql.runSel (sSel rso)
      (rso' :: S, ral :: Alias) = newAliasS rso
      rfrom = Sql.FromSelect rselect Nothing (Just (aliasName ral, []))
      rF :: r CExpr = unI $ trivial1 (\(CName n) ->
         let i = Sql.Identifier Nothing (aliasName ral)
             c = Sql.Column (Just (Sql.Table i)) n
         in I (UnsafeCExpr (Sql.ExprColumn c))) rcn
      -- joins
      lrfrom = Sql.FromJoin lfrom (Sql.Join rel rfrom (E.unN (wh lF rF)))
      lrso = lrsi { sNxt = sNxt rso', sSel = Sql.SelFrom lrfrom (sSel lrsi) }
  in (lrso, P2 lF rF)

restrict :: E Bool -> Q d a a
restrict (UnsafeE x) = mapSel (Sql.SelWhere x)

uie :: Sql.UieRel
    -> Maybe Sql.Distinct
    -> Q d () (a CExpr)
    -> Q d () (a CExpr)
    -> Q d () (a CExpr)    -- ^
uie r yd = \qa qb -> Q $ \(S s0 t0, ()) ->
  let (S sa ta, _)  = unQ qa (S s0 t0, ())
      (S sb tb, cb) = unQ qb (S s0 ta, ())
  in case yd of
      Nothing -> (S (Sql.SelUieAll r sa sb) tb, cb)
      Just Sql.Distinct -> (S (Sql.SelUie r sa sb) tb, cb)

unionAll :: Q d () (a CExpr) -> Q d () (a CExpr) -> Q d () (a CExpr) -- ^
unionAll = uie Sql.Union Nothing

union :: Q d () (a CExpr) -> Q d () (a CExpr) -> Q d () (a CExpr) -- ^
union = uie Sql.Union (Just Sql.Distinct)

intersectAll :: Q d () (a CExpr) -> Q d () (a CExpr) -> Q d () (a CExpr) -- ^
intersectAll = uie Sql.Intersect Nothing

intersect :: Q d () (a CExpr) -> Q d () (a CExpr) -> Q d () (a CExpr) -- ^
intersect = uie Sql.Intersect (Just Sql.Distinct)

exceptAll :: Q d () (a CExpr) -> Q d () (a CExpr) -> Q d () (a CExpr) -- ^
exceptAll = uie Sql.Except Nothing

except :: Q d () (a CExpr) -> Q d () (a CExpr) -> Q d () (a CExpr) -- ^
except = uie Sql.Except (Just Sql.Distinct)

