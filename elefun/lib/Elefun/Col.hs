{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses #-}
{-# OPTIONS_HADDOCK hide #-}

module Elefun.Col
 ( C
 , type CEp
 , type CEh
 , type CEn
 , type CEd
 , type CNp
 , type CNh
 , type CNn
 , type CNd
 , C_en
 , C_p
 , C_h
 , C_n
 , C_i

 , Default(Default, Explicit)

 , CExpr(UnsafeCExpr)
 , ce
 , cn
 , ec
 , ecp
 , ech
 , ecn
 , ecd
 , nc
 , ncp
 , nch
 , ncn
 , ncd

 , CRaw(CRawE, CRawN)
 , MkCRaw(mkCRaw)

 , COut(COutE, COutN)

 , CParser(CParser)
 , runCParser
 , CParserFromE(cParserFromE)

 , CIn(CIn)
 , RunCInExpr(runCInExpr)
 , RunCInExprToE(runCInExprToE)

 , CUp(CUp)

 , CName(CName)
 , MkColName(mkColName)
 , mkColSqlName
 ) where

import BasePrelude hiding (null)
import Data.Kind (Type)
import GHC.TypeLits (Symbol, KnownSymbol, symbolVal)

import Elefun.Expr (E(UnsafeE, unE), N(UnsafeN), ToE(e))
import qualified Elefun.Sql as Sql
import Elefun.Parse (Parser, runParser, RawField, FromE(fromE))

--------------------------------------------------------------------------------

-- | Column metadata.
--
-- Note that 'C' is only ever used as a kind, and its constructors are only ever
-- used as types through the type-synonyms 'CEp', 'CEh', 'CEn', 'CEd', 'CNp',
-- 'CNh', 'CNn' and 'CNd'.
--
-- Mnemonic: The “C” stands for “column”.
data C where
  CEp :: p -> C
  CEh :: p -> f -> C
  CEn :: p -> f -> n -> C
  CEd :: p -> f -> n -> C
  CNp :: p -> C
  CNh :: p -> f -> C
  CNn :: p -> f -> n -> C
  CNd :: p -> f -> n -> C

-- | Column metadata type conveying the idea that the column contains a
-- not-nullable expression and can, at most, be used as part of PostgreSQL queries.
--
-- Mnemonic: In “CEp”, the “C” stands for “column”, the “E” stands for the
-- not-nullable 'E' type, and the “p” stands for PostgreSQL.
--
-- [@p@ :: any 'Type' with a 'Elefun.PgTyped' instance] The PostgreSQL type that
-- this column will take (e.g., 'Elefun.PgInt2', 'Elefun.PgText', etc.).
type CEp (p :: Type) = 'CEp p

-- | Like 'CEp', but the expression may be @NULL@.
--
-- Mnemonic: In “CNp”, the “C” stands for “column”, the “N” stands for the
-- nullable 'N' type, and the “p” stands for PostgreSQL.
type CNp (p :: Type) = 'CNp p

-- | Column metadata type conveying the idea that the column can not only be
-- used as part of SQL queries as in 'CEp', but also that the expresions in this
-- column can be fetched from PostgreSQL into Haskell.
--
-- Mnemonic: In “CEh”, the “C” stands for “column”, the “E” stands for the
-- not-nullable 'E' type, and the “h” stands for “Haskell”..
--
-- [@p@ :: any 'Type' with a 'Elefun.PgTyped' instance] Just like @p@ in 'CEp'.
--
-- [@h@ :: any 'Type'] The Haskell type that expressions in this column will
-- take when fetched from PostgreSQL into Haskell. In order to achieve the
-- conversion of @p@ into @h@, a @'Elefun.FromE' p h@ will eventually be
-- required.
type CEh (p :: Type) (h :: Type) = 'CEh p h

-- | Like 'CEh', but the expression may be @NULL@.
--
-- Mnemonic: In “CNh”, the “C” stands for “column”, the “N” stands for the
-- nullable 'N' type, and the “h” stands for “Haskell”.
type CNh (p :: Type) (h :: Type) = 'CNh p h

-- | Column metadata type conveying the idea that the column can not only be
-- used as part of SQL queries as in 'CEp', and that the expresions in this
-- column can be fetched from PostgreSQL into Haskell as in 'CEh', but also that
-- the column where this expression belongs has a name in a PostgreSQL table,
-- thus it can be looked up or inserted there.
--
-- Mnemonic: In “CEn”, the “C” stands for “column”, the “E” stands for the
-- not-nullable type 'E', and the “n” stands for “name of the column”.
--
-- [@p@ :: any 'Type' with a 'Elefun.PgTyped' instance] Just like @p@ in 'CEp'.
--
-- [@h@ :: any 'Type'] Like @h@ in 'CEh', except this time @h@ could also be
-- used as the source Haskell type that will be converted to some 'PgType' when
-- inserting this column into a PostgreSQL table.
--
-- [@n@ :: any 'Symbol'] is the case-sensitive name of the column.
type CEn (p :: Type) (h :: Type) (n :: Symbol) = 'CEn p h n

-- | Like 'CEn', but the expression may be @NULL@.
--
-- Mnemonic: In “CNn”, the “C” stands for “column”, the “N” stands for the
-- nullable type 'N', and the “n” stands for “name of the column”.
type CNn (p :: Type) (h :: Type) (n :: Symbol) = 'CNn p h n

-- | Column metadata type conveying the idea that the column can not only be
-- used as part of SQL queries as in 'CEp', and that the expresions in this
-- column can be fetched from PostgreSQL into Haskell as in 'CEh', and that the
-- expressions in this column can be obtained from or inserted into a named
-- column in some PostgreSQL table like in 'CEn', but also that the expressions
-- in this column can take a @DEFAULT@ value when being inserted.
--
-- Mnemonic: In “CEn”, the “C” stands for “column”, the “E” stands for the
-- not-nullable type 'E', and the “d” stands for the @DEFAULT@ SQL expression.
--
-- [@p@ :: any 'Type' with a 'Elefun.PgTyped' instance] Just like @p@ in 'CEp'.
--
-- [@h@ :: any 'Type'] Just like @h@ in 'CEh'.
--
-- [@n@ :: any 'Symbol'] Just like @n@ in 'CEn'.
type CEd (p :: Type) (h :: Type) (n :: Symbol) = 'CEd p h n

-- | Like 'CEd', but the expression may be @NULL@.
--
-- Mnemonic: In “CNd”, the “C” stands for “column”, the “N” stands for the
-- nullable type 'N', and the “d” stands for the @DEFAULT@ SQL expression.
type CNd (p :: Type) (h :: Type) (n :: Symbol) = 'CNd p h n

--------------------------------------------------------------------------------

data Default (a :: Type) = Default | Explicit !a
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

--------------------------------------------------------------------------------

-- | Either 'E' or 'N', as it appears in the @en@ argument to 'Cp', 'Ch', 'Cn'
-- and 'Cd'.
--
-- Note: The following simingly redundant matches against E and N, which could
-- be replaced by a wildcard, are there so that we can further enforce E and N
-- in places where this type-family are used.
type family C_en (c :: C) :: Type -> Type where
  C_en ('CEp _) = E
  C_en ('CNp _) = N
  C_en ('CEh _ _) = E
  C_en ('CNh _ _) = N
  C_en ('CEn _ _ _) = E
  C_en ('CNn _ _ _) = N
  C_en ('CEd _ _ _) = E
  C_en ('CNd _ _ _) = N

-- | The type-index to 'E' or 'N', as it appears in the @p@ argument to 'Cp',
-- 'Ch', 'Cn' and 'Cd'.
--
-- Note: The following simingly redundant matches against E and N, which could
-- be replaced by a wildcard, are there so that we can further enforce E and N
-- in places where this type-family are used.
type family C_p (c :: C) :: Type where
  C_p ('CEp p) = p
  C_p ('CNp p) = p
  C_p ('CEh p _) = p
  C_p ('CNh p _) = p
  C_p ('CEn p _ _) = p
  C_p ('CNn p _ _) = p
  C_p ('CEd p _ _) = p
  C_p ('CNd p _ _) = p

-- | The Haskell type, as it appears in the @h@ argument to 'Ch', 'Cn' or 'Cd'.
--
-- Note: The following simingly redundant matches against E and N, which could
-- be replaced by a wildcard, are there so that we can further enforce E and N
-- in places where this type-family are used.
type family C_h (c :: C) :: Type where
  C_h ('CEh _ h) = h
  C_h ('CNh _ h) = h
  C_h ('CEn _ h _) = h
  C_h ('CNn _ h _) = h
  C_h ('CEd _ h _) = h
  C_h ('CNd _ h _) = h

-- | The column name, as it appears in the @n@ argument to 'Cn' or 'Cd'.
--
-- Note: The following simingly redundant matches against E and N, which could
-- be replaced by a wildcard, are there so that we can further enforce E and N
-- in places where this type-family are used.
type family C_n (c :: C) :: Symbol where
  C_n ('CEn _ _ n) = n
  C_n ('CNn _ _ n) = n
  C_n ('CEd _ _ n) = n
  C_n ('CNd _ _ n) = n

-- | The “insert” type, which is the @h@ argument to 'Cn' or 'Cd', with a
-- 'Maybe' wrapper in case the column is nullable, and a further 'Default'
-- wrapper if the column can take a @DEFAULT@ value.
--
-- Note: The following simingly redundant matches against E and N, which could
-- be replaced by a wildcard, are there so that we can further enforce E and N
-- in places where this type-family are used.
type family C_i (c :: C) :: Type where
  C_i ('CEn p h n) = C_h ('CEn p h n)
  C_i ('CNn p h n) = Maybe (C_h ('CNn p h n))
  C_i ('CEd p h n) = Default (C_h ('CEd p h n))
  C_i ('CNd p h n) = Default (Maybe (C_h ('CNd p h n)))

--------------------------------------------------------------------------------

data CExpr (c :: C) where
  UnsafeCExpr :: !Sql.Expr -> CExpr col

-- | Mnemonic: From 'CExpr' to 'E'.
--
-- @
-- 'ce' :: 'CExpr' ('CEp' p) -> 'E' p
-- 'ce' :: 'CExpr' ('CEh' p h) -> 'E' p
-- 'ce' :: 'CExpr' ('CEn' p h n) -> 'E' p
-- 'ce' :: 'CExpr' ('CEd' p h n) -> 'E' p
-- @
ce :: C_en c ~ E => CExpr c -> E (C_p c)
ce (UnsafeCExpr x) = UnsafeE x
{-# INLINE ce #-}

-- | Mnemonic: From 'CExpr' to 'N'.
--
-- @
-- 'cn' :: 'CExpr' ('CNp' p) -> 'N' p
-- 'cn' :: 'CExpr' ('CNh' p h) -> 'N' p
-- 'cn' :: 'CExpr' ('CNn' p h n) -> 'N' p
-- 'cn' :: 'CExpr' ('CNd' p h n) -> 'N' p
-- @
cn :: C_en c ~ N => CExpr c -> N (C_p c)
cn (UnsafeCExpr x) = UnsafeN x
{-# INLINE cn #-}

-- | Convert from 'E' to 'CExpr'.
--
-- Notice that the type-inference for the return type of this function is bad.
-- Consider using one of 'ecp', 'ech', 'ecn' or 'ecd' instead.
--
-- Mnemonic: from 'E' to a Column.
--
-- @
-- 'ec' :: 'E' p -> 'CExpr' ('CEp' p)
-- 'ec' :: 'E' p -> 'CExpr' ('CEh' p h)
-- 'ec' :: 'E' p -> 'CExpr' ('CEn' p h n)
-- 'ec' :: 'E' p -> 'CExpr' ('CEd' p h n)
-- @
ec :: C_en c ~ E => E (C_p c) -> CExpr c
ec = \(UnsafeE x) -> UnsafeCExpr x
{-# INLINE ec #-}

-- | Mnemonic: from 'E' to 'Cp'.
ecp :: E p -> CExpr ('CEp p)
ecp = ec
{-# INLINE ecp #-}

-- | Mnemonic: from 'E' to 'Ch'.
ech :: E p -> CExpr ('CEh p h)
ech = ec
{-# INLINE ech #-}

-- | Mnemonic: from 'E' to 'Cn'.
ecn :: E p -> CExpr ('CEn p h n)
ecn = ec
{-# INLINE ecn #-}

-- | Mnemonic: from 'E' to 'Cd'.
ecd :: E p -> CExpr ('CEd p h n)
ecd = ec
{-# INLINE ecd #-}

-- | Convert from 'N' to 'CExpr'.
--
-- Notice that the type-inference for the return type of this function is bad.
-- Consider using one of 'ncp', 'nch', 'ncn' or 'ncd' instead.
--
-- Mnemonic: from 'N' to a Column.
--
-- @
-- 'nc' :: 'PgTyped' p => 'N' p -> 'CExpr' ('CNp' p)
-- 'nc' :: 'PgTyped' p => 'N' p -> 'CExpr' ('CNh' p h)
-- 'nc' :: 'PgTyped' p => 'N' p -> 'CExpr' ('CNn' p h n)
-- 'nc' :: 'PgTyped' p => 'N' p -> 'CExpr' ('CNd' p h n)
-- @
nc :: (C_en c ~ N) => N (C_p c) -> CExpr c
nc = \(UnsafeN x) -> UnsafeCExpr x
{-# INLINE nc #-}

-- | Mnemonic: from 'N' to 'Cp'.
ncp :: N p -> CExpr ('CNp p)
ncp = nc
{-# INLINE ncp #-}

-- | Mnemonic: from 'N' to 'Ch'.
nch :: N p -> CExpr ('CNh p h)
nch = nc
{-# INLINE nch #-}

-- | Mnemonic: from 'N' to 'Cn'.
ncn :: N p -> CExpr ('CNn p h n)
ncn = nc
{-# INLINE ncn #-}

-- | Mnemonic: from 'N' to 'Cd'.
ncd :: N p -> CExpr ('CNd p h n)
ncd = nc
{-# INLINE ncd #-}

--------------------------------------------------------------------------------

-- | INTERNAL. Container for the raw data for the column. This is essentially
-- the input to 'CParser', which when parsed correctly ends up in 'COut'.
data CRaw (c :: C) where
  CRawE :: C_en c ~ E => !(RawField (C_p c)) -> CRaw c
  CRawN :: C_en c ~ N => !(Maybe (RawField (C_p c))) -> CRaw c

-- | INTERNAL. Helper class to construct 'CRaw' values from potentially NULL
-- inputs.
class MkCRaw (c :: C) where
  -- | INTERNAL. If the column supports nullable values, then providing this
  -- function with either 'Just' or 'Nothing' will result in 'Just'. Otherwise,
  -- if the column doesn't support nullable values, then only 'Just' will result
  -- in 'Just'.
  mkCRaw :: Maybe (RawField (C_p c)) -> Maybe (CRaw c)
instance MkCRaw ('CEp p) where
  mkCRaw = fmap CRawE
  {-# INLINABLE mkCRaw #-}
instance MkCRaw ('CEh p h) where
  mkCRaw = fmap CRawE
  {-# INLINABLE mkCRaw #-}
instance MkCRaw ('CEn p h n) where
  mkCRaw = fmap CRawE
  {-# INLINABLE mkCRaw #-}
instance MkCRaw ('CEd p h n) where
  mkCRaw = fmap CRawE
  {-# INLINABLE mkCRaw #-}
instance MkCRaw ('CNp p) where
  mkCRaw = Just . CRawN
  {-# INLINABLE mkCRaw #-}
instance MkCRaw ('CNh p h) where
  mkCRaw = Just . CRawN
  {-# INLINABLE mkCRaw #-}
instance MkCRaw ('CNn p h n) where
  mkCRaw = Just . CRawN
  {-# INLINABLE mkCRaw #-}
instance MkCRaw ('CNd p h n) where
  mkCRaw = Just . CRawN
  {-# INLINABLE mkCRaw #-}

--------------------------------------------------------------------------------

-- | Columns of type 'Ch', 'Cn' and 'Cd' can be fetched to Haskell. This
-- datatype holds the result of that fetching after it's been parsed.
data COut (c :: C) where
  COutE :: C_en c ~ E => !(C_h c) -> COut c
  COutN :: C_en c ~ N => !(Maybe (C_h c)) -> COut c

deriving instance Eq (C_h c) => Eq (COut c)
deriving instance Show (C_h c) => Show (COut c)

--------------------------------------------------------------------------------

-- | Container for a 'Parser' for a particular column.
--
-- Usually, these are constructed automatically behind the scenes when using a
-- functions like 'Elefun.select', which relies on 'FromE' instances being defined
-- for the 'C_p' and 'C_h' types in question.
data CParser (c :: C) where
  CParser :: (Typeable (C_p c), Typeable (C_h c))
          => !(Parser (C_p c) (C_h c)) -> CParser c

-- | Run a 'CParser' with its input coming from a 'CRaw'.
runCParser :: CParser c -> CRaw c -> Either String (COut c)
runCParser (CParser p) (CRawE  f) = COutE <$> runParser p f
runCParser (CParser p) (CRawN yf) = COutN <$> traverse (runParser p) yf
{-# INLINABLE runCParser #-}

class
  ( FromE (C_p c) (C_h c), Typeable (C_p c), Typeable (C_h c)
  ) => CParserFromE (c :: C) where
  cParserFromE :: CParser c
instance
  ( FromE (C_p c) (C_h c), Typeable (C_p c), Typeable (C_h c)
  ) => CParserFromE c where
  cParserFromE = CParser fromE
  {-# INLINE cParserFromE #-}

--------------------------------------------------------------------------------

-- | Container for the Haskell value to be converted to a PostgreSQL value when
-- inserting this column to a table.
data CIn (c :: C) where
  CIn :: !(C_i c) -> CIn c

deriving instance Eq (C_i c) => Eq (CIn c)
deriving instance Show (C_i c) => Show (CIn c)


class RunCInExpr (c :: C) where
  runCInExpr :: CIn c -> (C_h c -> E (C_p c)) -> Sql.Expr
instance RunCInExpr ('CEn p h n) where
  runCInExpr (CIn i) f = unE (f i)
  {-# INLINABLE runCInExpr #-}
instance RunCInExpr ('CNn p h n) where
  runCInExpr (CIn Nothing) _ = Sql.ExprNull
  runCInExpr (CIn (Just i)) f = unE (f i)
  {-# INLINABLE runCInExpr #-}
instance RunCInExpr ('CEd p h n) where
  runCInExpr (CIn Default) _ = Sql.ExprDefault
  runCInExpr (CIn (Explicit i)) f = unE (f i)
  {-# INLINABLE runCInExpr #-}
instance RunCInExpr ('CNd p h n) where
  runCInExpr (CIn Default) _ = Sql.ExprDefault
  runCInExpr (CIn (Explicit Nothing)) _ = Sql.ExprNull
  runCInExpr (CIn (Explicit (Just i))) f = unE (f i)
  {-# INLINABLE runCInExpr #-}

class (MkColName c, ToE (C_h c) (C_p c), RunCInExpr c) => RunCInExprToE (c :: C) where
  runCInExprToE :: CIn c -> Sql.Expr
instance forall c. (MkColName c, ToE (C_h c) (C_p c), RunCInExpr c) => RunCInExprToE c where
  runCInExprToE cin = runCInExpr cin (e :: C_h c -> E (C_p c))
  {-# INLINABLE runCInExprToE #-}

--------------------------------------------------------------------------------

-- | Container for a term-level representation of the column name.
data CName (c :: C) where
  CName :: !Sql.Name -> CName c

-- | INTERNAL. Helper class to build a 'CName'.
class KnownSymbol (C_n c) => MkColName (c :: C) where
  mkColName :: CName c
instance forall c. KnownSymbol (C_n c) => MkColName c where
  mkColName = CName (fromString (symbolVal (Proxy :: Proxy (C_n c))))
  {-# INLINABLE mkColName #-}

mkColSqlName :: forall c. MkColName c => Proxy c -> Sql.Name
mkColSqlName _ = case mkColName :: CName c of CName x -> x

--------------------------------------------------------------------------------

-- | Container for the new SQL value to use when updating a column in a table.
data CUp (c :: C) where
  -- | If 'Nothing', then don't update.
  CUp :: !(Maybe (CExpr c)) -> CUp c

