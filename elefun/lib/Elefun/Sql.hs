{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RankNTypes #-}
{-# OPTIONS_HADDOCK hide #-}

-- | This module exports an unsafe AST for generating SQL, and functions for
-- rendering said AST.
module Elefun.Sql where

import BasePrelude hiding (Fixity(..), (<>))
import Control.Lens (Traversal', Prism', prism', _1, both)
import Control.Lens.Plated (Plated(plate))
import qualified Data.Double.Conversion.ByteString as DCB
import qualified Data.ByteString.Builder as BB
import qualified Data.ByteString.Builder.Scientific as BB (scientificBuilder)
import qualified Data.ByteString.Builder.Prim as BBP
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy as BL
import Data.Hashable (Hashable)
import qualified Data.List.NonEmpty as NEL
import Data.Scientific (Scientific)
import Data.Semigroup (Semigroup((<>)))
import Data.String (IsString(fromString))
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TL
import qualified Test.QuickCheck as QC

--------------------------------------------------------------------------------
-- | Whether to quote or not something (e.g., a 'Name').
data Quote = Quote | NoQuote
  deriving (Eq, Show)

--------------------------------------------------------------------------------

-- | A valid PostgreSQL name.
--
-- SQL identifiers and key words must begin with a letter (a-z, but also
-- letters with diacritical marks and non-Latin letters) or an underscore (_).
-- Subsequent characters in an identifier or key word can be letters,
-- underscores, digits (0-9), or dollar signs ($). Note that dollar signs are
-- not allowed in identifiers according to the letter of the SQL standard, so
-- their use might render applications less portable. The SQL standard will not
-- define a key word that contains digits or starts or ends with an underscore,
-- so identifiers of this form are safe against possible conflict with future
-- extensions of the standard.
--
-- The system uses no more than NAMEDATALEN-1 bytes of an identifier; longer
-- names can be written in commands, but they will be truncated. By default,
-- NAMEDATALEN is 64 so the maximum identifier length is 63 bytes.
data Name
  = NameQuote B.ByteString
  | NameNoQuote B.ByteString
  deriving (Eq, Show, Generic)

instance Hashable Name

-- | 'NameQuote'
instance IsString Name where
  fromString = NameQuote . B8.pack

rName :: Name -> BB.Builder
rName = \case
  NameQuote x -> "\"" <> BB.byteString x <> "\""
  NameNoQuote x -> BB.byteString x
{-# INLINABLE rName #-}

--------------------------------------------------------------------------------
newtype TypeName = TypeName B.ByteString
  deriving (Eq, Show)

instance IsString TypeName where
  fromString = TypeName . B8.pack

rTypeName :: TypeName -> BB.Builder
rTypeName = \(TypeName x) -> BB.byteString x
{-# INLINABLE rTypeName #-}

--------------------------------------------------------------------------------
data Fixity = Infix | Prefix | Postfix
  deriving (Eq, Show)

--------------------------------------------------------------------------------
-- | An operator name (like @~@, @!@, or @NOT@).
newtype Op (a :: Fixity) = Op B.ByteString
  deriving (Eq, Show)

instance IsString (Op a) where
  fromString = Op . B8.pack

rOp :: Op x -> BB.Builder
rOp = \(Op x) -> BB.byteString x
{-# INLINABLE rOp #-}

--------------------------------------------------------------------------------

data OpApp
  = OpAppInfix Expr (Op 'Infix) Expr
  | OpAppPrefix (Op 'Prefix) Expr
  | OpAppPostfix Expr (Op 'Postfix)
  deriving (Eq, Show)

expr_opApp :: Traversal' OpApp Expr
expr_opApp f = \case
  OpAppInfix e1 o e2 -> OpAppInfix <$> f e1 <*> pure o <*> f e2
  OpAppPrefix o e -> OpAppPrefix o <$> f e
  OpAppPostfix e o -> OpAppPostfix <$> f e <*> pure o

rOpApp :: OpApp -> BB.Builder
rOpApp = \case
  OpAppInfix l o r -> rExpr l <> " " <> rOp o <> " " <> rExpr r
  OpAppPrefix o r -> rOp o <> " " <> rExpr r
  OpAppPostfix l o -> rExpr l <> " " <> rOp o
{-# INLINABLE rOpApp #-}

--------------------------------------------------------------------------------
newtype Schema = Schema Name
  deriving (Eq, Show)

rSchema :: Schema -> BB.Builder
rSchema = \(Schema x) -> rName x
{-# INLINABLE rSchema #-}

--------------------------------------------------------------------------------
data Identifier = Identifier (Maybe Schema) Name
  deriving (Eq, Show)

rIdentifier :: Identifier -> BB.Builder
rIdentifier = \(Identifier ys n) -> case ys of
  Nothing -> rName n
  Just s -> rSchema s <> "." <> rName n
{-# INLINABLE rIdentifier #-}

--------------------------------------------------------------------------------
newtype Table = Table Identifier
  deriving (Eq, Show)

rTable :: Table -> BB.Builder
rTable = \(Table i) -> rIdentifier i
{-# INLINABLE rTable #-}

--------------------------------------------------------------------------------
newtype Collation = Collation Identifier
  deriving (Eq, Show)

rCollation :: Collation -> BB.Builder
rCollation = \(Collation i) -> rIdentifier i
{-# INLINABLE rCollation #-}

--------------------------------------------------------------------------------
data Column = Column (Maybe Table) Name
  deriving (Eq, Show)

rColumn :: Column -> BB.Builder
rColumn = \(Column yt n) -> case yt of
  Nothing -> rName n
  Just t -> rTable t <> "." <> rName n
{-# INLINABLE rColumn #-}

--------------------------------------------------------------------------------

newtype PosParamRef = PosParamRef Word32
  deriving (Eq, Show)

rPosParamRef :: PosParamRef -> BB.Builder
rPosParamRef = \(PosParamRef x) -> "$" <> BB.word32Dec x
{-# INLINABLE rPosParamRef #-}

--------------------------------------------------------------------------------
-- | @
-- function_name ([expression [, expression ... ]] )
-- @
--
-- Keyword arguments are supported.
data FunApp = FunApp Name [Expr] [(Name, Expr)]
  deriving (Eq, Show)

expr_funApp :: Traversal' FunApp Expr
expr_funApp f = \case
  FunApp n es nes -> FunApp n <$> traverse f es <*> traverse (traverse f) nes

rFunApp :: FunApp -> BB.Builder
rFunApp = \(FunApp f as kws) -> mconcat
  [ rName f, "("
  , commas (map rExpr as)
  , si (not (null as) && not (null kws)) ", "
  , commas (map (\(k,v) -> rName k <> " := " <> rExpr v) kws)
  , ")" ]

--------------------------------------------------------------------------------

data AggrFunApp
  = AggrFunAppStar Name (Maybe Expr)
    -- ^ @aggregate_name ( * ) [ FILTER ( WHERE filter_clause ) ]@
  | AggrFunApp Name (Maybe Distinct) (NEL.NonEmpty Expr) (Maybe OrderBy) (Maybe Expr)
    -- ^ @aggregate_name (expression [ , ... ] [ order_by_clause ] ) [ FILTER ( WHERE filter_clause ) ]@
    --
    -- @aggregate_name (DISTINCT expression [ , ... ] [ order_by_clause ] ) [ FILTER ( WHERE filter_clause ) ]@
  | AggrFunAppWithinGroup Name [Expr] OrderBy (Maybe Expr)
    -- ^ @aggregate_name ( [ expression [ , ... ] ] ) WITHIN GROUP ( order_by_clause ) [ FILTER ( WHERE filter_clause ) ]@
 deriving (Eq, Show)

expr_aggrFunApp :: Traversal' AggrFunApp Expr
expr_aggrFunApp f = \case
  AggrFunAppStar n ye -> AggrFunAppStar n <$> traverse f ye
  AggrFunApp n yd es0 yo ye1 -> AggrFunApp n yd <$> traverse f es0 <*> pure yo <*> traverse f ye1
  AggrFunAppWithinGroup n es0 o ye1 -> AggrFunAppWithinGroup n <$> traverse f es0 <*> pure o <*> traverse f ye1

rAggrFunApp :: AggrFunApp -> BB.Builder
rAggrFunApp = \case
  AggrFunAppStar f yw -> mconcat
    [ rName f, "(*)", sy yw (\w -> " FILTER (WHERE " <> rExpr w <> ")") ]
  AggrFunApp f yd nse yob yw -> mconcat
    [ rName f, "(", sys yd rDistinct
    , commas (map rExpr (NEL.toList nse))
    , sy yob (\ob -> " " <> rOrderBy ob)
    , ")", sy yw (\w -> " FILTER (WHERE " <> rExpr w <> ")") ]
  AggrFunAppWithinGroup f ses ob yw -> mconcat
    [ rName f, "(" , commas (map rExpr ses)
    , ") WITHIN GROUP (", rOrderBy ob, ")"
    , sy yw (\w -> " FILTER (WHERE " <> rExpr w <> ")") ]

--------------------------------------------------------------------------------
data Frame
  = FramePrecedingUnbounded -- ^ @UNBOUNDED PRECEDING@
  | FramePreceding Expr -- ^ @value PRECEDING@
  | FrameFollowingUnbounded -- ^ @UNBOUNDED FOLLOWING@
  | FrameFollowing Expr -- ^ @value FOLLOWING@
  | FrameCurrentRow -- ^ @CURRENT ROW@
  deriving (Eq, Show)

expr_frame :: Traversal' Frame Expr
expr_frame f = \case
  FramePreceding e -> FramePreceding <$> f e
  FrameFollowing e -> FrameFollowing <$> f e
  x -> pure x

rFrame :: Frame -> BB.Builder
rFrame = \case
  FramePrecedingUnbounded -> "UNBOUNDED PRECEDING"
  FramePreceding a -> rExpr a <> " PRECEDING"
  FrameFollowingUnbounded -> "UNBOUNDED FOLLOWING"
  FrameFollowing a -> rExpr a <> " FOLLOWING"
  FrameCurrentRow -> "CURRENT ROW"
{-# INLINABLE rFrame #-}

--------------------------------------------------------------------------------

data FrameTarget = FrameTargetRange | FrameTargetRows
  deriving (Eq, Show)

rFrameTarget :: FrameTarget -> BB.Builder
rFrameTarget = \case { FrameTargetRange -> "RANGE";  FrameTargetRows -> "ROWS" }
{-# INLINABLE rFrameTarget #-}

--------------------------------------------------------------------------------

-- | @
-- { RANGE | ROWS } frame_start
-- { RANGE | ROWS } BETWEEN frame_start AND frame_end
-- @
data FrameClause
  = FrameClause FrameTarget Frame (Maybe Frame)
    -- ^ Target, start, optional end.
  deriving (Eq, Show)

expr_frameClause :: Traversal' FrameClause Expr
expr_frameClause g = \(FrameClause ft f yf) ->
  FrameClause ft <$> expr_frame g f <*> traverse (expr_frame g) yf

rFrameClause :: FrameClause -> BB.Builder
rFrameClause = \(FrameClause t s ye) -> mconcat
  [ rFrameTarget t, " "
  , case ye of Nothing -> rFrame s
               Just e -> "BETWEEN " <> rFrame s <> " AND " <> rFrame e ]
{-# INLINABLE rFrameClause #-}

--------------------------------------------------------------------------------
newtype OrderBy = OrderBy (NEL.NonEmpty (Expr, Order))
  deriving (Eq, Show)

instance Semigroup OrderBy where
  OrderBy a <> OrderBy b = OrderBy (a <> b)

expr_orderBy :: Traversal' OrderBy Expr
expr_orderBy f = \(OrderBy eos) -> OrderBy <$> traverse (_1 f) eos

rOrderBy :: OrderBy -> BB.Builder
rOrderBy = \(OrderBy neos) -> mappend "ORDER BY "
   (commas (map (\(e,o) -> rExpr (f e) <> " " <> rOrder o) (NEL.toList neos)))
 where
   f :: Expr -> Expr
   f = \e -> case e of
     ExprLiteral (LiteralNum{}) ->
        -- Necessary so that "ORDER BY 2" doesn't get interpreted as
        -- "order by the second column".
        ExprFunApp (FunApp (NameNoQuote "COALESCE") [e] [])
     _ -> e
{-# INLINABLE rOrderBy #-}

--------------------------------------------------------------------------------
-- | @
-- [ existing_window_name ]
-- [ PARTITION BY expression [, ...] ]
-- [ ORDER BY expression [ ASC | DESC | USING operator ] [ NULLS { FIRST | LAST } ] [, ...] ]
-- [ frame_clause ]
-- @
--
-- TODO: Everything is optional here? WTF?
data Window = Window (Maybe Name) [Expr] (Maybe OrderBy) (Maybe FrameClause)
  deriving (Eq, Show)

expr_window :: Traversal' Window Expr
expr_window f = \(Window yn es yo yfc) -> Window yn
  <$> traverse f es
  <*> traverse (expr_orderBy f) yo
  <*> traverse (expr_frameClause f) yfc

rWindow :: Window -> BB.Builder
rWindow = \(Window yn pes yob yfc) -> mconcat
  [ sy yn rName
  , si (isJust yn && not (null pes)) " "
  , si (not (null pes)) ("PARTITION BY " <> commas (map rExpr pes))
  , si (not (null pes) && isJust yob) " "
  , sy yob rOrderBy
  , si (isJust yob && isJust yfc) " "
  , sy yfc rFrameClause
  ]

--------------------------------------------------------------------------------

-- @
-- function_name ([expression [, expression ... ]]) [ FILTER ( WHERE filter_clause ) ] OVER window_name
-- function_name ([expression [, expression ... ]]) [ FILTER ( WHERE filter_clause ) ] OVER ( window_definition )
-- function_name ( * ) [ FILTER ( WHERE filter_clause ) ] OVER window_name
-- function_name ( * ) [ FILTER ( WHERE filter_clause ) ] OVER ( window_definition )
-- @
data WinFunApp = WinFunApp Name [Expr] (Maybe Expr) (Either Name Window)
  deriving (Eq, Show)

expr_winFunApp :: Traversal' WinFunApp Expr
expr_winFunApp f = \(WinFunApp n es0 ye1 enw) -> WinFunApp n
  <$> traverse f es0
  <*> traverse f ye1
  <*> traverse (expr_window f) enw


rWinFunApp :: WinFunApp -> BB.Builder
rWinFunApp = \(WinFunApp n es yfil ewin) -> mconcat
  [ rName n, parens (case es of { [] -> "*"; _  -> commas (map rExpr es) })
  , sy yfil (\e -> "FILTER (WHERE " <> rExpr e <> ") ")
  , "OVER ", either rName (parens . rWindow) ewin ]

--------------------------------------------------------------------------------
data OrderDirection = OrderAsc | OrderDesc | OrderUsing (Op 'Infix)
  deriving (Eq, Show)

instance QC.Arbitrary OrderDirection where
  arbitrary = QC.oneof [pure OrderAsc, pure OrderDesc]

rOrderDirection :: OrderDirection -> BB.Builder
rOrderDirection = \case
  OrderAsc -> "ASC"
  OrderDesc -> "DESC"
  OrderUsing o -> "USING " <> rOp o
{-# INLINABLE rOrderDirection #-}

--------------------------------------------------------------------------------
data OrderNulls = OrderNullsFirst | OrderNullsLast
  deriving (Eq, Show)

instance QC.Arbitrary OrderNulls where
  arbitrary = QC.oneof [pure OrderNullsFirst, pure OrderNullsLast]

rOrderNulls :: OrderNulls -> BB.Builder
rOrderNulls = \case
  OrderNullsFirst -> "NULLS FIRST"
  OrderNullsLast -> "NULLS LAST"
{-# INLINABLE rOrderNulls #-}

--------------------------------------------------------------------------------
data Order = Order OrderDirection OrderNulls
  deriving (Eq, Show)

instance QC.Arbitrary Order where
  arbitrary = Order <$> QC.arbitrary <*> QC.arbitrary

rOrder :: Order -> BB.Builder
rOrder = \(Order d n) -> rOrderDirection d <> " " <> rOrderNulls n
{-# INLINABLE rOrder #-}

--------------------------------------------------------------------------------
data Distinct = Distinct
  deriving (Eq, Show)

rDistinct :: Distinct -> BB.Builder
rDistinct = \Distinct -> "DISTINCT"
{-# INLINABLE rDistinct #-}

--------------------------------------------------------------------------------
data RangeBound
  = RangeBoundInfinity
  | RangeBoundInclusive Expr
  | RangeBoundExclusive Expr
  deriving (Eq, Show)

expr_rangeBound :: Traversal' RangeBound Expr
expr_rangeBound f = \case
  RangeBoundInfinity -> pure RangeBoundInfinity
  RangeBoundInclusive e -> RangeBoundInclusive <$> f e
  RangeBoundExclusive e -> RangeBoundExclusive <$> f e

--------------------------------------------------------------------------------
data Range = Range
  { lower :: RangeBound
  , upper :: RangeBound
  } deriving (Eq, Show)

rRange :: Range -> BB.Builder
rRange = \(Range l u) -> commas
  [ case l of RangeBoundInfinity -> "(-infinity"
              RangeBoundInclusive a -> "[" <> rExpr a
              RangeBoundExclusive a -> "(" <> rExpr a
  , case u of RangeBoundInfinity -> "infinity)"
              RangeBoundInclusive a -> rExpr a <> "]"
              RangeBoundExclusive a -> rExpr a <> ")" ]
{-# INLINABLE rRange #-}

--------------------------------------------------------------------------------
data Cast = Cast Expr TypeName
  deriving (Eq, Show)

expr_cast :: Traversal' Cast Expr
expr_cast f = \(Cast e tn) -> Cast <$> f e <*> pure tn

rCast :: Cast -> BB.Builder
rCast = \(Cast e t) -> "CAST(" <> rExpr e <> " AS " <> rTypeName t <> ")"
{-# INLINABLE rCast #-}

--------------------------------------------------------------------------------

-- | @expr COLLATE collation@
data Collate = Collate Expr Collation
  deriving (Eq, Show)

expr_collate :: Traversal' Collate Expr
expr_collate f = \(Collate e c) -> Collate <$> f e <*> pure c

rCollate :: Collate -> BB.Builder
rCollate = \(Collate e c) -> rExpr e <> " COLLATE " <> rCollation c
{-# INLINABLE rCollate #-}

--------------------------------------------------------------------------------

data Array
  = Array (NEL.NonEmpty Expr) -- ^ @ARRAY[x, y, z, ...]@
  | ArrayEmpty TypeName -- ^ @CAST(ARRAY[] AS typ[])@
  deriving (Eq, Show, Generic)

expr_array :: Traversal' Array Expr
expr_array f = \case
  Array nxs -> Array <$> traverse f nxs
  ArrayEmpty tn -> pure (ArrayEmpty tn)

rArray :: Array -> BB.Builder
rArray = \case
  Array nes -> "ARRAY[" <> commas (map rExpr (NEL.toList nes)) <> "]"
  ArrayEmpty tn -> "CAST(ARRAY[] AS " <> rTypeName tn <> "[])"
{-# INLINABLE rArray #-}

--------------------------------------------------------------------------------
-- | @array[index]@
data ArrayIndex
  = ArrayIndex Array Expr -- ^ Array, index (@int4@).
  deriving (Eq, Show)

expr_arrayIndex :: Traversal' ArrayIndex Expr
expr_arrayIndex f = \(ArrayIndex a e) -> ArrayIndex <$> expr_array f a <*> f e

rArrayIndex :: ArrayIndex -> BB.Builder
rArrayIndex (ArrayIndex ar ix) = "(" <> rArray ar <> ")[" <> rExpr ix <> "]"
{-# INLINABLE rArrayIndex #-}

-- | @(array)[lower:upper]@
data ArraySlice
  = ArraySlice Array Expr Expr -- ^ Array, lower (@int4@), upper (@int4@).
  deriving (Eq, Show)

expr_arraySlice :: Traversal' ArraySlice Expr
expr_arraySlice f = \(ArraySlice a e1 e2) ->
  ArraySlice <$> expr_array f a <*> f e1 <*> f e2

rArraySlice :: ArraySlice -> BB.Builder
rArraySlice = \(ArraySlice ar l u) ->
  "(" <> rArray ar <> ")[" <> rExpr l <> ":" <> rExpr u <> "]"
{-# INLINABLE rArraySlice #-}

--------------------------------------------------------------------------------
-- | @ROW(1, 2.5, 'this is a test', t.x)@
data CompositeConstructor = CompositeConstructor [Expr]
  deriving (Eq, Show)

expr_compositeConstructor :: Traversal' CompositeConstructor Expr
expr_compositeConstructor f =
  \(CompositeConstructor es) -> CompositeConstructor <$> traverse f es

rCompositeConstructor :: CompositeConstructor -> BB.Builder
rCompositeConstructor = \(CompositeConstructor es) ->
  "ROW(" <> commas (map rExpr es) <> ")"
{-# INLINABLE rCompositeConstructor #-}

--------------------------------------------------------------------------------
-- | @(composite).field@
data CompositeField
  = CompositeField Expr Name -- ^ Composite, field.
  deriving (Eq, Show)

expr_compositeField :: Traversal' CompositeField Expr
expr_compositeField f =
  \(CompositeField e n) -> CompositeField <$> f e <*> pure n

rCompositeField :: CompositeField -> BB.Builder
rCompositeField = \(CompositeField c f) -> "(" <> rExpr c <> ")." <> rName f
{-# INLINABLE rCompositeField #-}

--------------------------------------------------------------------------------
-- | @(composite).*@
newtype CompositeFields = CompositeFields Expr
  deriving (Eq, Show)

expr_compositeFields :: Traversal' CompositeFields Expr
expr_compositeFields f = \(CompositeFields e) -> CompositeFields <$> f e

rCompositeFields :: CompositeFields -> BB.Builder
rCompositeFields = \(CompositeFields c) -> "(" <> rExpr c <> ").*"
{-# INLINABLE rCompositeFields #-}

--------------------------------------------------------------------------------

newtype Param = Param BB.Builder

--------------------------------------------------------------------------------

data TextLit = TextLit TL.Text
  deriving (Eq, Show)

rTextLit :: TextLit -> BB.Builder
rTextLit = \(TextLit t) ->
  "E'" <> TL.encodeUtf8BuilderEscaped utf8PgEscaped t <> "'"

utf8PgEscaped :: BBP.BoundedPrim Word8
utf8PgEscaped =
   BBP.condB (==  0) (fixed2 (92,  48))  $  -- '\0'  ->  "\\0"
   BBP.condB (==  8) (fixed2 (92,  98))  $  -- '\b'  ->  "\\b"
   BBP.condB (==  9) (fixed2 (92, 116))  $  -- '\t'  ->  "\\t"
   BBP.condB (== 10) (fixed2 (92, 110))  $  -- '\n'  ->  "\\n"
   BBP.condB (== 13) (fixed2 (92, 114))  $  -- '\r'  ->  "\\r"
   BBP.condB (== 34) (fixed2 (92,  34))  $  -- '"'   ->  "\\\""
   BBP.condB (== 39) (fixed2 (92,  39))  $  -- '\''  ->  "\\'"
   BBP.condB (== 92) (fixed2 (92,  92))  $  -- '\\'  ->  "\\\\"
   BBP.liftFixedToBounded BBP.word8
 where
   {-# INLINE fixed2 #-}
   fixed2 :: (Word8, Word8) -> BBP.BoundedPrim Word8
   fixed2 x = BBP.liftFixedToBounded
     (const x BBP.>$< BBP.word8 BBP.>*< BBP.word8)
{-# INLINE utf8PgEscaped #-}

--------------------------------------------------------------------------------

data BytesLit = BytesLit BL.ByteString
  deriving (Eq, Show)

rBytesLit :: BytesLit -> BB.Builder
rBytesLit = \(BytesLit x) -> "E'\\\\x" <> BB.lazyByteStringHex x <> "'"
{-# INLINABLE rBytesLit #-}

--------------------------------------------------------------------------------

data NumLit
  = NumLitInt16 Int16
  | NumLitInt32 Int32
  | NumLitInt64 Int64
  | NumLitInteger Integer
  | NumLitScientific Scientific
  | NumLitFloat Float
  | NumLitDouble Double
  | NumLitNaN
  | NumLitInfinity
  | NumLitNegInfinity
  deriving (Eq, Show)

rNumLit :: NumLit -> BB.Builder
rNumLit = \case
  NumLitInt16 x -> BB.int16Dec x
  NumLitInt32 x -> BB.int32Dec x
  NumLitInt64 x -> BB.int64Dec x
  NumLitInteger x -> BB.integerDec x
  NumLitScientific x -> BB.scientificBuilder x
  NumLitFloat x -> BB.string7 (show x)  -- slow?
  NumLitDouble x -> BB.byteString (DCB.toShortest x)
  NumLitNaN -> "'NaN'"
  NumLitInfinity -> "'Infinity'"
  NumLitNegInfinity -> "'-Infinity'"
{-# INLINABLE rNumLit #-}

--------------------------------------------------------------------------------

newtype BoolLit = BoolLit Bool
  deriving (Eq, Show)

rBoolLit :: BoolLit -> BB.Builder
rBoolLit = \case { BoolLit True -> "TRUE"; BoolLit False -> "FALSE" }
{-# INLINABLE rBoolLit #-}

--------------------------------------------------------------------------------
data Literal
  = LiteralNum NumLit
  | LiteralBytes BytesLit
  | LiteralText TextLit
  | LiteralBool BoolLit
  deriving (Eq, Show)

rLiteral :: Literal -> BB.Builder
rLiteral = \case
  LiteralNum x -> rNumLit x
  LiteralBytes x -> rBytesLit x
  LiteralText x -> rTextLit x
  LiteralBool x -> rBoolLit x
{-# INLINABLE rLiteral #-}

--------------------------------------------------------------------------------

-- | A raw chunk of SQL
newtype Raw = Raw BL.ByteString
  deriving (Eq, Show)

rRaw :: Raw -> BB.Builder
rRaw = \(Raw x) -> BB.lazyByteString x
{-# INLINABLE rRaw #-}

--------------------------------------------------------------------------------

data In = In Expr (NEL.NonEmpty Expr)
  deriving (Eq, Show)

expr_in :: Traversal' In Expr
expr_in f = \(In e es) -> In <$> f e <*> traverse f es

rIn :: In -> BB.Builder
rIn = \(In x xs) -> rExpr x <> " IN " <> parens (commas (rExpr <$> toList xs))

--------------------------------------------------------------------------------

data Case = Case (NEL.NonEmpty (Expr, Expr)) Expr
  deriving (Eq, Show)

expr_case :: Traversal' Case Expr
expr_case f = \(Case nees e) -> Case <$> traverse (both f) nees <*> f e

rCase :: Case -> BB.Builder
rCase = \(Case pxs els) -> mconcat
  [ "CASE"
  , mconcat (map (\(p,x) -> " WHEN " <> rExpr p <> " THEN " <> rExpr x)
            (NEL.toList pxs))
  , " ELSE ", rExpr els, " END"
  ]

--------------------------------------------------------------------------------

data Expr
  = ExprColumn Column
  | ExprPosParamRef PosParamRef
  | ExprLiteral Literal
  | ExprArray Array
  | ExprArrayIndex ArrayIndex
  | ExprArraySlice ArraySlice
  | ExprCompositeField CompositeField
  | ExprCompositeFields CompositeFields
  | ExprCompositeConstructor CompositeConstructor
  | ExprOpApp OpApp
  | ExprFunApp FunApp
  | ExprAggrFunApp AggrFunApp
  | ExprWinFunApp WinFunApp
  | ExprCast Cast
  | ExprCollate Collate
  -- | ExprScalarSelect ScalarSelect -- Do we need this?
  | ExprCase Case
  | ExprIn In
  | ExprRaw Raw
  | ExprNull
  | ExprDefault
  deriving (Eq, Show, Generic)

_ExprColumn :: Prism' Expr Column
_ExprColumn = prism' ExprColumn (\case { ExprColumn x -> Just x; _ -> Nothing })

instance Plated Expr where
  plate g = \case
    ExprArray a -> ExprArray <$> expr_array g a
    ExprArrayIndex ai -> ExprArrayIndex <$> expr_arrayIndex g ai
    ExprArraySlice ai -> ExprArraySlice <$> expr_arraySlice g ai
    ExprCompositeField cf -> ExprCompositeField <$> expr_compositeField g cf
    ExprCompositeFields cfs ->
      ExprCompositeFields <$> expr_compositeFields g cfs
    ExprCompositeConstructor cc ->
      ExprCompositeConstructor <$> expr_compositeConstructor g cc
    ExprOpApp oa -> ExprOpApp <$> expr_opApp g oa
    ExprFunApp fa -> ExprFunApp <$> expr_funApp g fa
    ExprAggrFunApp afa -> ExprAggrFunApp <$> expr_aggrFunApp g afa
    ExprWinFunApp wfa -> ExprWinFunApp <$> expr_winFunApp g wfa
    ExprCast c -> ExprCast <$> expr_cast g c
    ExprCollate c -> ExprCollate <$> expr_collate g c
    ExprCase c -> ExprCase <$> expr_case g c
    ExprIn i -> ExprIn <$> expr_in g i
    x -> pure x


expr__Column :: Prism' Expr Column
expr__Column = prism' ExprColumn (\case { ExprColumn a -> Just a; _ -> Nothing })

{-# INLINABLE rExpr #-}
rExpr :: Expr -> BB.Builder
rExpr = \case
  ExprColumn x -> rColumn x
  ExprPosParamRef x -> rPosParamRef x
  ExprLiteral x -> rLiteral x
  ExprArray x -> rArray x
  ExprArrayIndex x -> rArrayIndex x
  ExprArraySlice x -> rArraySlice x
  ExprCompositeField x -> rCompositeField x
  ExprCompositeFields x -> rCompositeFields x
  ExprCompositeConstructor x -> rCompositeConstructor x
  ExprOpApp x -> rOpApp x
  ExprFunApp x -> rFunApp x
  ExprAggrFunApp x -> rAggrFunApp x
  ExprWinFunApp x -> rWinFunApp x
  ExprCast x -> rCast x
  ExprCollate x -> rCollate x
  ExprCase x -> rCase x
  ExprIn x -> rIn x
  ExprRaw x -> rRaw x
  ExprNull -> "NULL"
  ExprDefault -> "DEFAULT"

expr_andMaybe :: Maybe Expr -> Maybe Expr -> Maybe Expr
expr_andMaybe (Just a) (Just b) = Just (ExprOpApp (OpAppInfix a (Op "AND") b))
expr_andMaybe a b = a <|> b

true :: Expr
true = ExprLiteral (LiteralBool (BoolLit True))

false :: Expr
false = ExprLiteral (LiteralBool (BoolLit False))

--------------------------------------------------------------------------------
-- | A query that returns just one element (i.e., 1 row and 1 column).
data ScalarSelect = ScalarSelect Select
    -- ^ WARNING no guarantee that the query returns one element.
  deriving (Eq, Show)

rScalarSelect :: ScalarSelect -> BB.Builder
rScalarSelect = \(ScalarSelect x) -> parens (rSelect x)
{-# INLINABLE rScalarSelect #-}

--------------------------------------------------------------------------------
-- | @a x, b, c y, ...@
newtype SelectList = SelectList (NEL.NonEmpty (Expr, Maybe Name))
  deriving (Eq, Show)

instance Semigroup SelectList where
  SelectList a <> SelectList b = SelectList (a <> b)

rSelectList :: SelectList -> BB.Builder
rSelectList = \(SelectList neys) ->
  commas (fmap (\(e, yn) -> rExpr e <> sy yn (\n -> " " <> rName n)) -- Add "AS"?
               (NEL.toList neys))
{-# INLINABLE rSelectList #-}

--------------------------------------------------------------------------------

data Lateral = Lateral
  deriving (Eq, Show)

rLateral :: Lateral -> BB.Builder
rLateral = \Lateral -> "LATERAL"
{-# INLINABLE rLateral #-}

--------------------------------------------------------------------------------

data Only = Only
  deriving (Eq, Show)

rOnly :: Only -> BB.Builder
rOnly = \Only -> "ONLY"
{-# INLINABLE rOnly #-}

--------------------------------------------------------------------------------

-- | TODO missing stuff /https://www.postgresql.org/docs/9.6/static/sql-select.html
data From
  = FromTable Table (Maybe Only) (Maybe (Name, [Name]))
    -- ^ @table_name [ [ AS ] alias [ ( column_alias [, ...] ) ] ]@
  | FromSelect Select (Maybe Lateral) (Maybe (Name, [Name]))
    -- ^ @[ LATERAL ] ( select ) [ [ AS ] alias [ ( column_alias [, ...] ) ] ]@
  | FromJoin From Join
    -- ^ @from_itemL join_type from_itemR [ ON join_condition ]@
  | FromFunction FunApp (Maybe (Name, Either [Name] [(Name, TypeName)]))
    -- ^ @f( [ argument [, ...] ] ) [ AS alias [ ( column_alias [, ...] | column_definition [, ...] ) ] ]@
  deriving (Eq, Show)

-- | @CROSS JOIN@
instance Semigroup From where
  a <> b = FromJoin a (Join Inner b (ExprLiteral (LiteralBool (BoolLit True))))

{-# INLINABLE rFrom #-}
rFrom :: From -> BB.Builder
rFrom = \case
   FromTable t yo ynns -> sys yo rOnly <> rTable t <> sy ynns rAliases
   FromSelect s yl ynns ->
     sys yl rLateral <> parens (rSelect s) <> sy ynns rAliases
   FromJoin f j -> rFrom f <> " " <> rJoin j
   FromFunction f yx -> rFunApp f <>
     sy yx (\(n,ex) -> case ex of Left ns -> rAliases (n,ns)
                                  Right nts -> rColDefs (n,nts) )
  where
   {-# INLINABLE rAliases #-}
   rAliases :: (Name, [Name]) -> BB.Builder
   rAliases = \(n, ns) -> " " <> rName n <> case ns of  -- Add "AS"?
     [] -> mempty
     _  -> " " <> parens (commas (map rName ns))
   {-# INLINABLE rColDefs #-}
   rColDefs :: (Name, [(Name, TypeName)]) -> BB.Builder
   rColDefs = \(n, nts) -> " AS " <> rName n <> case nts of
     [] -> mempty
     _  -> let f = \(n',t) -> rName n' <> " " <> rTypeName t
           in " " <> parens (commas (map f nts))

--------------------------------------------------------------------------------

data JoinRel = Inner | LeftOuter | FullOuter
  deriving (Eq, Show)

--------------------------------------------------------------------------------

data Join = Join JoinRel From Expr
  deriving (Eq, Show)

rJoin :: Join -> BB.Builder
rJoin = \(Join rel f e) -> case rel of
  Inner -> case e of
     ExprLiteral (LiteralBool (BoolLit True)) -> "CROSS JOIN " <> rFrom f
     _ -> "JOIN " <> rFrom f <> " ON (" <> rExpr e <> ")"
  LeftOuter -> "LEFT JOIN " <> rFrom f <> " ON (" <> rExpr e <> ")"
  FullOuter -> "FULL JOIN " <> rFrom f <> " ON (" <> rExpr e <> ")"
{-# INLINABLE rJoin #-}

--------------------------------------------------------------------------------
-- | @WITH [ RECURSIVE ] with_query [, ...]@
data With
  = With Bool (NEL.NonEmpty (Name, Select))
  -- ^ The 'Bool' indicates whether @RECURSIVE@ or not.
  deriving (Eq, Show)

rWith :: With -> BB.Builder
rWith = \(With r nnss) -> mconcat
  [ "WITH ", si r "RECURSIVE "
  , commas (fmap (\(n,s) -> rName n <> " AS (" <> rSelect s <> ")")
                 (NEL.toList nnss)) ]
{-# INLINABLE rWith #-}

instance Semigroup With where
  With r1 ns1 <> With r2 ns2 = With (r1 || r2) (ns1 <> ns2)

--------------------------------------------------------------------------------

data Select
 = SRaw Raw
 | SUie SelectUie
 | SBld SelectBuilder
 deriving (Eq, Show)

rSelect :: Select -> BB.Builder
rSelect = \case
  SRaw x -> rRaw x
  SUie x -> rSelectUie x
  SBld x -> rSelectBuilder x

--------------------------------------------------------------------------------

data Sel
  = SelEmpty
  | SelRaw Raw
  | SelUie UieRel Sel Sel
  | SelUieAll UieRel Sel Sel
  | SelWith With  Sel
  | SelDistinct Sel
  | SelDistinctOn Expr Sel
  | SelList SelectList Sel
  | SelFrom From Sel
  | SelWhere Expr Sel
  | SelGroup Expr Sel
  | SelHaving Expr Sel
  | SelWindow Name Window Sel
  | SelOrder OrderBy Sel
  | SelLimit Expr Sel
  | SelOffset Expr Sel
  | SelFor For Sel
  deriving (Eq, Show)

{-# INLINABLE rSel #-}
rSel :: Sel -> BB.Builder
rSel = \x -> rSelect (runSel x)

{-# INLINABLE runSel #-}
runSel :: Sel -> Select
runSel = \case
  SelEmpty -> SBld mempty
  SelRaw x -> SRaw x
  SelUie x z1 z2 -> SUie (SelectUie x (Just Distinct) (runSel z1) (runSel z2))
  SelUieAll x z1 z2 -> SUie (SelectUie x Nothing (runSel z1) (runSel z2))
  -- The following just repeat the 'mappend' behavior of 'SelectBuilder'
  SelWith x z -> h z (\s -> s {sbWith = sbWith s <> Just x})
  SelDistinct z -> h z (\s -> s {sbDistinct = sbDistinct s <> Just []})
  SelDistinctOn x z -> h z (\s -> s {sbDistinct = sbDistinct s <> Just [x]})
  SelList x z -> h z (\s -> s {sbList = sbList s <> Just x})
  SelFrom x z -> h z (\s -> s {sbFrom = sbFrom s <> Just x})
  SelWhere x z -> h z (\s -> s {sbWhere = expr_andMaybe (sbWhere s) (Just x)})
  SelGroup x z -> h z (\s -> s {sbGroup = sbGroup s <> [x]})
  SelHaving x z -> h z (\s -> s {sbHaving = expr_andMaybe (sbHaving s) (Just x)})
  SelWindow n w z -> h z (\s -> s {sbWindow = sbWindow s <> [(n, w)]})
  SelOrder x z -> h z (\s -> s {sbOrder = sbOrder s <> Just x})
  SelLimit x z -> h z (\s -> s {sbLimit = sbLimit s <> Last (Just x)})
  SelOffset x z -> h z (\s -> s {sbOffset = sbOffset s <> Last (Just x)})
  SelFor x z -> h z (\s -> s {sbFor = sbFor s <> Just x})
 where
  h :: Sel -> (SelectBuilder -> SelectBuilder) -> Select
  h z f = case runSel z of { SBld sb -> SBld (f sb); s -> s }
  {-# INLINE h #-}

--------------------------------------------------------------------------------

-- | @
-- SELECT [ ALL | DISTINCT [ ON ( expression [, ...] ) ] ]
--     [ * | expression [ [ AS ] output_name ] [, ...] ]
--     [ FROM from_item [, ...] ]
--     [ WHERE condition ]
--     [ GROUP BY grouping_element [, ...] ]
--     [ HAVING condition [, ...] ]
--     [ WINDOW window_name AS ( window_definition ) [, ...] ]
--     [ { UNION | INTERSECT | EXCEPT } [ ALL | DISTINCT ] select ]
--     [ ORDER BY expression [ ASC | DESC | USING operator ] [ NULLS { FIRST | LAST } ] [, ...] ]
--     [ LIMIT { count | ALL } ]
--     [ OFFSET start ]
--     [ FOR { UPDATE | NO KEY UPDATE | SHARE | KEY SHARE } [ OF table_name [, ...] ] [ NOWAIT | SKIP LOCKED ] [...] ]
-- @
data SelectBuilder = SelectBuilder
   { sbWith :: Maybe With
     -- ^ Optional @WITH@ clause.
   , sbDistinct :: Maybe [Expr]
     -- ^ Optional @DISTINCT@ clause. An empty list means @DISTINCT@, a non
     -- empty means @DISTINCT ON (...)@.
   , sbList :: Maybe SelectList
     -- ^ Optional explicit list of 'Expr's to select. If empty, then @*@ is
     -- selected.
   , sbFrom :: Maybe From
     -- ^ Optional @FROM@ clause.
   , sbWhere :: Maybe Expr
     -- ^ Optional @WHERE@ clause with the given boolean 'Expr'.
   , sbGroup :: [Expr]
     -- ^ Optional @GROUP BY@ clause with the given 'Expr's.
   , sbHaving :: Maybe Expr
     -- ^ Optional @HAVING@ clause with the given boolean 'Expr'.
   , sbWindow :: [(Name, Window)]
     -- ^ Optional @WINDOW@ clause with the given window 'Name's and 'Window'
     -- definitions. E.g., @WINDOW n1 as (w1), n2 as (w2), ...@
   , sbOrder :: Maybe OrderBy
     -- ^ Optional @ORDER BY@ clause.
   , sbLimit :: Last Expr
     -- ^ Optional @LIMIT@ clause.
   , sbOffset :: Last Expr
     -- ^ Optional @OFFSET@ clause.
   , sbFor :: Maybe For
     -- ^ Optional @FOR@ clause.
   } deriving (Eq, Show)


instance Semigroup SelectBuilder
instance Monoid SelectBuilder where
  mempty = SelectBuilder
    { sbWith = Nothing
    , sbDistinct = Nothing
    , sbList = Nothing
    , sbFrom = Nothing
    , sbWhere = Nothing
    , sbGroup = []
    , sbHaving = Nothing
    , sbWindow = []
    , sbOrder = Nothing
    , sbLimit = Last Nothing
    , sbOffset = Last Nothing
    , sbFor = Nothing }
  mappend x y = SelectBuilder
    { sbWith = sbWith x <> sbWith y
    , sbDistinct = sbDistinct x <> sbDistinct y
    , sbList = sbList x <> sbList y
    , sbFrom = sbFrom x <> sbFrom y
    , sbWhere = expr_andMaybe (sbWhere x) (sbWhere y)
    , sbGroup = sbGroup x <> sbGroup y
    , sbHaving = expr_andMaybe (sbHaving x) (sbHaving y)
    , sbWindow = sbWindow x <> sbWindow y
    , sbOrder = sbOrder x <> sbOrder y
    , sbLimit = sbLimit x <> sbLimit y
    , sbOffset = sbOffset x <> sbOffset y
    , sbFor = sbFor x <> sbFor y }


rSelectBuilder :: SelectBuilder -> BB.Builder
rSelectBuilder = \s -> mconcat
 [ sys (sbWith s) rWith
 , "SELECT"
 , case sbDistinct s of
     Nothing -> mempty
     Just [] -> " DISTINCT"
     Just es -> " DISTINCT ON (" <> commas (map rExpr es) <> ")"
 , maybe " *" (\x -> " " <> rSelectList x) (sbList s)
 , sy (sbFrom s) (\x -> " FROM " <> rFrom x)
 , sy (sbWhere s) (\e -> " WHERE (" <> rExpr e <> ")")
 , case sbGroup s of
     [] -> mempty
     es -> " GROUP BY " <> commas (map rExpr es)
 , sy (sbHaving s) (\e -> " HAVING (" <> rExpr e <> ")")
 , case sbWindow s of
     [] -> mempty
     nws -> mappend " WINDOW "
       (commas (map (\(n,w) -> rName n <> " AS (" <> rWindow w <> ")") nws))
 , sy (sbOrder s) (\x -> " " <> rOrderBy x)
 , sy (getLast (sbLimit s)) (\x -> " LIMIT " <> rExpr x)
 , sy (getLast (sbOffset s)) (\x -> " OFFSET " <> rExpr x)
 , sy (sbFor s) (\x -> " " <> rFor x)
 ]

--------------------------------------------------------------------------------

data LockStrength
  = ForKeyShare
  | ForShare
  | ForNoKeyUpdate
  | ForUpdate
  deriving (Eq, Ord, Show)

-- | In @'mappend' a b@ the greater among them prevail ('Ord').
instance Semigroup LockStrength where
  a <> b = if a > b then a else b

rLockStrength :: LockStrength -> BB.Builder
rLockStrength = \case
  ForKeyShare -> "KEY SHARE"
  ForShare -> "SHARE"
  ForNoKeyUpdate -> "NO KEY UPDATE"
  ForUpdate -> "UPDATE"
{-# INLINABLE rLockStrength #-}

--------------------------------------------------------------------------------

data ForWait = SkipLocked | NoWait
  deriving (Eq, Ord, Show)

-- | In @'mappend' a b@ the greater among them prevail ('Ord').
instance Semigroup ForWait where
  a <> b = if a > b then a else b

rForWait :: ForWait -> BB.Builder
rForWait = \case
  NoWait -> "NOWAIT"
  SkipLocked -> "SKIP LOCKED"
{-# INLINABLE rForWait #-}

--------------------------------------------------------------------------------

data For = For LockStrength [Table] (Maybe ForWait)
  deriving (Eq, Show)

-- | Locks all tables with the minimum required lock strength and wait policy.
instance Semigroup For where
  For g1 ts1 ys1 <> For g2 ts2 ys2 = For (g1 <> g2) (ts1 <> ts2) (ys1 <> ys2)

rFor :: For -> BB.Builder
rFor = \(For g ts yw) -> mconcat
  [ "FOR ", rLockStrength g
  , si (not (null ts)) (" OF " <> commas (map rTable ts))
  , sy yw (\x -> " " <> rForWait x) ]
{-# INLINABLE rFor #-}

--------------------------------------------------------------------------------

-- @{ UNION | INTERSECT | EXCEPT } [ ALL | DISTINCT ] select@
data SelectUie = SelectUie UieRel (Maybe Distinct) Select Select
 deriving (Eq, Show)

rSelectUie :: SelectUie -> BB.Builder
rSelectUie = \(SelectUie r yd s1 s2) -> mconcat
  [ parens (rSelect s1), " ", rSelectUieRel r, " "
  , maybe "ALL " mempty yd
  , parens (rSelect s2) ]
{-# INLINABLE rSelectUie #-}

data UieRel = Union | Intersect | Except deriving (Eq, Show)

rSelectUieRel :: UieRel -> BB.Builder
rSelectUieRel = \case
  Union -> "UNION"
  Intersect -> "INTERSECT"
  Except -> "EXCEPT"
{-# INLINABLE rSelectUieRel #-}

--------------------------------------------------------------------------------

data InsertInput
  = InsertInputDefaults
  | InsertInputValues (NEL.NonEmpty Name) (NEL.NonEmpty (NEL.NonEmpty Expr))
    -- ^ WARNING: There's no guarantee that the number of column 'Name's matches
    -- the number of 'Exprs's
  | InsertInputSelect (NEL.NonEmpty Name) Select
    -- ^ WARNING: There's no guarantee that the number of column 'Name's matches
    -- the number resulting from 'Select'
  deriving (Eq, Show)

rInsertInput :: InsertInput -> BB.Builder
rInsertInput = \case
  InsertInputDefaults -> "DEFAULT VALUES"
  InsertInputValues ncs nness -> mconcat
    [ parens (commas (fmap rName (NEL.toList ncs)))
    , " VALUES "
    , commas (map (\nes -> parens (commas (map rExpr (NEL.toList nes))))
                  (NEL.toList nness)) ]
  InsertInputSelect ncs q ->
    parens (commas (fmap rName (NEL.toList ncs))) <> " " <> rSelect q

--------------------------------------------------------------------------------
newtype Returning = Returning (NEL.NonEmpty Expr)
  deriving (Eq, Show)

rReturning :: Returning -> BB.Builder
rReturning = \(Returning nses) ->
  "RETURNING " <> commas (fmap rExpr (NEL.toList nses))
{-# INLINABLE rReturning #-}

--------------------------------------------------------------------------------
--
data Insert = Insert Table InsertInput {- Maybe InsertConflict -} (Maybe Returning)
  deriving (Eq, Show)

rInsert :: Insert -> BB.Builder
rInsert = \(Insert t ii yr) -> mconcat
  [ "INSERT INTO ", rTable t, " ", rInsertInput ii
  , sy yr (\r -> " " <> rReturning r) ]
{-# INLINABLE rInsert #-}

--------------------------------------------------------------------------------

data Update = Update Table UpdateInput (Maybe Expr) (Maybe Returning)
  deriving (Eq, Show)

rUpdate :: Update -> BB.Builder
rUpdate = \(Update t ui yw yr) -> mconcat
  [ "UPDATE ", rTable t, " SET ", rUpdateInput ui
  , sy yw (\w -> " WHERE " <> rExpr w)
  , sy yr (\r -> " " <> rReturning r) ]
{-# INLINABLE rUpdate #-}

--------------------------------------------------------------------------------

data UpdateInput
  = UpdateInputValues (NEL.NonEmpty (Name, Expr))
  | UpdateInputSelect (NEL.NonEmpty Name) Select
  deriving (Eq, Show)

rUpdateInput :: UpdateInput -> BB.Builder
rUpdateInput = \case
  UpdateInputValues nnes ->
    commas (map (\(n, e) -> rName n <> " = " <> rExpr e) (NEL.toList nnes))
  UpdateInputSelect nns s ->
    parens (commas (map rName (NEL.toList nns))) <> " = " <> parens (rSelect s)
{-# INLINABLE rUpdateInput #-}

--------------------------------------------------------------------------------

data Delete = Delete Table (Maybe Expr) (Maybe Returning)
  deriving (Eq, Show)

rDelete :: Delete -> BB.Builder
rDelete = \(Delete t yw yr) -> mconcat
  [ "DELETE FROM ", rTable t
  , sy yw (\w -> " WHERE " <> rExpr w)
  , sy yr (\r -> " " <> rReturning r)
  ]

--------------------------------------------------------------------------------

commas :: [BB.Builder] -> BB.Builder
commas = \xs -> mconcat (intersperse ", " xs)
{-# INLINABLE commas #-}

-- | “Si” means “if” in Spanish.
si :: Bool -> BB.Builder -> BB.Builder
si = \x y -> if x then y else mempty
{-# INLINABLE si #-}

-- @'sy' f  ===  'maybe' 'mempty' f@
sy :: Maybe a -> (a -> BB.Builder) -> BB.Builder
sy = \y f -> case y of { Nothing -> mempty; Just a -> (f a) }
{-# INLINABLE sy #-}

-- | Like 'sy', but adds a trailing whitespace if 'Just'.
sys :: Maybe a -> (a -> BB.Builder) -> BB.Builder
sys = \y f -> sy y (\a -> f a <> " ")
{-# INLINABLE sys #-}

-- | Wrap in parentheses.
parens :: BB.Builder -> BB.Builder
parens = \x -> "(" <> x <> ")"
{-# INLINABLE parens #-}

ex1 :: Expr
ex1 = ExprArray (Array (NEL.fromList xs))
  where xs = [ ExprNull, ExprColumn (Column Nothing "bar"), ExprArray (Array (NEL.fromList ys)) ]
        ys = [ ExprNull, ExprColumn (Column Nothing "qux") ]

tf :: Expr -> Expr
tf = \case
  ExprColumn (Column _ _) -> ExprColumn (Column Nothing "RRRR")
  x -> x

