{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE PolyKinds #-}
{-# OPTIONS_HADDOCK hide #-}

module Elefun.Util
 ( I(I, unI)
 , K(K, unK)
 , R0(R0)
 , R1(R1)
 , P0(P0)
 , P1(P1)
 , P2(P2)
 , P3(P3)
 , P4(P4)
 , mapConcurrentlyCPU
 , forConcurrentlyCPU
 , mapConcurrentlyIO
 , forConcurrentlyIO
 , ffor
 , showsTypeRepWithModule
 , NoInstance
 , bsToPositiveIntegral
 ) where

import BasePrelude
import Control.Concurrent.Async (mapConcurrently)
import Data.ByteString.Char8 as B8
import Flay (Dict(Dict), Flayable1, Flayable, flay1, flay)
import qualified Flay
import GHC.Exts (Constraint)
import qualified GHC.TypeLits as GHC

--------------------------------------------------------------------------------

-- | Like 'Data.Functor.Identity' from "Data.Functor.Identity", but with a
-- shorter name.
--
-- Note: Ideally we'd just have a @pattern I a = 'Identity' a@, but that leads
-- to silly compiler warnings until https://ghc.haskell.org/trac/ghc/ticket/8779
-- is fixed.
newtype I a = I { unI :: a }
  deriving (Eq, Ord, Show, Read, Generic, Data)

instance Functor I where
  fmap f = \(I a) -> I (f a)
  {-# INLINE fmap #-}

instance Applicative I where
  pure = I
  {-# INLINE pure #-}
  I f <*> I x = I (f x)
  {-# INLINE (<*>) #-}

instance Monad I where
  I a >>= f = f a
  {-# INLINE (>>=) #-}

--------------------------------------------------------------------------------

-- | Like 'Const' from "Data.Functor.Const", but with a shorter name.
--
-- Note: Ideally we'd just have a @pattern K a = 'Const' a@, but that leads
-- to silly compiler warnings until https://ghc.haskell.org/trac/ghc/ticket/8779
-- is fixed.
newtype K a b = K { unK :: a }
  deriving (Eq, Ord, Show, Read, Generic, Data)

instance Functor (K a) where
  fmap _ = \(K a) -> K a
  {-# INLINE fmap #-}

instance Monoid a => Applicative (K a) where
  pure = \_ -> K mempty
  {-# INLINE pure #-}
  K f <*> K x = K (getConst (Const f <*> Const x))
  {-# INLINE (<*>) #-}

instance Flay.Terminal (K () a) where
  terminal = K ()
  {-# INLINE terminal #-}

--------------------------------------------------------------------------------

data R0 f = R0
  deriving (Eq, Show, Generic, Data)

instance Flay.Terminal (R0 f) where
  terminal = R0
  {-# INLINE terminal #-}

instance Flayable1 c R0 where
  flay1 _ = \R0 -> pure R0
  {-# INLINE flay1 #-}

instance Flayable c (R0 f) (R0 g) f g where
  flay _ = \R0 -> pure R0
  {-# INLINE flay #-}

--------------------------------------------------------------------------------

data R1 a f = R1 !(f a)
  deriving (Eq, Show, Generic, Data)

instance c a => Flayable1 c (R1 a) where
  flay1 h = \(R1 a) -> R1 <$> h Dict a
  {-# INLINE flay1 #-}

instance Flayable1 c (R1 a) => Flayable c (R1 a f) (R1 a g) f g where
  flay = flay1
  {-# INLINE flay #-}


--------------------------------------------------------------------------------

data P0 z = P0
  deriving (Eq, Show, Generic, Data)

instance Flayable1 c P0 where
  flay1 _ = \P0 -> pure P0
  {-# INLINE flay1 #-}

instance Flayable1 c P0 => Flayable c (P0 y) (P0 z) y z where
  flay = flay1
  {-# INLINE flay #-}

---

data P1 a z = P1 !(a z)
  deriving (Eq, Show, Generic, Data)

instance Flayable1 c a => Flayable1 c (P1 a) where
  flay1 h = \(P1 a) -> P1 <$> flay1 h a
  {-# INLINE flay1 #-}

instance Flayable1 c (P1 a) => Flayable c (P1 a y) (P1 a z) y z where
  flay = flay1
  {-# INLINE flay #-}

---

data P2 a b z = P2 !(a z) !(b z)
  deriving (Eq, Show, Generic, Data)

instance (Flayable1 c a, Flayable1 c b) => Flayable1 c (P2 a b) where
  flay1 h = \(P2 a b) -> P2 <$> flay1 h a <*> flay1 h b
  {-# INLINE flay1 #-}

instance Flayable1 c (P2 a b) => Flayable c (P2 a b y) (P2 a b z) y z where
  flay = flay1
  {-# INLINE flay #-}

---

data P3 a b c z = P3 !(a z) !(b z) !(c z)
  deriving (Eq, Show, Generic, Data)

instance (Flayable1 c' a, Flayable1 c' b, Flayable1 c' c)
  => Flayable1 c' (P3 a b c) where
  flay1 h = \(P3 a b c) -> P3 <$> flay1 h a <*> flay1 h b <*> flay1 h c
  {-# INLINE flay1 #-}

instance Flayable1 c' (P3 a b c)
  => Flayable c' (P3 a b c y) (P3 a b c z) y z where
  flay = flay1
  {-# INLINE flay #-}

---

data P4 a b c d z = P4 !(a z) !(b z) !(c z) !(d z)
  deriving (Eq, Show, Generic, Data)

instance (Flayable1 c' a, Flayable1 c' b, Flayable1 c' c, Flayable1 c' d)
  => Flayable1 c' (P4 a b c d) where
  flay1 h = \(P4 a b c d) ->
    P4 <$> flay1 h a <*> flay1 h b <*> flay1 h c <*> flay1 h d
  {-# INLINE flay1 #-}

instance Flayable1 c' (P4 a b c d)
  => Flayable c' (P4 a b c d y) (P4 a b c d z) y z where
  flay = flay1
  {-# INLINE flay #-}

--------------------------------------------------------------------------------

-- | Like 'mapConcurrently', but better when the tasks to run are CPU-bound.
--
-- Prefer 'mapConcurrentlyCPU' if the tasks are IO-bound.
mapConcurrentlyCPU :: Traversable t => (a -> IO b) -> t a -> IO (t b)
mapConcurrentlyCPU f = \xs -> do
  sem <- newQSem =<< getNumCapabilities
  mapConcurrently (\x -> bracket_ (waitQSem sem) (signalQSem sem) (f x)) xs

-- | @'forConcurrentlyCPU' == 'flip' 'mapConcurrentlyCPU'@
forConcurrentlyCPU :: Traversable t => t a -> (a -> IO b) -> IO (t b)
forConcurrentlyCPU xs f = mapConcurrentlyCPU f xs

-- | This is 'mapConcurrently'. Suitable when the tasks to run are IO-bound.
--
-- Prefer 'mapConcurrentlyCPU' if the tasks are CPU-bound.
mapConcurrentlyIO :: Traversable t => (a -> IO b) -> t a -> IO (t b)
mapConcurrentlyIO = mapConcurrently

-- | @'forConcurrentlyIO' == 'flip' 'mapConcurrentlyIO'@
forConcurrentlyIO :: Traversable t => t a -> (a -> IO b) -> IO (t b)
forConcurrentlyIO xs f = mapConcurrentlyIO f xs

--------------------------------------------------------------------------------

ffor :: Functor f => f a -> (a -> b) -> f b
ffor x g = fmap g x
{-# INLINE ffor #-}

--------------------------------------------------------------------------------

showsTypeRepWithModule :: TypeRep -> ShowS
{-# INLINE showsTypeRepWithModule #-}
showsTypeRepWithModule tr =
  showString (tyConModule (typeRepTyCon tr)) . showString "." . showsPrec 11 tr

--------------------------------------------------------------------------------

type family NoInstance (i :: ki) (t :: kt) :: Constraint where
  NoInstance i t = GHC.TypeError
    ('GHC.Text "The " 'GHC.:<>: 'GHC.ShowType i 'GHC.:<>:
     'GHC.Text " instance for " 'GHC.:<>: 'GHC.ShowType i 'GHC.:<>:
     'GHC.Text " is explicitely disabled.")


--------------------------------------------------------------------------------

bsToPositiveIntegral :: Integral n => B8.ByteString -> Maybe n
bsToPositiveIntegral = B8.foldl' f (Just 0)
  where f (Just acc) c | '0' <= c && c <= '9'
          = Just (10 * acc + fromIntegral (ord c - ord '0'))
        f _ _ = Nothing
