{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE PolyKinds #-}
{-# OPTIONS_HADDOCK hide #-}

module Elefun.Conn where

import BasePrelude
import qualified Control.Exception as Ex
import Control.Monad.IO.Class (MonadIO, liftIO)
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Database.PostgreSQL.LibPQ as Pq

--------------------------------------------------------------------------------

newtype Conn (d :: k) = Conn (MVar (Maybe Pq.Connection))

--------------------------------------------------------------------------------

connect :: MonadIO m => B.ByteString -> m (Conn d)
connect = \bs -> liftIO $ Pq.connectdb bs >>= \c -> Conn <$> newMVar (Just c)

close :: MonadIO m => Conn d -> m ()
close = \(Conn mv) -> liftIO $ modifyMVar_ mv $ \y -> Nothing <$ for y Pq.finish

--------------------------------------------------------------------------------

withRawConn :: Conn d -> (Pq.Connection -> IO a) -> IO a
withRawConn = \(Conn mv) f -> withMVar mv $ \case
   Just c -> f c
   Nothing -> Ex.throwIO Err_ConnectionClosed

data Err_ConnectionClosed = Err_ConnectionClosed deriving (Show)
instance Ex.Exception Err_ConnectionClosed

--------------------------------------------------------------------------------

exec :: MonadIO m => Conn d -> BL.ByteString -> m Pq.Result
exec = \conn bl -> liftIO $ withRawConn conn $ \c -> do
   Pq.exec c (BL.toStrict bl) >>= \case
      Just r -> pure r
      Nothing -> do
         ymsg <- Pq.errorMessage c
         Ex.throwIO (Err_LibPq (maybe "" id ymsg))

data Err_LibPq = Err_LibPq B.ByteString deriving (Show)
instance Ex.Exception Err_LibPq

