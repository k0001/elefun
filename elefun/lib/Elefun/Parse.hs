{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableSuperClasses #-}
{-# OPTIONS_HADDOCK hide #-}

module Elefun.Parse
  ( RawField(RawField, _rawFieldOid, _rawFieldFormat, _rawFieldBytes)
  , Parser(Parser, unParser)
  , runParser
  , checkOid
  , bytes
  , atto
  , FromE(fromE)
  ) where

import BasePrelude
import Control.Monad.Fail (MonadFail(fail))
import qualified Data.Attoparsec.ByteString as AB
import qualified Data.Attoparsec.ByteString.Char8 as A8
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import Data.Kind (Type)
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text
import qualified Data.Text.Lazy as TextL
import qualified Data.Text.Lazy.Encoding as TextL
import Data.Word (Word32)
import qualified Database.PostgreSQL.LibPQ as Pq

import qualified Elefun.Expr as E
import Elefun.Util (showsTypeRepWithModule)

--------------------------------------------------------------------------------

data RawField (p :: Type) = RawField
  { _rawFieldOid :: !Pq.Oid
  , _rawFieldFormat :: !Pq.Format
  , _rawFieldBytes :: !B.ByteString
  } deriving (Eq, Show)

--------------------------------------------------------------------------------

-- | Parser from a not-null PostgreSQL type @t@ (e.g., 'E.PgInt2') to a Haskell
-- type @r@ (e.g., 'Int16').
newtype Parser (a :: Type) (b :: Type)
  = Parser { unParser :: RawField a -> Either String b }
  deriving (Functor)

runParser
  :: forall a b
  .  (Typeable a, Typeable b)
  => Parser a b -> RawField a -> Either String b  -- ^
{-# INLINE runParser #-}
runParser p rf = case unParser p rf of
  Right b -> Right b
  Left x -> do
    let tra = typeRep (Proxy :: Proxy a)
        trb = typeRep (Proxy :: Proxy b)
    Left (showString "Parser " $ showsPrec 11 tra $ showString " " $
          showsTypeRepWithModule trb $ showString ": " $ x)

instance Applicative (Parser a) where
  pure = \r -> Parser (\_ -> Right r)
  {-# INLINE pure #-}
  Parser f <*> Parser x = Parser (\raw -> f raw <*> x raw)
  {-# INLINE (<*>) #-}

instance Alternative (Parser a) where
  empty = Parser (\_ -> Left "empty")
  {-# INLINE empty #-}
  Parser f <|> Parser x = Parser (\raw -> f raw <|> x raw)
  {-# INLINE (<|>) #-}

instance Monad (Parser a) where
  Parser a >>= k = Parser (\raw ->
     case a raw of
        Left x -> Left x
        Right x -> unParser (k x) raw)
  {-# INLINE (>>=) #-}

instance MonadPlus (Parser a) where
  mzero = empty
  {-# INLINE mzero #-}
  mplus = (<|>)
  {-# INLINE mplus #-}

instance MonadFail (Parser a) where
  fail x = Parser (\_ -> Left x)
  {-# INLINE fail #-}

checkOid :: [Word32] -> Parser a ()
{-# INLINABLE checkOid #-}
checkOid oids = Parser $ \rf -> do
  let oid0 :: Word32 = case _rawFieldOid rf of Pq.Oid x -> fromIntegral x
  case elem oid0 oids of
     True -> Right ()
     False -> Left ("OID " ++ show oid0 ++ " is not among the expected OIDs: "
                           ++ show oids)

bytes :: (B.ByteString -> Either String b) -> Parser a b
{-# INLINE bytes #-}
bytes f = Parser (\rf -> case f (_rawFieldBytes rf) of
  Right a -> Right a
  Left x -> Left ("Error parsing raw bytes: " ++ x))

atto :: AB.Parser b -> Parser a b
{-# INLINE atto #-}
atto p = bytes (AB.parseOnly p)

--------------------------------------------------------------------------------

-- | A default @'Parser' a b@.
class E.PgTyped a => FromE a b where
  fromE :: Parser a b

instance FromE E.PgInt2 Int16 where
  fromE = checkOid [21] >> atto (A8.signed A8.decimal)
  {-# INLINE fromE #-}

instance FromE E.PgInt2 Int32 where
  fromE = fmap fromIntegral (fromE :: Parser E.PgInt2 Int16)
  {-# INLINE fromE #-}
instance FromE E.PgInt4 Int32 where
  fromE = checkOid [23] >> atto (A8.signed A8.decimal)
  {-# INLINE fromE #-}

instance FromE E.PgInt2 Int64 where
  fromE = fmap fromIntegral (fromE :: Parser E.PgInt2 Int16)
  {-# INLINE fromE #-}
instance FromE E.PgInt4 Int64 where
  fromE = fmap fromIntegral (fromE :: Parser E.PgInt4 Int32)
  {-# INLINE fromE #-}

-- | Assumes UTF-8 encoding.
instance FromE E.PgText Text.Text where
  {-# INLINE fromE #-}
  fromE = do
    checkOid [25, 19, 18, 1042, 1043]
    bytes (\bs -> first show (Text.decodeUtf8' bs))

-- | Assumes UTF-8 encoding.
instance FromE E.PgText TextL.Text where
  {-# INLINE fromE #-}
  fromE = do
    checkOid [25, 19, 18, 1042, 1043]
    bytes (\bs -> first show (TextL.decodeUtf8' (BL.fromStrict bs)))

-- | Assumes UTF-8 encoding.
instance FromE E.PgText [Char] where
  fromE = fmap Text.unpack (fromE :: Parser E.PgText Text.Text)
  {-# INLINE fromE #-}

