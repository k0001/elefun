{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

-- | This module is /designed/ to be imported qualified as follows:
--
-- @import qualified "Elefun" as L@
module Elefun
 ( Conn.Conn
 , Conn.connect
 , Conn.close
 , Conn.Err_LibPq(Err_LibPq)
 , Run.select
 , Run.insert

 , Col.C
 , type Col.CEp
 , type Col.CEh
 , type Col.CEn
 , type Col.CEd
 , type Col.CNp
 , type Col.CNh
 , type Col.CNn
 , type Col.CNd
 , Col.C_en
 , Col.C_p
 , Col.C_h
 , Col.C_n

 , Col.Default(Col.Default, Col.Explicit)

 , Col.CParser(CParser)

 , Parse.Parser
 , Parse.checkOid
 , Parse.bytes
 , Parse.atto
 , Parse.FromE(fromE)

 , Col.CExpr
 , Col.ce
 , Col.cn
 , Col.ec
 , Col.ecp
 , Col.ech
 , Col.ecn
 , Col.ecd
 , Col.nc
 , Col.ncp
 , Col.nch
 , Col.ncn
 , Col.ncd

 , Col.CIn(CIn)

 , Col.COut(COutE, COutN)

 , Query.SchemaName(SchemaName)
 , Query.TableName(TableName)

 , Query.Q
 , Query.input
 , Query.limit
 , Query.offset
 , Query.fromTable
 , Query.innerJoin
 , Query.crossJoin
 , Query.leftJoin
 , Query.fullJoin
 , Query.kleisli
 , Query.unkleisli
 , Query.initial
 , Query.terminal

 , Expr.PgArray
 , Expr.pgArray

 , Expr.PgArrayN
 , Expr.pgArrayN

 , Expr.PgBool
 , Expr.pgBool
 , Expr.true
 , Expr.false

 , Expr.PgBytea
 , Expr.pgBytea

 , Expr.PgCitext

 , Expr.PgDate

 , Expr.PgFloat4
 , Expr.pgFloat4

 , Expr.PgFloat8
 , Expr.pgFloat8

 , Expr.PgInt2
 , Expr.pgInt2

 , Expr.PgInt4
 , Expr.pgInt4

 , Expr.PgInt8
 , Expr.pgInt8

 , Expr.PgJsonb
 , Expr.unsafePgJsonb

 , Expr.PgJson

 , Expr.PgText
 , Expr.pgText

 , Expr.PgTimestamptz

 , Expr.PgTimestamp

 , Expr.PgTime

 , Expr.PgUuid
 , Expr.pgUuid

 , Expr.PgNumeric
 , Expr.pgRational
 , Expr.pgScientific
 , Expr.pgFixed

 , Expr.PgPrimType(pgPrimTypeName)
 , Expr.PgTyped(PgType)

 , Expr.E(UnsafeE, unE)
 , Expr.rE
 , Expr.rE'
 , Expr.CastE
 , Expr.castE
 , Expr.upcastE
 , Expr.unsafeDowncastE
 , Expr.unsafeCoerceE
 , Expr.unsaferCoerceE
 , Expr.unsaferCastE

 , Expr.N(UnsafeN, unN)
 , Expr.rN
 , Expr.rN'
 , Expr.en
 , Expr.liftN2
 , Expr.matchN
 , Expr.mapN
 , Expr.altN
 , Expr.bindN
 , Expr.forN
 , Expr.fromN

 , Expr.ToE(e)

 , Expr.reSub
 , Expr.reSplit
 , Expr.reReplace
 , Expr.reReplaceg
 , Expr.reMatch

 , Expr.toTimestamptz
 , Expr.toTimestamp
 , Expr.tstzEpoch
 , Expr.tsCentury
 , Expr.tsDay
 , Expr.tsDayOfTheWeek
 , Expr.tsDayOfTheWeekISO8601
 , Expr.tsDayOfTheYear
 , Expr.tsDecade
 , Expr.tsHour
 , Expr.tsMicroseconds
 , Expr.tsMillenium
 , Expr.tsMilliseconds
 , Expr.tsMinute
 , Expr.tsMonth
 , Expr.tsQuarter
 , Expr.tsSecond
 , Expr.tsWeekISO8601
 , Expr.tsYear
 , Expr.tsYearISO8601

 , Expr.nowClock
 , Expr.nowStatement
 , Expr.nowTransaction

 , Expr.bwand
 , Expr.bwor
 , Expr.bwxor
 , Expr.bwnot
 , Expr.bwsl
 , Expr.bwsr

 , Expr.eq
 , Expr.eqN
 , Expr.elem

 , Expr.lt
 , Expr.lte
 , Expr.gt
 , Expr.gte

 , Expr.not
 , Expr.and
 , Expr.ands
 , Expr.or
 , Expr.ors

 , Expr.bool

 , Expr.round
 , Expr.truncate
 , Expr.ceiling
 , Expr.floor

 , Expr.mod

 , Util.I(I, unI)
 , Util.K(K, unK)
 , Util.R1(R1)
 , Util.P2(P2)
 ) where

import qualified Elefun.Col as Col
import qualified Elefun.Conn as Conn
import qualified Elefun.Expr as Expr
import qualified Elefun.Parse as Parse
import qualified Elefun.Query as Query
import qualified Elefun.Run as Run
import qualified Elefun.Util as Util

