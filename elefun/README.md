# elefun

Strongly typed PostgreSQL client in Haskell.

> See the Apache-2.0
> [LICENSE.txt](https://gitlab.com/k0001/elefun/blob/master/LICENSE.txt)
> file to learn more about legal terms and conditions for this library.

**Build this project** using [Nix](https://nixos.org/nix), by running
`nix-build` inside this folder.

**Hack on this project** using [Nix](https://nixos.org/nix), by running
`nix-shell` inside this folder. This puts you in a shell where you can use use
`cabal` or `Setup.hs` as usual. Notice that executing `./Setup.hs` will
automatically enter the `nix-shell` for you. So, for example, running
`./Setup.hs repl elefun` works even from outside the `nix-shell` environment.

**Test this project** by building it using `nix-build`, as described above.
Among other things, Nix sets up a temporal PostgreSQL database when running the
tests. Alternatively, if you want to run the tests without running `nix-build`,
you can execute `./test.sh` from within or from outside the `nix-shell`. Notice
that `./test.sh` expects you to have run `./Setup.hs configure --enable-test` or
similar beforehand.

**Depend on this project** by referring to the `pkg.nix` package definition on
this folder. That is, when extending your Nix Haskell package set, use something
like:

```
pkgs.haskell.packages.ghc822.override {
  packageSetConfig = self: super: {
    elefun = super.callPackage (pkgs.fetchgit {
      url = "https://gitlab.com/k0001/elefun.git";
      rev = "master"; # replace `master` with the git commit ID you want.
      sha256 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    } + "/elefun/pkg.nix") {};
  };
};
```
