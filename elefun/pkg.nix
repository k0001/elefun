{ pkgs, mkDerivation, aeson, async, base, base-prelude, bytestring
, case-insensitive, double-conversion, attoparsec, ex-pool
, flay, ghc-prim, hashable, lens, postgresql-libpq, profunctors
, QuickCheck, safe-exceptions, scientific, stdenv, tagged, tasty, tasty-hunit
, tasty-quickcheck, text, time, transformers, unordered-containers
, uuid-types, vector, postgresql
}:

let
# A path filtering function that only succeeds for paths that
# match none of 'exclude' and at least one of 'include'.
# TODO Upstream this to nixpkgs
filterSourceRegex = { include, exclude }: src:
  let rel = path: pkgs.lib.removePrefix (toString src + "/") (toString path);
      pred = path:
        let matches = pkgs.lib.any (re: builtins.match re (rel path) != null);
        in !(matches exclude) && (matches include);
  in builtins.filterSource (path: type: pred path) src;

in
mkDerivation {
  pname = "elefun";
  version = "0.1";
  src = filterSourceRegex
    { include = [ "^lib(/.+)*$" "^test(/.+)*$" "^test\.sh$"
                  "^elefun.cabal$" "^Setup.hs$" "^NOTICE.txt$"
                  "^README.md$" "^LICENSE.txt$" "^CHANGELOG.md$" ];
      exclude = [ "^.+/\\..+\\.sw[a-p]$" "^.*~$" ];
    } ./.;
  libraryHaskellDepends = [
    attoparsec aeson async base base-prelude bytestring
    case-insensitive double-conversion flay ghc-prim hashable lens
    postgresql-libpq profunctors QuickCheck scientific tagged text time
    transformers unordered-containers uuid-types vector
  ];
  testHaskellDepends = [
    base base-prelude bytestring ex-pool flay postgresql-libpq safe-exceptions
    tasty tasty-hunit tasty-quickcheck vector
  ];
  testSystemDepends = [ postgresql ];
  homepage = "https://gitlab.com/k0001/elefun";
  description = "Strongly typed PostgreSQL client";
  license = stdenv.lib.licenses.asl20;
  checkPhase = "bash ./test.sh";
}
