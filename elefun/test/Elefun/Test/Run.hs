{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE UndecidableInstances #-}

module Elefun.Test.Run (tt) where

import BasePrelude
import qualified Data.ByteString.Lazy as BL
import qualified Data.Pool as Pool
import qualified Data.Vector as V
import qualified Database.PostgreSQL.LibPQ as Pq
import qualified Flay
import qualified Test.Tasty as Tasty
import Test.Tasty.HUnit ((@?=))
import qualified Test.Tasty.HUnit as HU

import qualified Elefun as L
import qualified Elefun.Conn

--------------------------------------------------------------------------------

data Foo f = Foo
  { foo_i2ed :: f (L.CEd L.PgInt2 Int16 "i2ed")
  , foo_ted :: f (L.CEd L.PgText String "ted")
  } deriving (Generic)

instance
  ( c (L.CEd L.PgInt2 Int16 "i2ed")
  , c (L.CEd L.PgText String "ted")
  ) => Flay.Flayable1 c Foo

deriving instance
  ( Show (f (L.CEd L.PgInt2 Int16 "i2ed"))
  , Show (f (L.CEd L.PgText String "ted"))
  ) => Show (Foo f)

deriving instance
  ( Eq (f (L.CEd L.PgInt2 Int16 "i2ed"))
  , Eq (f (L.CEd L.PgText String "ted"))
  ) => Eq (Foo f)

qFoo :: L.Q "elefun" () (Foo L.CExpr)
qFoo = L.fromTable "elefun" "foo"

---
data Bar f = Bar
  { bar_te :: f (L.CEn L.PgText String "te")
  , bar_i4n :: f (L.CNn L.PgInt4 Int32 "i4n")
  } deriving (Generic)

instance
  ( c (L.CEn L.PgText String "te")
  , c (L.CNn L.PgInt4 Int32 "i4n")
  ) => Flay.Flayable1 c Bar

deriving instance
  ( Show (f (L.CEn L.PgText String "te"))
  , Show (f (L.CNn L.PgInt4 Int32 "i4n"))
  ) => Show (Bar f)

deriving instance
  ( Eq (f (L.CEn L.PgText String "te"))
  , Eq (f (L.CNn L.PgInt4 Int32 "i4n"))
  ) => Eq (Bar f)

qBar :: L.Q "elefun" () (Bar L.CExpr)
qBar = L.fromTable "elefun" "bar"

---
data Qux f = Qux
  { qux_i4ed :: f (L.CEd L.PgInt4 Int32 "i4ed")
  , qux_ate :: f (L.CEn (L.PgArray L.PgText) [String] "ate")
  } deriving (Generic)

instance
  ( c (L.CEd L.PgInt4 Int32 "i4ed")
  , c (L.CEn (L.PgArray L.PgText) [String] "ate")
  ) => Flay.Flayable1 c Qux

deriving instance
  ( Show (f (L.CEd L.PgInt4 Int32 "i4ed"))
  , Show (f (L.CEn (L.PgArray L.PgText) [String] "ate"))
  ) => Show (Qux f)

deriving instance
  ( Eq (f (L.CEd L.PgInt4 Int32 "i4ed"))
  , Eq (f (L.CEn (L.PgArray L.PgText) [String] "ate"))
  ) => Eq (Qux f)

qQux :: L.Q "elefun" () (Qux L.CExpr)
qQux = L.fromTable "elefun" "qux"

--------------------------------------------------------------------------------

testFreshDb
  :: Pool.Pool (L.Conn "elefun")
  -> Tasty.TestName
  -> (L.Conn "elefun" -> HU.Assertion)
  -> Tasty.TestTree
testFreshDb pool n act =
   HU.testCase n $ Pool.withResource pool $ \conn -> do
     res <- Elefun.Conn.exec conn freshSql
     st <- Pq.resultStatus res
     st @?= Pq.CommandOk
     act conn
  where
   freshSql :: BL.ByteString
   freshSql =
      "DROP SCHEMA IF EXISTS \"elefun\" CASCADE; \
      \CREATE SCHEMA \"elefun\"; \
      \CREATE TABLE \"elefun\".\"foo\" \
      \  (\"i2ed\" smallserial NOT NULL \
      \  ,\"ted\" text NOT NULL DEFAULT 'xxx'); \
      \CREATE TABLE \"elefun\".\"bar\" \
      \  (\"te\" text NOT NULL \
      \  ,\"i4n\" int4 NULL); \
      \CREATE TABLE \"elefun\".\"qux\" \
      \  (\"i4ed\" serial NOT NULL \
      \  ,\"ate\" text[] NOT NULL); \
      \INSERT INTO \"elefun\".\"foo\" (\"i2ed\", \"ted\") VALUES \
      \  (1, 'one'), (2, 'two'), (-3, 'nthree'), (4, 'four'); \
      \INSERT INTO \"elefun\".\"bar\" (\"te\", \"i4n\") VALUES \
      \  ('one', 10), ('nthree', -30); \
      \INSERT INTO \"elefun\".\"qux\" (\"i4ed\", \"ate\") VALUES \
      \  (1, CAST(ARRAY[] AS text[])), (24, ARRAY['two','four']);"

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

tt :: Pool.Pool (L.Conn "elefun") -> Tasty.TestTree
tt pool = Tasty.testGroup "Run"
  [ HU.testCase "exec" $ do
      -- Testing that Elefun.Conn.exec works
      res <- Pool.withResource pool $ \conn -> do
         Elefun.Conn.exec conn "SELECT 34"
      st <- Pq.resultStatus res
      st @?= Pq.TuplesOk
      ybs <- Pq.getvalue' res (Pq.Row 0) (Pq.Col 0)
      ybs @?= Just "34"

  , testFreshDb pool "testFreshDb" $ \_ -> do
      -- Testing that testFreshDb works
      pure ()

  , testFreshDb pool "fromTable" $ \conn -> do
      do ev <- L.select conn qFoo
         ev @?= (Right
           [ Foo {foo_i2ed = L.COutE 1, foo_ted = L.COutE "one"}
           , Foo {foo_i2ed = L.COutE 2, foo_ted = L.COutE "two"}
           , Foo {foo_i2ed = L.COutE (-3), foo_ted = L.COutE "nthree"}
           , Foo {foo_i2ed = L.COutE 4, foo_ted = L.COutE "four"} ])

      do ev <- L.select conn qBar
         fmap V.length ev @?= Right 2
  ]
