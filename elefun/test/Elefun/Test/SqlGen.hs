{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Elefun.Test.SqlGen (tt) where

import BasePrelude
import qualified Data.ByteString.Builder as BB
import qualified Data.ByteString.Lazy as BL
import qualified Test.Tasty as Tasty
import Test.Tasty.HUnit (testCase, (@?=), Assertion)
import Test.Tasty.QuickCheck (testProperty, forAll, arbitrary, (===))

import qualified Elefun.Sql as Sql

--------------------------------------------------------------------------------

tt :: Tasty.TestTree
tt = Tasty.testGroup "SqlGen"
 [ testCase "Raw" $ do
     bbl (Sql.rRaw (Sql.Raw "1")) @?= "1"
     bbl (Sql.rRaw (Sql.Raw "'foo'")) @?= "'foo'"
     bbl (Sql.rRaw (Sql.Raw "TRUE")) @?= "TRUE"

 , testCase "Name" $ do
     bbl (Sql.rName (Sql.NameNoQuote "foo")) @?= "foo"
     bbl (Sql.rName (Sql.NameQuote "foo")) @?= "\"foo\""

 , testCase "Op" $ do
     bbl (Sql.rOp (Sql.Op @'Sql.Infix   "+"))   @?= "+"
     bbl (Sql.rOp (Sql.Op @'Sql.Prefix  "NOT")) @?= "NOT"
     bbl (Sql.rOp (Sql.Op @'Sql.Postfix "!"))   @?= "!"

 , testCase "Schema" $ do
     bbl (Sql.rSchema (Sql.Schema (Sql.NameQuote "foo"))) @?= "\"foo\""

 , testCase "Identifier" $ do
     let n = Sql.NameQuote "bar"
         i0 = Sql.Identifier Nothing n
         i1 = Sql.Identifier (Just (Sql.Schema (Sql.NameQuote "foo"))) n
     bbl (Sql.rIdentifier i0) @?= "\"bar\""
     bbl (Sql.rIdentifier i1) @?= "\"foo\".\"bar\""

 , testCase "Table" $ do
     let n = Sql.NameQuote "bar"
         i0 = Sql.Identifier Nothing n
         i1 = Sql.Identifier (Just (Sql.Schema (Sql.NameQuote "foo"))) n
     bbl (Sql.rTable (Sql.Table i0)) @?= "\"bar\""
     bbl (Sql.rTable (Sql.Table i1)) @?= "\"foo\".\"bar\""

 , testCase "Collation" $ do
     let n = Sql.NameQuote "bar"
         i0 = Sql.Identifier Nothing n
         i1 = Sql.Identifier (Just (Sql.Schema (Sql.NameQuote "foo"))) n
     bbl (Sql.rCollation (Sql.Collation i0)) @?= "\"bar\""
     bbl (Sql.rCollation (Sql.Collation i1)) @?= "\"foo\".\"bar\""

 , testCase "Column" $ do
     let n0 = Sql.NameQuote "foo"
         n1 = Sql.NameQuote "bar"
         n2 = Sql.NameQuote "qux"
         t0 = Sql.Table (Sql.Identifier Nothing n1)
         t1 = Sql.Table (Sql.Identifier (Just (Sql.Schema n0)) n1)
     bbl (Sql.rColumn (Sql.Column Nothing n2)) @?= "\"qux\""
     bbl (Sql.rColumn (Sql.Column (Just t0) n2)) @?= "\"bar\".\"qux\""
     bbl (Sql.rColumn (Sql.Column (Just t1) n2)) @?= "\"foo\".\"bar\".\"qux\""

 , testCase "OrderDirection" $ do
     bbl (Sql.rOrderDirection Sql.OrderAsc) @?= "ASC"
     bbl (Sql.rOrderDirection Sql.OrderDesc) @?= "DESC"
     bbl (Sql.rOrderDirection (Sql.OrderUsing (Sql.Op "+"))) @?= "USING +"

 , testCase "OrderNulls" $ do
     bbl (Sql.rOrderNulls Sql.OrderNullsFirst) @?= "NULLS FIRST"
     bbl (Sql.rOrderNulls Sql.OrderNullsLast) @?= "NULLS LAST"

  , testProperty "Order" $ forAll arbitrary $ \o@(Sql.Order a n) -> do
      bbl (Sql.rOrderDirection a) <> " " <> bbl (Sql.rOrderNulls n)
        === bbl (Sql.rOrder o)

 , testCase "Distinct" $ do
     bbl (Sql.rDistinct Sql.Distinct) @?= "DISTINCT"

 , testCase "BoolLit" $ do
     bbl (Sql.rBoolLit (Sql.BoolLit True)) @?= "TRUE"
     bbl (Sql.rBoolLit (Sql.BoolLit False)) @?= "FALSE"
     Sql.true @?= Sql.ExprLiteral (Sql.LiteralBool (Sql.BoolLit True))
     Sql.false @?= Sql.ExprLiteral (Sql.LiteralBool (Sql.BoolLit False))

 , testCase "NumLit" $ do
     bbl (Sql.rNumLit (Sql.NumLitInt16 (27))) @?= "27"
     bbl (Sql.rNumLit (Sql.NumLitInt16 (-27))) @?= "-27"
     bbl (Sql.rNumLit (Sql.NumLitInt32 (27))) @?= "27"
     bbl (Sql.rNumLit (Sql.NumLitInt32 (-27))) @?= "-27"
     bbl (Sql.rNumLit (Sql.NumLitInt64 (28))) @?= "28"
     bbl (Sql.rNumLit (Sql.NumLitInt64 (-28))) @?= "-28"
     bbl (Sql.rNumLit (Sql.NumLitInteger (29))) @?= "29"
     bbl (Sql.rNumLit (Sql.NumLitInteger (-29))) @?= "-29"
     bbl (Sql.rNumLit (Sql.NumLitScientific (30))) @?= "30.0"
     bbl (Sql.rNumLit (Sql.NumLitScientific (-30))) @?= "-30.0"
     bbl (Sql.rNumLit (Sql.NumLitFloat (34.101))) @?= "34.101"
     bbl (Sql.rNumLit (Sql.NumLitFloat (-34.101))) @?= "-34.101"
     bbl (Sql.rNumLit (Sql.NumLitDouble (35.202))) @?= "35.202"
     bbl (Sql.rNumLit (Sql.NumLitDouble (-35.202))) @?= "-35.202"

 , testCase "TextLit" $ do
     bbl (Sql.rTextLit (Sql.TextLit "")) @?= "E''"
     bbl (Sql.rTextLit (Sql.TextLit "foo")) @?= "E'foo'"
     bbl (Sql.rTextLit (Sql.TextLit "ða§")) @?= "E'\195\176a\194\167'"

 , testCase "BytesLit" $ do
     bbl (Sql.rBytesLit (Sql.BytesLit "")) @?= "E'\\\\x'"
     bbl (Sql.rBytesLit (Sql.BytesLit "foo")) @?= "E'\\\\x666f6f'"

 , testCase "Array" $ do
     bbl (Sql.rArray (Sql.ArrayEmpty (Sql.TypeName "int4")))
       @?= "CAST(ARRAY[] AS int4[])"
     bbl (Sql.rArray (Sql.ArrayEmpty (Sql.TypeName "int4[]")))
       @?= "CAST(ARRAY[] AS int4[][])"
     bbl (Sql.rArray (Sql.Array [Sql.true])) @?= "ARRAY[TRUE]"
     bbl (Sql.rArray (Sql.Array [Sql.true, Sql.false])) @?= "ARRAY[TRUE, FALSE]"

 , testCase "ArrayIndex" $ do
     let a1 = Sql.ArrayEmpty (Sql.TypeName "int4")
         a2 = Sql.Array [Sql.true, Sql.false]
         l3 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 3))
     bbl (Sql.rArrayIndex (Sql.ArrayIndex a1 l3))
       @?= "(CAST(ARRAY[] AS int4[]))[3]"
     bbl (Sql.rArrayIndex (Sql.ArrayIndex a2 l3))
       @?= "(ARRAY[TRUE, FALSE])[3]"

 , testCase "Range" $ do
     let inf = Sql.RangeBoundInfinity
         elit = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 7))
         inc = Sql.RangeBoundInclusive elit
         exc = Sql.RangeBoundExclusive elit
     bbl (Sql.rRange (Sql.Range inf inf)) @?= "(-infinity, infinity)"
     bbl (Sql.rRange (Sql.Range inc inf)) @?= "[7, infinity)"
     bbl (Sql.rRange (Sql.Range exc inf)) @?= "(7, infinity)"
     bbl (Sql.rRange (Sql.Range inf inc)) @?= "(-infinity, 7]"
     bbl (Sql.rRange (Sql.Range inf exc)) @?= "(-infinity, 7)"
     bbl (Sql.rRange (Sql.Range inc inc)) @?= "[7, 7]"
     bbl (Sql.rRange (Sql.Range inc exc)) @?= "[7, 7)"
     bbl (Sql.rRange (Sql.Range exc inc)) @?= "(7, 7]"
     bbl (Sql.rRange (Sql.Range exc exc)) @?= "(7, 7)"

 , testCase "FunApp" $ do
     let f = Sql.NameNoQuote "f"
         g = Sql.NameQuote "g"
         x = Sql.NameQuote "x"
         y = Sql.NameNoQuote "y"
         n1 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 1))
         n2 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 2))
         n3 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 3))
         n4 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 4))
     bbl (Sql.rFunApp (Sql.FunApp f [] [])) @?= "f()"
     bbl (Sql.rFunApp (Sql.FunApp g [] [])) @?= "\"g\"()"
     bbl (Sql.rFunApp (Sql.FunApp f [n1] [])) @?= "f(1)"
     bbl (Sql.rFunApp (Sql.FunApp f [n1,n2] [])) @?= "f(1, 2)"
     bbl (Sql.rFunApp (Sql.FunApp f [] [(x,n1)])) @?= "f(\"x\" := 1)"
     bbl (Sql.rFunApp (Sql.FunApp f [] [(x,n1), (y,n2)]))
       @?= "f(\"x\" := 1, y := 2)"
     bbl (Sql.rFunApp (Sql.FunApp f [n1] [(x,n2)])) @?= "f(1, \"x\" := 2)"
     bbl (Sql.rFunApp (Sql.FunApp f [n1] [(x,n2), (y,n3)]))
       @?= "f(1, \"x\" := 2, y := 3)"
     bbl (Sql.rFunApp (Sql.FunApp f [n1,n2] [(x,n3), (y,n4)]))
       @?= "f(1, 2, \"x\" := 3, y := 4)"

 , testCase "AggrFunApp" $ do
     let f = Sql.NameNoQuote "f"
         g = Sql.NameQuote "g"
         a7 = Sql.ExprArray (Sql.Array [n7])
         n1 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 1))
         n2 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 2))
         n3 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 3))
         n7 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 7))
         o1 = Sql.Order Sql.OrderDesc Sql.OrderNullsFirst
         o2 = Sql.Order Sql.OrderAsc Sql.OrderNullsLast
         ob1 = Sql.OrderBy [(n1,o1)]
         ob2 = Sql.OrderBy [(n1,o1),(a7,o2)]
         w1 = Sql.ExprOpApp (Sql.OpAppInfix n2 (Sql.Op "<") n3)
         t = \x s -> bbl (Sql.rAggrFunApp x) @?= s

     t (Sql.AggrFunAppStar f Nothing) "f(*)"
     t (Sql.AggrFunAppStar g Nothing) "\"g\"(*)"
     t (Sql.AggrFunAppStar f (Just w1)) "f(*) FILTER (WHERE 2 < 3)"

     t (Sql.AggrFunApp f Nothing [n1] Nothing Nothing) "f(1)"
     t (Sql.AggrFunApp g Nothing [n1] Nothing Nothing) "\"g\"(1)"
     t (Sql.AggrFunApp f (Just Sql.Distinct) [n1] Nothing Nothing)
       "f(DISTINCT 1)"
     t (Sql.AggrFunApp f Nothing [n1] Nothing (Just w1))
       "f(1) FILTER (WHERE 2 < 3)"
     t (Sql.AggrFunApp f (Just Sql.Distinct) [n1] Nothing (Just w1))
       "f(DISTINCT 1) FILTER (WHERE 2 < 3)"
     t (Sql.AggrFunApp f Nothing [n1,n2] Nothing Nothing)
       "f(1, 2)"
     t (Sql.AggrFunApp f (Just Sql.Distinct) [n1,n2] Nothing Nothing)
       "f(DISTINCT 1, 2)"
     t (Sql.AggrFunApp f Nothing [n1,n2] Nothing (Just w1))
       "f(1, 2) FILTER (WHERE 2 < 3)"
     t (Sql.AggrFunApp f (Just Sql.Distinct) [n1,n2] Nothing (Just w1))
       "f(DISTINCT 1, 2) FILTER (WHERE 2 < 3)"
     t (Sql.AggrFunApp f Nothing [n2] (Just ob1) Nothing)
       "f(2 ORDER BY COALESCE(1) DESC NULLS FIRST)"
     t (Sql.AggrFunApp f Nothing [n2] (Just ob2) Nothing)
       "f(2 ORDER BY COALESCE(1) DESC NULLS FIRST, ARRAY[7] ASC NULLS LAST)"
     t (Sql.AggrFunApp f (Just Sql.Distinct) [n1, n3] (Just ob1) Nothing)
       "f(DISTINCT 1, 3 ORDER BY COALESCE(1) DESC NULLS FIRST)"
     t (Sql.AggrFunApp f Nothing [n2] (Just ob1) (Just w1))
       "f(2 ORDER BY COALESCE(1) DESC NULLS FIRST) FILTER (WHERE 2 < 3)"
     t (Sql.AggrFunApp f (Just Sql.Distinct) [n1, n3] (Just ob2) (Just w1))
       "f(DISTINCT 1, 3 ORDER BY COALESCE(1) DESC NULLS FIRST, \
         \ARRAY[7] ASC NULLS LAST) FILTER (WHERE 2 < 3)"

     t (Sql.AggrFunAppWithinGroup f [] ob1 Nothing)
       "f() WITHIN GROUP (ORDER BY COALESCE(1) DESC NULLS FIRST)"
     t (Sql.AggrFunAppWithinGroup f [n1] ob2 Nothing)
       "f(1) WITHIN GROUP (ORDER BY COALESCE(1) DESC NULLS FIRST, \
       \ARRAY[7] ASC NULLS LAST)"
     t (Sql.AggrFunAppWithinGroup f [n1,n2] ob1 Nothing)
       "f(1, 2) WITHIN GROUP (ORDER BY COALESCE(1) DESC NULLS FIRST)"
     t (Sql.AggrFunAppWithinGroup f [] ob2 (Just w1))
       "f() WITHIN GROUP (ORDER BY COALESCE(1) DESC NULLS FIRST, \
       \ARRAY[7] ASC NULLS LAST) FILTER (WHERE 2 < 3)"
     t (Sql.AggrFunAppWithinGroup f [n1] ob1 (Just w1))
       "f(1) WITHIN GROUP (ORDER BY COALESCE(1) DESC NULLS FIRST) \
       \FILTER (WHERE 2 < 3)"
     t (Sql.AggrFunAppWithinGroup f [n1,n2] ob2 (Just w1))
       "f(1, 2) WITHIN GROUP (ORDER BY COALESCE(1) DESC NULLS FIRST, \
       \ARRAY[7] ASC NULLS LAST) FILTER (WHERE 2 < 3)"

 , testCase "Frame" $ do
     let t = Sql.ExprLiteral (Sql.LiteralBool (Sql.BoolLit True))
     bbl (Sql.rFrame (Sql.FramePrecedingUnbounded)) @?= "UNBOUNDED PRECEDING"
     bbl (Sql.rFrame (Sql.FramePreceding t)) @?= "TRUE PRECEDING"
     bbl (Sql.rFrame (Sql.FrameFollowingUnbounded)) @?= "UNBOUNDED FOLLOWING"
     bbl (Sql.rFrame (Sql.FrameFollowing t)) @?= "TRUE FOLLOWING"
     bbl (Sql.rFrame (Sql.FrameCurrentRow)) @?= "CURRENT ROW"

 , testCase "SelectList" $ do
     let x = Sql.NameNoQuote "x"
         y = Sql.NameNoQuote "y"
         n1 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 1))
         n2 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 2))
     bbl (Sql.rSelectList (Sql.SelectList [(n1, Nothing)])) @?= "1"
     bbl (Sql.rSelectList (Sql.SelectList [(n1, Just x)])) @?= "1 x"
     bbl (Sql.rSelectList (Sql.SelectList [(n1, Just x), (n2, Nothing)]))
       @?= "1 x, 2"
     bbl (Sql.rSelectList (Sql.SelectList [(n1, Just x), (n2, Just y)]))
       @?= "1 x, 2 y"

 , testCase "From" $ do
     let t0 = Sql.Table (Sql.Identifier Nothing (Sql.NameQuote "foo"))
         t1 = Sql.Table (Sql.Identifier Nothing (Sql.NameQuote "bar"))
         nq = Sql.NameNoQuote "q"
         nx = Sql.NameQuote "x"
         ny = Sql.NameQuote "y"
         t = \x s -> bbl (Sql.rFrom x) @?= s
     t (Sql.FromTable t0 Nothing Nothing) "\"foo\""
     t (Sql.FromTable t0 Nothing (Just (nq, []))) "\"foo\" q"
     t (Sql.FromTable t0 Nothing (Just (nq, [nx]))) "\"foo\" q (\"x\")"
     t (Sql.FromTable t0 Nothing (Just (nq, [nx, ny])))
       "\"foo\" q (\"x\", \"y\")"

     t (Sql.FromTable t0 (Just Sql.Only) Nothing) "ONLY \"foo\""
     t (Sql.FromTable t0 (Just Sql.Only) (Just (nq, []))) "ONLY \"foo\" q"
     t (Sql.FromTable t0 (Just Sql.Only) (Just (nq, [nx])))
       "ONLY \"foo\" q (\"x\")"
     t (Sql.FromTable t0 (Just Sql.Only) (Just (nq, [nx,ny])))
       "ONLY \"foo\" q (\"x\", \"y\")"

     t (Sql.FromJoin
         (Sql.FromTable t0 Nothing (Just (nq, [nx,ny])))
         (Sql.Join Sql.Inner (Sql.FromTable t1 Nothing Nothing) Sql.false))
       "\"foo\" q (\"x\", \"y\") JOIN \"bar\" ON (FALSE)"
     t (Sql.FromJoin
         (Sql.FromTable t0 Nothing (Just (nq, [nx,ny])))
         (Sql.Join Sql.FullOuter (Sql.FromTable t1 Nothing Nothing) Sql.false))
       "\"foo\" q (\"x\", \"y\") FULL JOIN \"bar\" ON (FALSE)"
     t (Sql.FromJoin
         (Sql.FromTable t0 Nothing (Just (nq, [nx,ny])))
         (Sql.Join Sql.LeftOuter (Sql.FromTable t1 Nothing Nothing) Sql.false))
       "\"foo\" q (\"x\", \"y\") LEFT JOIN \"bar\" ON (FALSE)"
     t (Sql.FromJoin
         (Sql.FromTable t0 Nothing (Just (nq, [nx,ny])))
         (Sql.Join Sql.Inner (Sql.FromTable t1 Nothing Nothing) Sql.true))
       "\"foo\" q (\"x\", \"y\") CROSS JOIN \"bar\""

     -- TODO: t (Sql.FromSelect _ _)

 , testCase "Frame" $ do
     let t = \a b -> bbl (Sql.rFrame a) @?= b
     t (Sql.FramePrecedingUnbounded) "UNBOUNDED PRECEDING"
     t (Sql.FramePreceding Sql.false) "FALSE PRECEDING"
     t (Sql.FrameFollowingUnbounded) "UNBOUNDED FOLLOWING"
     t (Sql.FrameFollowing Sql.false) "FALSE FOLLOWING"
     t (Sql.FrameCurrentRow) "CURRENT ROW"

 , testCase "FrameTarget" $ do
     bbl (Sql.rFrameTarget Sql.FrameTargetRange) @?= "RANGE"
     bbl (Sql.rFrameTarget Sql.FrameTargetRows) @?= "ROWS"

 , testCase "FrameClause" $ do
     let t = \a b -> bbl (Sql.rFrameClause a) @?= b
     t (Sql.FrameClause Sql.FrameTargetRange Sql.FramePrecedingUnbounded
          Nothing)
       "RANGE UNBOUNDED PRECEDING"
     t (Sql.FrameClause Sql.FrameTargetRows Sql.FramePrecedingUnbounded
          (Just Sql.FrameCurrentRow))
       "ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW"

 , testCase "Window" $ do
     let t = \a b -> bbl (Sql.rWindow a) @?= b
         nx = Sql.NameQuote "x"
         o1 = Sql.Order Sql.OrderDesc Sql.OrderNullsFirst
         ob = Sql.OrderBy [(Sql.true, o1)]
         fc = Sql.FrameClause Sql.FrameTargetRows Sql.FramePrecedingUnbounded
                             (Just Sql.FrameCurrentRow)

     t (Sql.Window Nothing [Sql.true] Nothing Nothing)
       "PARTITION BY TRUE"
     t (Sql.Window Nothing [Sql.true] (Just ob) Nothing)
       "PARTITION BY TRUE ORDER BY TRUE DESC NULLS FIRST"
     t (Sql.Window Nothing [Sql.true,Sql.false] (Just ob) (Just fc))
       "PARTITION BY TRUE, FALSE ORDER BY TRUE DESC NULLS FIRST \
       \ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW"
     t (Sql.Window (Just nx) [Sql.true,Sql.false] (Just ob) (Just fc))
       "\"x\" PARTITION BY TRUE, FALSE ORDER BY TRUE DESC NULLS FIRST \
       \ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW"
     -- These branches seem useless
     t (Sql.Window Nothing [] Nothing Nothing) ""
     t (Sql.Window (Just nx) [] Nothing Nothing) "\"x\""

 , testCase "Select" $ do
     let t :: Sql.Sel -> BL.ByteString -> Assertion
         t = \a b -> bbl (Sql.rSelect (Sql.runSel a)) @?= b
         nx = Sql.NameQuote "x"
         ny = Sql.NameQuote "y"
         l3 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 3))
         t0 = Sql.Table (Sql.Identifier Nothing (Sql.NameQuote "foo"))
         s0 = Sql.SelFrom (Sql.FromTable t0 Nothing Nothing) Sql.SelEmpty
         ob = Sql.OrderBy [(Sql.true, Sql.Order Sql.OrderDesc Sql.OrderNullsFirst)]
         fc = Sql.FrameClause Sql.FrameTargetRows Sql.FramePrecedingUnbounded
                             (Just Sql.FrameCurrentRow)
     t s0 "SELECT * FROM \"foo\""

     -- sList. See more individual tests for SelectList
     let s1 = Sql.SelList (Sql.SelectList
                [(Sql.true, Just nx), (Sql.false, Just ny)])
     t (s1 s0) "SELECT TRUE \"x\", FALSE \"y\" FROM \"foo\""

     -- sDistinct
     let s2 = Sql.SelDistinct
     t (s2 s0) "SELECT DISTINCT * FROM \"foo\""
     let s3 = Sql.SelDistinctOn Sql.true
     t (s3 s0) "SELECT DISTINCT ON (TRUE) * FROM \"foo\""
     let s4 = Sql.SelDistinctOn Sql.false
     t (s4 (s3 s0)) "SELECT DISTINCT ON (TRUE, FALSE) * FROM \"foo\""

     -- sWhere
     let s5 = Sql.SelWhere Sql.true
     t (s5 s0) "SELECT * FROM \"foo\" WHERE (TRUE)"

     -- sGroupBy
     let s6 = Sql.SelGroup Sql.true
     t (s6 s0) "SELECT * FROM \"foo\" GROUP BY TRUE"
     let s7 = Sql.SelGroup Sql.false
     t (s7 (s6 s0)) "SELECT * FROM \"foo\" GROUP BY TRUE, FALSE"

     -- sHaving
     let s8 = Sql.SelHaving Sql.true
     t (s8 s0) "SELECT * FROM \"foo\" HAVING (TRUE)"

     -- sWindow
     let s9 = Sql.SelWindow nx (Sql.Window (Just ny)
                 [Sql.true,Sql.false] (Just ob) (Just fc))
     t (s9 s0) "SELECT * FROM \"foo\" WINDOW \"x\" \
               \AS (\"y\" PARTITION BY TRUE, FALSE \
               \ORDER BY TRUE DESC NULLS FIRST \
               \ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)"

     -- sUie
     let s10 = Sql.SelUieAll Sql.Intersect s0
     t (s10 s0) "(SELECT * FROM \"foo\") INTERSECT ALL (SELECT * FROM \"foo\")"

     -- sOrderBy
     let s11 = Sql.SelOrder ob
     t (s11 s0) "SELECT * FROM \"foo\" ORDER BY TRUE DESC NULLS FIRST"

     -- sLimit
     let s12 = Sql.SelLimit l3
     t (s12 s0) "SELECT * FROM \"foo\" LIMIT 3"

     -- sOffset
     let s13 = Sql.SelOffset l3
     t (s13 s0) "SELECT * FROM \"foo\" OFFSET 3"

     -- sFor
     let s14 = Sql.SelFor (Sql.For Sql.ForUpdate [t0] (Just Sql.NoWait))
     t (s14 s0) "SELECT * FROM \"foo\" FOR UPDATE OF \"foo\" NOWAIT"

     -- This is probably some malformed SQL. That's OK. We have it here mostly
     -- just so we can check the order in which stuff is rendered.
     t ((s14.s13.s12.s11.s10.s9.s8.s7.s6.s5.s4.s3.s2.s1) s0)
       "(SELECT * FROM \"foo\") INTERSECT ALL (SELECT DISTINCT ON (TRUE, \
       \FALSE) TRUE \"x\", FALSE \"y\" FROM \"foo\" WHERE (TRUE) GROUP BY \
       \TRUE, FALSE HAVING (TRUE) WINDOW \"x\" AS (\"y\" PARTITION BY TRUE, \
       \FALSE ORDER BY TRUE DESC NULLS FIRST ROWS BETWEEN UNBOUNDED PRECEDING \
       \AND CURRENT ROW))"

 , testCase "With" $ do
     let t0 = Sql.Table (Sql.Identifier Nothing (Sql.NameQuote "foo"))
         s0 = Sql.runSel (Sql.SelFrom (Sql.FromTable t0 Nothing Nothing)
                         Sql.SelEmpty)
         t1 = Sql.Table (Sql.Identifier Nothing (Sql.NameQuote "foo"))
         s1 = Sql.runSel (Sql.SelFrom (Sql.FromTable t1 Nothing Nothing)
                         Sql.SelEmpty)
     bbl (Sql.rWith (Sql.With False [(Sql.NameQuote "x", s0)]))
       @?= "WITH \"x\" AS (SELECT * FROM \"foo\")"
     bbl (Sql.rWith (Sql.With True [(Sql.NameQuote "x", s1),
                                    (Sql.NameQuote "y", s0)]))
       @?= "WITH RECURSIVE \"x\" AS (SELECT * FROM \"foo\"), \"y\" AS (\
           \SELECT * FROM \"foo\")"

 , testCase "InsertInput" $ do
     let t = \a b -> bbl (Sql.rInsertInput a) @?= b
         t0 = Sql.Table (Sql.Identifier Nothing (Sql.NameQuote "foo"))
         s0 = Sql.runSel (Sql.SelFrom (Sql.FromTable t0 Nothing Nothing)
                          Sql.SelEmpty)
     t Sql.InsertInputDefaults "DEFAULT VALUES"
     t (Sql.InsertInputValues [Sql.NameQuote "a"] [[Sql.true]])
       "(\"a\") VALUES (TRUE)"
     t (Sql.InsertInputValues [Sql.NameQuote "a", Sql.NameQuote "b"]
                              [[Sql.true, Sql.false]])
       "(\"a\", \"b\") VALUES (TRUE, FALSE)"
     t (Sql.InsertInputValues [Sql.NameQuote "a", Sql.NameQuote "b"]
                              [[Sql.true, Sql.false], [Sql.false, Sql.true]])
       "(\"a\", \"b\") VALUES (TRUE, FALSE), (FALSE, TRUE)"
     t (Sql.InsertInputSelect [Sql.NameQuote "a", Sql.NameQuote "b"] s0)
       "(\"a\", \"b\") SELECT * FROM \"foo\""

 , testCase "In" $ do
     bbl (Sql.rIn (Sql.In Sql.true [Sql.false])) @?= "TRUE IN (FALSE)"
     bbl (Sql.rIn (Sql.In Sql.false [Sql.true, Sql.false]))
       @?= "FALSE IN (TRUE, FALSE)"

 , testCase "Cast" $ do
     bbl (Sql.rCast (Sql.Cast Sql.true (Sql.TypeName "foo")))
       @?= "CAST(TRUE AS foo)"

 , testCase "ExprDefault" $ do
     bbl (Sql.rExpr Sql.ExprDefault) @?= "DEFAULT"

 , testCase "ExprNull" $ do
     bbl (Sql.rExpr Sql.ExprNull) @?= "NULL"

 , testCase "Case" $ do
     let t = \a b -> bbl (Sql.rCase a) @?= b
         l1 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 1))
         l2 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 2))
         l3 = Sql.ExprLiteral (Sql.LiteralNum (Sql.NumLitInt16 3))
     t (Sql.Case [(Sql.true, l1)] l2)
       "CASE WHEN TRUE THEN 1 ELSE 2 END"
     t (Sql.Case [(Sql.true, l1), (Sql.false, l2)] l3)
       "CASE WHEN TRUE THEN 1 WHEN FALSE THEN 2 ELSE 3 END"

 , testCase "CompositeConstructor" $ do
     let t = \a b -> bbl (Sql.rCompositeConstructor a) @?= b
     t (Sql.CompositeConstructor []) "ROW()"
     t (Sql.CompositeConstructor [Sql.true]) "ROW(TRUE)"
     t (Sql.CompositeConstructor [Sql.true, Sql.false]) "ROW(TRUE, FALSE)"

 , testCase "CompositeConstructor" $ do
     let t = \a b -> bbl (Sql.rCompositeConstructor a) @?= b
     t (Sql.CompositeConstructor []) "ROW()"
     t (Sql.CompositeConstructor [Sql.true]) "ROW(TRUE)"
     t (Sql.CompositeConstructor [Sql.true, Sql.false]) "ROW(TRUE, FALSE)"

 , testCase "CompositeField" $ do
     let t = \a b -> bbl (Sql.rCompositeField a) @?= b
         c0 = Sql.ExprCompositeConstructor (Sql.CompositeConstructor [Sql.true])
     t (Sql.CompositeField c0 (Sql.NameQuote "x")) "(ROW(TRUE)).\"x\""
     t (Sql.CompositeField c0 (Sql.NameNoQuote "y")) "(ROW(TRUE)).y"

 , testCase "CompositeFields" $ do
     let c0 = Sql.ExprCompositeConstructor (Sql.CompositeConstructor [Sql.true])
     bbl (Sql.rCompositeFields (Sql.CompositeFields c0)) @?= "(ROW(TRUE)).*"
 ]

--------------------------------------------------------------------------------
-- Misc

bbl :: BB.Builder -> BL.ByteString
bbl = BB.toLazyByteString

