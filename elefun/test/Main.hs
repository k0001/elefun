{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import BasePrelude
import qualified Data.ByteString.Char8 as B8
import qualified Data.Pool as Pool
import qualified Test.Tasty as Tasty
import qualified Test.Tasty.Runners as Tasty

import qualified Elefun as L
import qualified Elefun.Test.Run
import qualified Elefun.Test.SqlGen

--------------------------------------------------------------------------------

main :: IO ()
main = do
  dbPool :: Pool.Pool (L.Conn "elefun") <- do
     dbUrl <- B8.pack <$> getEnv "ELEFUN_TEST_DB_URL"
     Pool.createPool (L.connect dbUrl) L.close 2 4 6
  Tasty.defaultMainWithIngredients
    [ Tasty.consoleTestReporter
    , Tasty.listingTests
    ] (tt dbPool)

tt :: Pool.Pool (L.Conn "elefun") -> Tasty.TestTree
tt pool = Tasty.testGroup "elefun"
  [ Elefun.Test.SqlGen.tt
  , Elefun.Test.Run.tt pool
  ]
