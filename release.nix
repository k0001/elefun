{ nixpkgsBootstrap ? <nixpkgs>
, nixpkgs ? (import nixpkgsBootstrap {}).fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs-channels";
    rev = "3eccd0b11d176489d69c778f2fcb544438f3ab56"; # unstable, dec 4 2017
    sha256 = "1i3p5m0pnn86lzni5y1win0sacckw3wlg9kqaw15nszhykgz22zq"; }
}:

let
pkgs = import nixpkgs {};
hs = pkgs.haskell.lib;
inherit (pkgs.lib) versionAtLeast;

hsPackageSetConfig = self: super: {
  elefun = super.callPackage ./elefun/pkg.nix {};
  ex-pool = hs.overrideCabal super.ex-pool (_:
    { src = pkgs.fetchFromGitHub {
        owner = "kim";
        repo = "ex-pool";
        rev = "fca9301b99e4ad36fd0d8a70a0e77ca78e233489";
        sha256 = "13iv4vp1k6l2zx7hhf2xvib9fp9rwjw3phns2dba4rby6pwg12j2"; };
    });
  singletons = super.singletons_2_3_1;
  th-desugar = super.th-desugar_1_7;
  flay = super.callPackage "${pkgs.fetchFromGitHub {
      owner = "k0001";
      repo = "flay";
      rev = "278f73cf3ddd8b95ae4a982de1e02eed35d0d01f";
      sha256 = "08xify0yggq47qgkl09x57lpdi0sm5i9fa6z06fi63hsv89sr79y";
    }}/pkg.nix" {};
};

ghc822 = pkgs.haskell.packages.ghc822.override {
  packageSetConfig = hsPackageSetConfig;
};

in { inherit (ghc822) elefun; }
